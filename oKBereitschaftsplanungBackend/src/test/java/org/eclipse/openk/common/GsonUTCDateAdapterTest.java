/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.common;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.util.Date;

import org.junit.Test;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

public class GsonUTCDateAdapterTest {
	Date myDate;
	Type type;
	JsonSerializationContext jsonSerializationContext;
	JsonDeserializationContext jsonDeserializationContext;

	@Test
	public void testSerialize() {
		GsonUTCDateAdapter dateAdapter = new GsonUTCDateAdapter();
		JsonElement jsonElement = dateAdapter.serialize(new Date(), type, jsonSerializationContext);
		assertNotNull(jsonElement);

		Date date = dateAdapter.deserialize(jsonElement, type, jsonDeserializationContext);
		assertNotNull(date);
	}

	@Test
	public void testSerializeException() {
		Throwable th = null;

		GsonUTCDateAdapter dateAdapter = new GsonUTCDateAdapter();
		JsonElement jsonElement = dateAdapter.serialize(new Date(), type, jsonSerializationContext);
		assertNotNull(jsonElement);

		JsonPrimitive value = new JsonPrimitive("blah");

		try {
			Date date = dateAdapter.deserialize(value, type, jsonDeserializationContext);
		} catch (Exception e) {
			th = e;
		}

		assertNotNull(th);
	}

}