/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanningPhaseDtoTest {


	@Test
	public void getSet() {
		
		PlanningPhaseDto planningPhaseDto = new PlanningPhaseDto();
		assertNotNull(planningPhaseDto);
		

		
		Date d = new Date();
		String text = "text";
		Integer i = 5;
		Long id = new Long(3);

		planningPhaseDto.setEndDate(d);
		assertEquals(d, planningPhaseDto.getEndDate());

		planningPhaseDto.setStartDate(d);
		assertEquals(d, planningPhaseDto.getStartDate());
		


		List<UserInStandbyGroup> lsUsers = new ArrayList<>();
		UserInStandbyGroup msg = new UserInStandbyGroup(id);
		lsUsers.add(msg);
		planningPhaseDto.setLsUsers(lsUsers);
		assertNotNull(planningPhaseDto.getLsUsers());
		assertEquals(1, planningPhaseDto.getLsUsers().size());
		
		planningPhaseDto = new PlanningPhaseDto(d, d, lsUsers);
		assertNotNull(planningPhaseDto);

	}

}