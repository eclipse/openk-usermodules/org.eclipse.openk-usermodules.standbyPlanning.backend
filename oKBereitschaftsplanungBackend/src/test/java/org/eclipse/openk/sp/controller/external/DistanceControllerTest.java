/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class DistanceControllerTest {

	@Mock
	AuthController authController;

	@Mock
	FileHelper fileHelper;

	@Mock
	LocationRepository locationRepository;

	@Mock
	EntityConverter entityConverter;

	@Spy
	@InjectMocks
	DistanceController distanceController;

	private String errorToken = "https://169.50.13.155/elogbookFE/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String accessToken = "http://169.50.13.155/spfe/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";

	private boolean useHttps = true;

	@Test
	public void calcDistanceTest() throws IOException, HttpStatusException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		Long id = 1L;
		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		String token = accessToken;

		Response response = Response.status(200).build();
		Mockito.when(authController.login(Mockito.anyString())).thenReturn(response);

		List<StandbyScheduleBody> result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token,
				true);
		assertNotNull(result);

		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_SKIP)).thenReturn("true");

		result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token, true);
		assertNotNull(result);

		standbyScheduleSearchDto.setLocationId(id);
		result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token, true);
		assertNotNull(result);

		Mockito.when(locationRepository.findOne(standbyScheduleSearchDto.getLocationId())).thenReturn(location);
		Mockito.when(entityConverter.convertEntityToDto(location, new LocationDto())).thenReturn(locationDto);

		listBodies.add(body);
		result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token, true);
		assertNotNull(result);
	}

	@Test(expected = SpNestedException.class)
	public void calcDistanceExceptionTest() throws IOException, HttpStatusException {

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		listBodies.add(body);
		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		standbyScheduleSearchDto.setLocationId(location.getId());
		String token = accessToken;

		Mockito.when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString())).thenThrow(new IOException());

		List<StandbyScheduleBody> result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token,
				true);
		assertNotNull(result);

		result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token, false);

	}

	@Test(expected = SpNestedException.class)
	public void calcDistanceException2Test() throws IOException, HttpStatusException {

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		listBodies.add(body);
		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		standbyScheduleSearchDto.setLocationId(location.getId());
		String token = null;

//		Mockito.when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString())).thenThrow(new IOException());

		List<StandbyScheduleBody> result = distanceController.calcDistance(listBodies, standbyScheduleSearchDto, token,
				false);
		assertNotNull(result);

	}

	@Test
	public void mergeResponseTest() throws SpNestedException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		String retJson = fh.loadStringFromResource("testDistanceServiceDtoList.json");

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		listBodies.add(body);

		HttpResponse response = getResponse("http://localhost:8080/portalFE/#/login");

		Mockito.doReturn(retJson).when(distanceController).responseToJson(Mockito.any(), Mockito.any());

		boolean result = distanceController.mergeResponse(response, listBodies);

		assertNotNull(result);

	}

	private HttpResponse getResponse(String strURL) {
		System.out.println("getResponse");

		HttpResponse response = null;
		// Execute request an catch response
		try {
			HttpGet getRequest = new HttpGet(strURL);
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			response = httpClient.execute(getRequest);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("getResponse - ende");
		return response;

	}

}
