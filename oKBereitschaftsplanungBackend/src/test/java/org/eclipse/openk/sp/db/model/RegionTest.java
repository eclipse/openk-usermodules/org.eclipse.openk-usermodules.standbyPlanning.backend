/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.ArrayList;

import org.junit.Test;

public class RegionTest {

	@Test
	public void testGettersAndSetters() {
		Region r = new Region();

		r.setId(1l);
		assertEquals(1l, r.getId().longValue());

		r.setRegionName("Region");
		assertEquals("Region", r.getRegionName());

		Location d = new Location();
		Location d2 = new Location();
		ArrayList<Location> lsLocation = new ArrayList<>();
		lsLocation.add(d);
		lsLocation.add(d2);
		r.setLsLocations(lsLocation);
		assertNotNull(lsLocation);
		assertTrue(lsLocation.size() == 2);
		assertNotNull(r.getLsLocations());
		assertTrue(r.getLsLocations().size() == 2);
		assertEquals(d, r.getLsLocations().get(0));
		assertEquals(d2, r.getLsLocations().get(1));

		r.setLsStandbyGroups(new ArrayList<StandbyGroup>());
		assertNotNull(r.getLsStandbyGroups());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		Region obj1 = new Region();
		obj1.setId(5L);

		Region obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		Region obj3 = new Region();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

	@Test
	public void testConstructor() {
		Region obj1 = new Region();
		obj1.setId(5L);

		assertEquals(new Long(5), obj1.getId());

		Region obj2 = new Region(7L);
		assertEquals(new Long(7), obj2.getId());

	}

	@Test
	public void testCopy() {
		Region obj1 = new Region();
		obj1.setId(5L);

		Region obj2 = obj1.copy();
		assertEquals(null, obj2.getId());
		assertNotEquals(obj1.hashCode(), obj2.hashCode());
	}

}
