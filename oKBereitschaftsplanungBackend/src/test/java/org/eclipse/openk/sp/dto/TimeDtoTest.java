/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class TimeDtoTest {

	@Test
	public void getterAndSetterTest() {
		TimeDto dto = new TimeDto();

		dto.setHour(1);
		assertNotNull(dto.getHour());
		assertTrue(dto.getHour() == 1);

		dto.setMinute(1);
		assertNotNull(dto.getMinute());
		assertTrue(dto.getMinute() == 1);

		dto.setSecond(59);
		assertNotNull(dto.getSecond());
		assertTrue(dto.getSecond() == 59);

		Date date = dto.getAsDate();
		assertNotNull(date);

		dto.setHour(0);
		dto.setMinute(0);
		dto.setSecond(0);

		dto.storeDate(date);

		assertTrue(dto.getHour() == 1);
		assertTrue(dto.getMinute() == 1);
		assertTrue(dto.getSecond() == 59);
	}
}
