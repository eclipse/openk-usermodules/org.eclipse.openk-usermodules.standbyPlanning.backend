/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class UserTest {

	@Test
	public void testGettersAndSetters() {
		String test = "test";
		Date d = new Date();
		User u = new User();
		ContactData cd = new ContactData();

		u.setId(1l);
		assertEquals(1l, u.getId().longValue());

		u.setBusinessContactData(cd);
		assertEquals(cd, u.getBusinessContactData());

		u.setFirstname("FirstName");
		assertEquals("FirstName", u.getFirstname());

		u.setIsCompany(true);
		assertEquals(true, u.getIsCompany());

		u.setLastname("LastName");
		assertEquals("LastName", u.getLastname());

		u.setHrNumber(test);
		assertEquals(test, u.getHrNumber());

		u.setUserKey(test);
		assertEquals(test, u.getUserKey());

		u.setModificationDate(d);
		assertEquals(d, u.getModificationDate());

		u.setNotes("This is a place to mak e a notice.");
		assertEquals("This is a place to mak e a notice.", u.getNotes());

		Organisation orga = new Organisation();
		u.setOrganisation(orga);
		assertEquals(orga, u.getOrganisation());

		Address add = new Address();
		u.setPrivateAddress(add);
		assertEquals(add, u.getPrivateAddress());

		u.setPrivateContactData(cd);
		assertEquals(cd, u.getPrivateContactData());

		u.setValidFrom(d);
		assertEquals(d, u.getValidFrom());

		u.setValidTo(d);
		assertEquals(d, u.getValidTo());

		List<UserInRegion> lsUserInRegion = new ArrayList<UserInRegion>();
		UserInRegion uir = new UserInRegion();
		lsUserInRegion.add(uir);
		u.setLsUserInRegions(lsUserInRegion);
		assertNotNull(u.getLsUserInRegions());
		assertEquals(uir, u.getLsUserInRegions().get(0));

		List<UserHasUserFunction> lsUserFunctions = new ArrayList<UserHasUserFunction>();
		UserHasUserFunction uf = new UserHasUserFunction();
		lsUserFunctions.add(uf);
		u.setLsUserHasUserFunction(lsUserFunctions);
		assertNotNull(u.getLsUserHasUserFunction());
		assertEquals(uf, u.getLsUserHasUserFunction().get(0));
	}

	@Test
	public void testCompare() throws MalformedURLException {
		User obj1 = new User();
		obj1.setId(5L);

		User obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		User obj3 = new User();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
}
