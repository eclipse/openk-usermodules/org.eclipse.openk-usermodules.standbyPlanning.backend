/*
******************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.sp.dto.report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.junit.Test;
import org.mockito.InjectMocks;

public class ReportDtoTest {

	@InjectMocks
	ReportDto reportDto;

	@Test
	public void testGettersAndSetters() throws MalformedURLException, ParseException {
		Long id = 1L;

		ReportDto report = new ReportDto();
		report.setReportName("Wochenübersicht");
		assertEquals("Wochenübersicht", report.getReportName());

		report.setReportName("Monatsübersicht");
		assertEquals("Monatsübersicht", report.getReportName());

		report.setPrintFormat(null);
		report.setPrintFormat("pdf");
		assertEquals("pdf", report.getPrintFormat());

		report.setUserId(1L);
		assertEquals(1L, report.getUserId().longValue());

		Date validationDate = new Date();

		report.setValidFromDate(validationDate);
		assertEquals(validationDate.getTime(), report.getValidFromDate().getTime());

		report.setValidToDate(new Date());
		assertEquals(validationDate.getTime(), report.getValidToDate().getTime());

		report.setStandByGroup(new StandbyGroup());
		assertNotNull(report.getStandByGroup());

		report.setStandByList(new StandbyList());
		assertNotNull(report.getStandByList());

		report.setDefaultDayRange(5);
		assertEquals((Integer) 5, report.getDefaultDayRange());

		report.setReportLevel("Ist-Ebene");
		assertEquals("Ist-Ebene", report.getReportLevel());

		report.setGroupPosition(1);
		assertEquals(1, report.getGroupPosition());

		report.setGroupId(id);
		assertEquals(id.longValue(), report.getGroupId().longValue());

		report.setStandByListId(id);
		assertEquals(id.longValue(), report.getStandByListId().longValue());

		report.setStatusId(id);
		assertEquals(id.longValue(), report.getStatusId().longValue());
	}
}
