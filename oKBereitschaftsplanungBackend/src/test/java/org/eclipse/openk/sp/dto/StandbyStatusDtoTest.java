/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StandbyStatusDtoTest {

	@Test
	public void testGettersAndSetters() {
		StandbyStatusDto status = new StandbyStatusDto();
		status.setId(1l);
		assertEquals(1l, status.getId().longValue());

		status.setTitle("Title");
		assertEquals("Title", status.getTitle());
	}
	
	


}
