/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class StandbyScheduleBodyArchiveDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		StandbyScheduleBodyArchiveDto body = new StandbyScheduleBodyArchiveDto();
		body.setId(1l);
		assertEquals(1l, body.getId().longValue());

		body.setValidFrom(d);
		assertEquals(d, body.getValidFrom());

		body.setValidTo(d);
		assertEquals(d, body.getValidTo());

		// StandbyScheduleHeaderDto header = new StandbyScheduleHeaderDto();
		// body.setStandbyScheduleHeader(header);
		// assertEquals(header, body.getStandbyScheduleHeader());

		UserDto user = new UserDto();
		body.setUser(user);
		assertEquals(user, body.getUser());

		StandbyStatusDto status = new StandbyStatusDto();
		body.setStatus(status);
		assertEquals(status, body.getStatus());

		StandbyGroupArchiveDto standbyGroup = new StandbyGroupArchiveDto();
		body.setStandbyGroup(standbyGroup);
		assertEquals(standbyGroup, body.getStandbyGroup());

	}

}
