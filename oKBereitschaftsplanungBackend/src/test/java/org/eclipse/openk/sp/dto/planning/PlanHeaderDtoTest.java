/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanHeaderDtoTest {

	@InjectMocks
	private PlanHeaderDto planHederDto;

	@Test
	public void getSet() {
		String text = "text";
		planHederDto.setLabel(text);
		assertEquals(text, planHederDto.getLabel());

		List<StandbyGroupSelectionDto> listGroups = new ArrayList<>();
		StandbyGroupSelectionDto group = new StandbyGroupSelectionDto();
		listGroups.add(group);
		planHederDto.setListGroups(listGroups);

		assertNotNull(planHederDto.getListGroups());
		assertEquals(1, planHederDto.getListGroups().size());
	}

}
