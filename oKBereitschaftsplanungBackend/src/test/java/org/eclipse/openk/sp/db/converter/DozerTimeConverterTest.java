/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.dto.TimeDto;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DozerTimeConverterTest {
	protected static final Logger logger = Logger.getLogger(DozerTimeConverterTest.class);

	@Test
	public void convertTest() {
		DozerTimeConverter dozerTimeConverter = new DozerTimeConverter();

		// prepare data
		TimeDto srcDto = new TimeDto();
		srcDto.setHour(1);
		srcDto.setMinute(59);
		srcDto.setSecond(59);

		// create Date from TimeDto
		Object resultDate = dozerTimeConverter.convert(null, srcDto, null, null);
		assertNotNull(resultDate);
		assertTrue(resultDate instanceof Date);
		DateTime dt = new DateTime(resultDate);
		assertTrue(dt.getHourOfDay() == 1);
		assertTrue(dt.getMinuteOfHour() == 59);
		assertTrue(dt.getSecondOfMinute() == 59);

		// create TimeDto from Date
		Object resultTimeDto = dozerTimeConverter.convert(null, resultDate, null, null);
		assertNotNull(resultTimeDto);
		assertTrue(resultTimeDto instanceof TimeDto);
		TimeDto resultDto = (TimeDto) resultTimeDto;
		assertTrue(resultDto.getHour() == 1);
		assertTrue(resultDto.getMinute() == 59);
		assertTrue(resultDto.getSecond() == 59);

		// create wrong Input
		Object resultWrong = dozerTimeConverter.convert(null, new String("Error"), null, null);
		assertNull(resultWrong);
	}
}
