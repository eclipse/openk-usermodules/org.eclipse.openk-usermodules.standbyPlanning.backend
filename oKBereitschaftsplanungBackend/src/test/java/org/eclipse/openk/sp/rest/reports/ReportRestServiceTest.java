package org.eclipse.openk.sp.rest.reports;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

@RunWith(MockitoJUnitRunner.class)
public class ReportRestServiceTest {

	@Mock
	StandbyGroupRepository standbyGroupRepository;

	@Mock
	StandbyListRepository standbyListRepository;

	@Mock
	ReportController reportController;

	@InjectMocks
	ReportRestService reportRestService;

	@Test
	public void getReportsTest() {
		String jwt = "LET-ME-IN";
		Response result = reportRestService.getReports(jwt);
		assertNotNull(result);
	}

	@Test
	public void generateReportTest() throws SpException, IOException {
		String jwt = "LET-ME-IN";
		Response result;

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(1L);
		standbyGroup.setTitle("Einsatzgruppe1");

		StandbyList standbyList = new StandbyList();
		standbyList.setId(1L);
		standbyList.setTitle("Einsatzliste1");
		standbyList.getLsStandbyGroups().add(standbyGroup);

		File mockFile = new ClassPathResource("reports/Autoplan.pdf").getFile();
		Mockito.when(standbyListRepository.findOne(Mockito.any())).thenReturn(standbyList);
		Mockito.when(standbyGroupRepository.findOne(Mockito.any())).thenReturn(standbyGroup);
		Mockito.when(reportController.generateReport(Mockito.any())).thenReturn(mockFile);

		Date date1 = DateHelper.getDate(2018, 10, 11, 12, 00, 00);
		Date date2 = DateHelper.getDate(2018, 10, 11, 12, 00, 00);
		// If list is input parameter
		result = reportRestService.generateReport("Autoplan", 1L, null, null, "pdf", date1, date2, "Ist-Ebene",
				response, jwt);
		assertNotNull(result);

		// If group is input parameter
		result = reportRestService.generateReport("Autoplan", null, 1L, null, "pdf", date1, date2, "Plan-Ebene",
				response, jwt);
		assertNotNull(result);

		Mockito.when(reportController.generateReport(Mockito.any())).thenThrow(new SpException());

		// If group is input parameter
		result = reportRestService.generateReport("Autoplan", null, 1L, null, "pdf", date1, date2, "Plan-Ebene",
				response, jwt);
		assertNotNull(result);

	}

	
	@Test
	public void generateReportNewTest() throws SpException, IOException {
		String jwt = "LET-ME-IN";
		Response result;

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(1L);
		standbyGroup.setTitle("Einsatzgruppe1");

		StandbyList standbyList = new StandbyList();
		standbyList.setId(1L);
		standbyList.setTitle("Einsatzliste1");
		standbyList.getLsStandbyGroups().add(standbyGroup);

		File mockFile = new ClassPathResource("reports/Autoplan.pdf").getFile();
		Mockito.when(standbyListRepository.findOne(Mockito.any())).thenReturn(standbyList);
		Mockito.when(standbyGroupRepository.findOne(Mockito.any())).thenReturn(standbyGroup);
		Mockito.when(reportController.generateReport(Mockito.any())).thenReturn(mockFile);

		Date date1 = DateHelper.getDate(2018, 10, 11, 12, 00, 00);
		Date date2 = DateHelper.getDate(2018, 10, 11, 12, 00, 00);

		ReportDto reportDto = new ReportDto();
		reportDto.setValidFromDate(date1);
		reportDto.setValidToDate(date2);

		reportDto.setStandByList(standbyList);
		reportDto.setStatusId(1L);

//		// If list is input parameter
//		result = reportRestService.generateReportNew(response, jwt, reportDto);
//		assertNotNull(result);
//
//		Mockito.when(reportController.generateReport(Mockito.any())).thenThrow(new SpException());
//
//		// If group is input parameter
//		result = reportRestService.generateReportNew(response, jwt, null);
//		assertNotNull(result);

	}
}
