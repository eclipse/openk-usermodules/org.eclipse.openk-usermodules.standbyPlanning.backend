/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.junit.Test;

public class StandbyScheduleFilterDtoTest {

	@Test
	public void getSet() {
		Date d = new Date();
		Long id = new Long(2);

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(id);
		assertEquals(id, filter.getStandbyListId());

		filter.setValidFrom(d);
		assertEquals(d, filter.getValidFrom());

		filter.setValidTo(d);
		assertEquals(d, filter.getValidTo());

	}

}
