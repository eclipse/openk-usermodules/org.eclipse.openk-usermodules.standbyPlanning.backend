/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExternalInterfaceMapperTest {

	@Mock
	AuthController authController;

	@Mock
	DistanceController distanceService;

	@Mock
	FileHelper fileHelper;

	@InjectMocks
	ExternalInterfaceMapper externalInterfaceMapper;

	private String errorToken = "https://169.50.13.155/elogbookFE/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String accessToken = "http://169.50.13.155/spfe/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";

	private boolean useHttps = true;

	@Test
	public void calcDistanceTest() throws IOException, HttpStatusException {

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		String token = accessToken;

		Response response = Response.status(200).build();
		Mockito.when(authController.login(Mockito.anyString())).thenReturn(response);

		List<StandbyScheduleBody> result = externalInterfaceMapper.calcDistance(listBodies, standbyScheduleSearchDto,
				token);
		assertNotNull(result);

		Mockito.when(distanceService.calcDistance((List<StandbyScheduleBody>) Mockito.anyList(), Mockito.any(),
				Mockito.anyString(), Mockito.anyBoolean())).thenThrow(new RuntimeException());
		result = externalInterfaceMapper.calcDistance(listBodies, standbyScheduleSearchDto, token);
		assertNotNull(result);

		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_SKIP)).thenReturn("true");

		result = externalInterfaceMapper.calcDistance(listBodies, standbyScheduleSearchDto, token);
		assertNotNull(result);

	}

	@Test
	public void calcDistanceExceptionTest() throws IOException, HttpStatusException {

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		String token = accessToken;

		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_SKIP))
				.thenThrow(new IOException());

		List<StandbyScheduleBody> result = externalInterfaceMapper.calcDistance(listBodies, standbyScheduleSearchDto,
				token);
		assertNotNull(result);

	}

	@Test
	public void loginTest() throws IOException, HttpStatusException {

		Response response = Response.status(200).build();
		Mockito.when(authController.login(Mockito.anyString())).thenReturn(response);

		response = externalInterfaceMapper.authLogin(accessToken);
		assertNotNull(response);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void logoutTest() throws IOException, HttpStatusException {

		Response response = Response.status(200).build();
		Mockito.when(authController.logout(Mockito.anyString())).thenReturn(response);

		response = externalInterfaceMapper.authLogout(accessToken);
		assertNotNull(response);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void responseFromExceptionTest1() {

		Response response = null;

		Exception exception = new Exception("TestException");

		response = externalInterfaceMapper.responseFromException(exception);

		assertEquals(500, response.getStatus());
	}

	@Test
	public void responseFromExceptionTest2() {

		Response response = null;

		Exception exception = new HttpStatusException(999, "TestException");

		response = externalInterfaceMapper.responseFromException(exception);

		assertEquals(999, response.getStatus());
	}

	@Test
	public void responseFromExceptionTest3() {

		Response response = null;
		ErrorReturn error = new ErrorReturn(999, "test");
		SpNestedException exception = new SpNestedException(error);

		response = externalInterfaceMapper.responseFromException(exception);

		assertEquals(999, response.getStatus());
	}

	private HttpResponse getResponse(String strURL) {
		System.out.println("getResponse");

		HttpResponse response = null;
		// Execute request an catch response
		try {
			HttpGet getRequest = new HttpGet(strURL);
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			response = httpClient.execute(getRequest);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("getResponse - ende");
		return response;

	}

}
