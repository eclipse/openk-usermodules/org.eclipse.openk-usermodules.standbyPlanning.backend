/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.StandbyListController;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyListHasStandbyGroupDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class StandbyListRestServiceTest {

	@InjectMocks
	private StandbyListRestService standbyListRestService;

	@Mock
	private StandbyListController standbyListController;

	public StandbyListRestServiceTest() {

	}

	@Test
	public void getStandbyListsTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyListRestService.getStandbyLists(jwt);
		assertNotNull(result);
	}

	@Test
	public void getStandbyListsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyListRestService.getStandbyListsSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getStandbyListTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyListRestService.getStandbyList(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void getStandbyListDropdownTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyListRestService.getStandbyListsDropdown(jwt);
		assertNotNull(result);
	}

	@Test
	public void saveStandbyListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);

		String jwt = "LET-ME-IN";
		Response result = standbyListRestService.saveStandbyList(jwt, standbyListDto);
		assertNotNull(result);
	}

	@Test
	public void addStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();
		Long standbyGroupId = 30L;
		String jwt = "LET-ME-IN";

		Response result = standbyListRestService.addStandbyGroup(standbyGroupId, jwt, lsStandbyListHasStandbyGroupDto);
		assertNotNull(result);
	}

	@Test
	public void deleteStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();
		Long standbyGroupId = 30L;
		String jwt = "LET-ME-IN";

		Response result = standbyListRestService.deleteStandbyGroup(standbyGroupId, jwt,
				lsStandbyListHasStandbyGroupDto);
		assertNotNull(result);
	}

}
