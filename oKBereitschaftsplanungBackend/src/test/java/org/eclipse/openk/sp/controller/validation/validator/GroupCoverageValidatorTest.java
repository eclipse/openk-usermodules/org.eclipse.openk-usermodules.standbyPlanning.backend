/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/** Class to validate if all time slots are set for a group. */
@RunWith(MockitoJUnitRunner.class)
public class GroupCoverageValidatorTest {
	protected static final Logger LOGGER = Logger.getLogger(GroupCoverageValidatorTest.class);

	@Mock
	private PlanningController planningController;

	@Mock
	private StandbyScheduleBodyRepository scheduleBodyRepository;

	@InjectMocks
	GroupCoverageValidator groupCoverageValidator;

	@Test
	public void executeTest() throws SpException {

		Date from = DateHelper.getDate(2018, 9, 9);
		Date till = DateHelper.getDate(2018, 9, 10);
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		lsStandbyGroups.add(group);
		lsStandbyGroups.add(group);
		Long statusId = new Long(1);

		// positive test - with holiday
		Mockito.when(planningController.isHoliday(Mockito.any(), Mockito.any())).thenReturn(true);
		List<PlanningMsgDto> planningMsgDtos = groupCoverageValidator.execute(from, till, group, lsStandbyGroups,
				statusId, null);
		assertNotNull(planningMsgDtos);

		// positive test - without holiday
		Mockito.when(planningController.isHoliday(Mockito.any(), Mockito.any())).thenReturn(false);
		planningMsgDtos = groupCoverageValidator.execute(from, till, group, lsStandbyGroups, statusId, null);
		assertNotNull(planningMsgDtos);
	}
}
