/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;

import org.junit.Test;

public class OrganisationTest {

	@Test
	public void testGettersAndSetters() {
		Organisation o = new Organisation();
		
		o.setOrgaName("Mettenmeier");
		assertEquals("Mettenmeier", o.getOrgaName());
		
		o.setId(1l);
		assertEquals(1l, o.getId().longValue());
		
		Address a = new Address();
		o.setAddress(a);
		assertEquals(a, o.getAddress());
		
	}
	
	@Test
	public void testCompare() throws MalformedURLException {
		Organisation obj1 = new Organisation();
		obj1.setId(5L);

		Organisation obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		Organisation obj3 = new Organisation();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
