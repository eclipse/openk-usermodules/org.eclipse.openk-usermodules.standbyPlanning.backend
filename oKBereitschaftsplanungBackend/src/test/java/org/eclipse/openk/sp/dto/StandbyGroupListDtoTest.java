/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;

import org.eclipse.openk.sp.db.model.StandbyList;
import org.junit.Test;

public class StandbyGroupListDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		Boolean b = new Boolean(true);
		StandbyGroupListDto group = new StandbyGroupListDto();
		group.setId(1l);
		assertEquals(1l, group.getId().longValue());

		group.setModificationDate(d);
		assertEquals(d, group.getModificationDate());

		group.setTitle("Title");
		assertEquals("Title", group.getTitle());

		group.setNote("Test Note");
		assertEquals("Test Note", group.getNote());

		group.setNextUserInNextCycle(b);
		assertEquals(b, group.getNextUserInNextCycle());

		ArrayList<StandbyList> lsStandbyLists = new ArrayList<>();
		group.setLsStandbyLists(lsStandbyLists);
		assertEquals(lsStandbyLists, group.getLsStandbyLists());
	}


}
