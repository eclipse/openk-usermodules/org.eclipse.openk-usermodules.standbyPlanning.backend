package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidationDtoTest {

	@Test
	public void testGettersAndSetters() {
		ValidationDto dto = new ValidationDto();
		dto.setStandbyListId(1L);
		assertTrue(dto.getStandbyListId().longValue() == 1L);
	}
}
