/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.eclipse.openk.sp.controller.CyclicReportsController;
import org.eclipse.openk.sp.dto.ReportGenerationConfigDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CyclicReportsRestServiceTest {

	@InjectMocks
	private CyclicReportsRestService automaticReportsRestService;

	@Mock
	private CyclicReportsController automaticReportsController;

	@Test
	public void constructorTest() {
		CyclicReportsRestService arrs = new CyclicReportsRestService();
		assertNotNull(arrs);
	}

	@Test
	public void getAllReportGenerationConfigTest() throws SpException {
		String jwt = "TOKEN";
		Mockito.when(automaticReportsController.getAllReportGenerationConfig()).thenReturn(new ArrayList<>());
		Response response = automaticReportsRestService.getAllReportGenerationConfig(jwt);
		assertNotNull(response);
	}

	@Test
	public void addReportGenerationConfigTest() {
		String jwt = "TOKEN";
		ReportGenerationConfigDto reportGenerationConfig = new ReportGenerationConfigDto();
		Response response = automaticReportsRestService.addReportGenerationConfig(reportGenerationConfig, jwt);
		assertNotNull(response);
	}

	@Test
	public void deleteReportGenerationConfigTest() {
		String jwt = "TOKEN";
		Long configId = 23L;
		Response response = automaticReportsRestService.deleteReportGenerationConfig(configId, jwt);
		assertNotNull(response);
	}

	@Test
	public void editReportGenerationConfigTest() {
		String jwt = "TOKEN";
		ReportGenerationConfigDto reportGenerationConfig = new ReportGenerationConfigDto();
		Long configId = 23L;
		Response response = automaticReportsRestService.editReportGenerationConfig(configId, reportGenerationConfig,
				jwt);
		assertNotNull(response);
	}
}
