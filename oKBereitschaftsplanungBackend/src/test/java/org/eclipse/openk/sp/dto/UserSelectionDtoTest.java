/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class UserSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {
		String test = "test";
		Date d = new Date();
		UserSelectionDto u = new UserSelectionDto();

		u.setId(1l);
		assertEquals(1l, u.getId().longValue());

		u.setFirstname("FirstName");
		assertEquals("FirstName", u.getFirstname());

		u.setIsCompany(true);
		assertEquals(true, u.getIsCompany());

		u.setLastname("LastName");
		assertEquals("LastName", u.getLastname());

		u.setHrNumber(test);
		assertEquals(test, u.getHrNumber());

		u.setUserKey(test);
		assertEquals(test, u.getUserKey());

		u.setModificationDate(d);
		assertEquals(d, u.getModificationDate());

		u.setNotes("This is a place to mak e a notice.");
		assertEquals("This is a place to mak e a notice.", u.getNotes());

		u.setValidFrom(d);
		assertEquals(d, u.getValidFrom());

		u.setValidTo(d);
		assertEquals(d, u.getValidTo());

		u.setUserRegionStr("region");
		assertEquals("region", u.getUserRegionStr());

		u.setUserHasUserFunctionStr("userHasUserFunction");
		assertEquals("userHasUserFunction", u.getUserHasUserFunctionStr());

		u.setOrganisationName("Orga");
		assertEquals("Orga", u.getOrganisationName());

		u.setUserRole("role");
		assertEquals("role", u.getUserRole());

		AddressDto addressDto = new AddressDto();
		u.setPrivateAddress(addressDto);
		assertEquals(addressDto, u.getPrivateAddress());

		ContactDataDto contactDataDto = new ContactDataDto();
		u.setPrivateContactData(contactDataDto);
		assertEquals(contactDataDto, u.getPrivateContactData());

		u.setBusinessContactData(contactDataDto);
		assertEquals(contactDataDto, u.getBusinessContactData());

		OrganisationDto org = new OrganisationDto();
		u.setOrganisation(org);

		assertEquals(org, u.getOrganisation());

	}
}
