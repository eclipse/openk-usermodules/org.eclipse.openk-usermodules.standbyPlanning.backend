/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class StandbyScheduleBodyTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		String text = "user";
		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setId(1l);
		assertEquals(1l, body.getId().longValue());

		body.setModificationDate(d);
		assertEquals(d, body.getModificationDate());

		body.setModifiedBy(text);
		assertEquals(text, body.getModifiedBy());

		body.setModifiedCause(text);
		;
		assertEquals(text, body.getModifiedCause());

		body.setValidFrom(d);
		assertEquals(d, body.getValidFrom());

		body.setOrganisationDistance(text);
		assertEquals(text, body.getOrganisationDistance());

		body.setPrivateDistance(text);
		assertEquals(text, body.getPrivateDistance());

		body.setValidTo(d);
		assertEquals(d, body.getValidTo());

		User user = new User();
		body.setUser(user);
		assertEquals(user, body.getUser());

		StandbyStatus status = new StandbyStatus();
		body.setStatus(status);
		assertEquals(status, body.getStatus());

		StandbyGroup standbyGroup = new StandbyGroup();
		body.setStandbyGroup(standbyGroup);
		assertEquals(standbyGroup, body.getStandbyGroup());

	}

	@Test
	public void testCompare() throws MalformedURLException {
		StandbyScheduleBody obj1 = new StandbyScheduleBody();
		obj1.setId(5L);

		StandbyScheduleBody obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyScheduleBody obj3 = new StandbyScheduleBody();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

	@Test
	public void toStringTest() {
		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setId(5L);

		User user = new User();
		user.setLastname("lastname");
		body.setUser(user);

		StandbyGroup standbyGroup = new StandbyGroup();
		body.setStandbyGroup(standbyGroup);
		standbyGroup.setTitle("title");

		String result = body.toString();
		assertNotNull(result);

	}
}
