/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanningBodyResultDtoTest {

	@InjectMocks
	private PlanningBodyResultDto planningBodyResultDto;

	@Test
	public void getSet() {
		Date d = new Date();
		String text = "text";
		Integer i = 5;
		Long id = new Long(3);

		planningBodyResultDto.setTempDate(d);
		assertEquals(d, planningBodyResultDto.getTempDate());

		planningBodyResultDto.setNewPosition(i);
		assertEquals(i, planningBodyResultDto.getNewPosition());
		
		planningBodyResultDto.setLastStartUserId(id);
		assertEquals(id, planningBodyResultDto.getLastStartUserId());

		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		PlanningMsgDto msg = new PlanningMsgDto(text, text, text);
		lsMsg.add(msg);
		planningBodyResultDto.setLsMsg(lsMsg);
		assertNotNull(planningBodyResultDto.getLsMsg());
		assertEquals(1, planningBodyResultDto.getLsMsg().size());

	}

}