/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class StandbyCycleTest {

	@Test
	public void testGettersAndSetters() {
		StandbyCycle c = new StandbyCycle();
		c.setCycleName("Name");
		assertEquals("Name", c.getCycleName());

		Date d = new Date();
		c.setEndTime(d);
		assertEquals(d, c.getEndTime());
		
		c.setStartTime(d);
		assertEquals(d, c.getStartTime());

		c.setHandOverDay("Montag");
		assertEquals("Montag", c.getHandOverDay());
		c.setHandOverOnHoliday(d);
		assertEquals(d, c.getHandOverOnHoliday());

		c.setId(1l);
		assertEquals(1l, c.getId().longValue());

		c.setIsSpecialStandby(true);
		assertTrue(c.getIsSpecialStandby());

		c.setWeekdayEnd("Montag");
		assertEquals("Montag", c.getWeekdayEnd());

		c.setWeekdayStart("Montag");
		assertEquals("Montag", c.getWeekdayStart());

		c.setWeekendEndTime(d);
		assertEquals(d, c.getWeekendEndTime());

		c.setWeekendStartTime(d);
		assertEquals(d, c.getWeekendStartTime());
	}

	
	
	@Test
	public void testCompare() throws MalformedURLException {
		StandbyCycle obj1 = new StandbyCycle();
		obj1.setId(5L);

		StandbyCycle obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyCycle obj3 = new StandbyCycle();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
	
	
}
