/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class UserInStandByGroupDtoTest {

	@Test
	public void testGettersAndSetters() {
		Integer i = new Integer(1);
		Long l = new Long(30l);
		Date d = new Date();
		String t = "Text";

		UserInStandbyGroupDto u = new UserInStandbyGroupDto();

		u.setId(l);
		assertEquals(l, u.getId());

		u.setValidFrom(d);
		assertEquals(d, u.getValidFrom());

		u.setValidTo(d);
		assertEquals(d, u.getValidTo());

		u.setUserId(l);
		assertEquals(l, u.getUserId());

		u.setStandbyGroupId(l);
		assertEquals(l, u.getStandbyGroupId());

		u.setFirstname(t);
		assertEquals(t, u.getFirstname());

		u.setLastname(t);
		assertEquals(t, u.getLastname());

		u.setUserRegionStr(t);
		assertEquals(t, u.getUserRegionStr());

		u.setUserHasUserFunctionStr(t);
		assertEquals(t, u.getUserHasUserFunctionStr());

		u.setOrganisationName(t);
		assertEquals(t, u.getOrganisationName());

		u.setPosition(i);
		assertEquals(i, u.getPosition());
	}
}
