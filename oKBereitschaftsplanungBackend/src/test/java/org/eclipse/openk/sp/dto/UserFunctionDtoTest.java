/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserFunctionDtoTest {

	@Test
	public void testGettersAndSetters() {

		UserFunctionDto uf = new UserFunctionDto();

		uf.setId(1l);
		assertEquals(1l, uf.getId().longValue());
		
		uf.setFunctionId(1l);
		assertEquals(1l, uf.getFunctionId().longValue());

		uf.setFunctionName("Name");
		assertEquals("Name", uf.getFunctionName());

//		List<RegionHasFunction> ls = new ArrayList<>();
//		RegionHasFunction rhf = new RegionHasFunction();
//		ls.add(rhf);
//		uf.setLsRegionHasFunctions(ls);
//		assertNotNull(uf.getLsRegionHasFunctions());
//		assertEquals(rhf, uf.getLsRegionHasFunctions().get(0));

//		List<User> lsUsers = new ArrayList<User>();
//		User user = new User();
//		lsUsers.add(user);
//		uf.setLsUser(lsUsers);
//		assertNotNull(uf.getLsUser());
//		assertEquals(user, uf.getLsUser().get(0));
	}
}
