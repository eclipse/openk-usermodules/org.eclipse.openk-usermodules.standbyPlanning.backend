/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsArchiveDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.junit.Test;

public class StandbyScheduleArchiveDtoTest {

	@Test
	public void getSet() {
		StandbyScheduleArchiveDto dto = new StandbyScheduleArchiveDto();
		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		dto.setFilter(filter);
		assertEquals(filter, dto.getFilter());

		PlanHeaderDto planHeader = new PlanHeaderDto();
		dto.setPlanHeader(planHeader);
		assertEquals(planHeader, dto.getPlanHeader());

		List<PlanRowsArchiveDto> listPlanRows = new ArrayList<>();
		dto.setListPlanRows(listPlanRows);
		assertEquals(listPlanRows, dto.getListPlanRows());

	}

}
