/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.controller.planning.BodyDataCopyController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.StandbyDurationRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle standbyGroup operations. */
@RunWith(MockitoJUnitRunner.class)
public class CopyControllerTest {
	protected static final Logger logger = Logger.getLogger(CopyControllerTest.class);

	@Mock
	private BranchRepository branchRepository;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private UserFunctionRepository userFunctionRepository;

	@Mock
	private UserInStandbyGroupRepository userInStandbyGroupRepository;

	@Mock
	private StandbyDurationRepository standbyDurationRepository;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private RegionRepository regionRepository;

	@Mock
	private CalendarRepository calendarRepository;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	private BodyDataCopyController bodyDataCopyController;

	@Mock
	private ValidationController validationController;

	@Spy
	@InjectMocks
	private CopyController copyController;

	@Mock
	StandbyGroupController standbyGroupController;

	@Test
	public void copyStandbyGroupTest() throws SpException, InstantiationException, IllegalAccessException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroupDto.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);

		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<Branch> list = standbyGroup.getLsBranches();
		Branch branch = new Branch(1L);
		list.add(branch);

		StandbyGroup newSBG = new StandbyGroup();
		newSBG.setId(110L);

		CopyDto copyDto = new CopyDto();
		copyDto.setCopyBranch(true);
		copyDto.setCopyCalendar(true);
		copyDto.setCopyDuration(true);
		copyDto.setCopyFunction(true);
		copyDto.setCopyRegion(true);
		copyDto.setCopyUser(true);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyGroup);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save((StandbyGroup) Mockito.any())).thenReturn(newSBG);

		List userFunctionList = standbyGroup.getLsUserFunction();
		Mockito.doReturn(userFunctionList).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));

		List regionList = standbyGroup.getLsRegions();
		Mockito.doReturn(regionList).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(RegionDto.class));

		// run test
		StandbyGroupDto standbyGroupResult = null;

		standbyGroupResult = (StandbyGroupDto) copyController.copy(1L, copyDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());

		copyDto.setCopyBranch(false);
		copyDto.setCopyCalendar(false);
		copyDto.setCopyDuration(false);
		copyDto.setCopyFunction(false);
		copyDto.setCopyRegion(false);
		copyDto.setCopyUser(false);

		standbyGroupResult = (StandbyGroupDto) copyController.copy(1L, copyDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());

		copyDto.setCopyBranch(true);
		copyDto.setCopyCalendar(false);
		copyDto.setCopyDuration(false);
		copyDto.setCopyFunction(false);
		copyDto.setCopyRegion(false);
		copyDto.setCopyUser(false);

		standbyGroup.setLsBranches(null);
		standbyGroupResult = (StandbyGroupDto) copyController.copy(1L, copyDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());

		standbyGroup.setLsBranches(new ArrayList<>());
		standbyGroupResult = (StandbyGroupDto) copyController.copy(1L, copyDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());
	}

	@Test
	public void calcCopyOfEntityListTest() throws InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		List<AbstractDto> result = copyController.calcCopyOfEntityList(arrayAbstr, new UserInStandbyGroupDto());

		assertNotNull(result);

	}

	@Test
	public void copyDeepUserTest() throws SpException, InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		List<AbstractDto> listDto = new ArrayList<>();
		UserInStandbyGroupDto uISG = new UserInStandbyGroupDto();
		uISG.setValidFrom(new Date());
		uISG.setValidTo(new Date());
		listDto.add(uISG);

		Mockito.doReturn(listDto).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));

		copyController.copyDeepUser(standbyGroup, 110L, true);

	}

	@Test(expected = SpException.class)
	public void copyDeepUserErrorTest() throws SpException, InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		Mockito.doThrow(new InstantiationException()).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));
		copyController.copyDeepUser(standbyGroup, 110L, true);

	}

	@Test(expected = SpException.class)
	public void copyDeepUserErrorTest2() throws SpException, InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		Mockito.doThrow(new IllegalAccessException()).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));
		copyController.copyDeepUser(standbyGroup, 110L, true);

	}

	@Test(expected = SpException.class)
	public void copyDeepDurationErrorTest() throws SpException, InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		Mockito.doThrow(new InstantiationException()).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));
		copyController.copyDeepDuration(standbyGroup, 110L);

	}

	@Test(expected = SpException.class)
	public void copyDeepDurationErrorTest2() throws SpException, InstantiationException, IllegalAccessException {

		FileHelperTest fh = new FileHelperTest();
		String json2 = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
		standbyGroup.setId(1l);

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		Mockito.doThrow(new IllegalAccessException()).when(copyController).calcCopyOfEntityList(Mockito.any(),
				Mockito.any(UserFunctionSelectionDto.class));
		copyController.copyDeepDuration(standbyGroup, 110L);

	}

	@Test
	public void copyDeepBranchesTest() throws SpException {
		Exception th = null;
		try {

			FileHelperTest fh = new FileHelperTest();
			String json2 = fh.loadStringFromResource("testStandbyGroup.json");
			StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json2, StandbyGroup.class);
			standbyGroup.setId(1l);

			List<Branch> list = standbyGroup.getLsBranches();
			Branch branch = new Branch(1L);
			list.add(branch);

			copyController.copyDeepBranches(standbyGroup, 110L);

		} catch (Exception e) {
			th = e;
		}
		assertNull(th);

	}
}
