/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class StandbyScheduleHeaderTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		StandbyScheduleHeader header = new StandbyScheduleHeader();
		header.setId(1l);
		assertEquals(1l, header.getId().longValue());

		header.setModificationDate(d);
		assertEquals(d, header.getModificationDate());

		header.setValidFrom(d);
		assertEquals(d, header.getValidFrom());

		header.setValidTo(d);
		assertEquals(d, header.getValidTo());
		
		header.setTitle("Title");
		assertEquals("Title", header.getTitle());
		
		StandbyStatus status = new StandbyStatus();
		header.setStandbyStatus(status);
		assertEquals(status, header.getStandbyStatus());
	}
	
	
	@Test
	public void testCompare() throws MalformedURLException {
		StandbyScheduleHeader obj1 = new StandbyScheduleHeader();
		obj1.setId(5L);

		StandbyScheduleHeader obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyScheduleHeader obj3 = new StandbyScheduleHeader();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
