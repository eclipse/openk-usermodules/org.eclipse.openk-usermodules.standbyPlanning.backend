/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HealthControllerTest {

	@Mock
	PostcodeRepository postcodeRepository;

	@InjectMocks
	HealthController healthController;
	
	@Mock
	FileHelper fileHelper;

	private static final Logger LOGGER = Logger.getLogger(HealthController.class.getName());

	@Test
	public void getVersionTest() throws IOException {
		
		when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString())).thenReturn("42");
		String version = healthController.getVersion();
		assertNotNull(version);
	}
	
	@Test
	public void getVersion2Test() throws IOException {
		
		when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString())).thenThrow(new IOException());
//		Mockito.doThrow(new IOException()).when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString()));
		
		String version = healthController.getVersion();
		assertNull(version);
	}

	@Test
	public void responseFromExceptionTest1() {

		Response response = null;

		Exception exception = new Exception("TestException");

		response = healthController.responseFromException(exception);

		assertEquals(500, response.getStatus());
	}

	@Test
	public void responseFromExceptionTest2() {

		Response response = null;

		Exception exception = new HttpStatusException(999, "TestException");

		response = healthController.responseFromException(exception);

		assertEquals(999, response.getStatus());
	}

	@Test
	public void getAppStatusTeste() throws HttpStatusException {

		boolean result = healthController.checkDB();
		assertEquals(true, result);

	}

	@Test (expected = HttpStatusException.class)
	public void getAppStatusExceptionTeste() throws HttpStatusException {

		when(postcodeRepository.count()).thenThrow(new MockitoException("TestException"));

		boolean result = healthController.checkDB();
		assertNull(result);

	}

}
