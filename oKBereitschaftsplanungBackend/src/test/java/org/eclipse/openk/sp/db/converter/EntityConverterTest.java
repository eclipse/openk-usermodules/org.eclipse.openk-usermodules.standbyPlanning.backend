/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserSelectionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle converting between Entity and DTO. */
@RunWith(MockitoJUnitRunner.class)
public class EntityConverterTest {
	protected static final Logger logger = Logger.getLogger(EntityConverterTest.class);

	@Mock
	DozerBeanMapper mapper;

	@InjectMocks
	EntityConverter entityConverter;

	@Test
	public void convertEntityToDtoTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);
		User user = (User) new Gson().fromJson(json, User.class);

		// create when
		when(mapper.map(any(), any())).thenReturn((UserDto) userDto);

		UserDto userResultDto = (UserDto) entityConverter.convertEntityToDto(user, userDto);

		assertNotNull(userResultDto);
	}

	@Test
	public void convertDtoToEntityTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);
		User user = (User) new Gson().fromJson(json, User.class);

		// create when
		when(mapper.map(any(), any())).thenReturn((User) user);

		User userResult = (User) entityConverter.convertDtoToEntity(userDto, user);

		assertNotNull(userResult);
	}

	@Test
	public void converUserSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserSelectionDto userSelectionDto = (UserSelectionDto) new Gson().fromJson(json, UserSelectionDto.class);
		User user = (User) new Gson().fromJson(json, User.class);

		// create when
		when(mapper.map(any(), any())).thenReturn((User) user);

		User result1 = (User) entityConverter.convertDtoToEntity(userSelectionDto, user);
		assertNotNull(result1);

		// create when
		when(mapper.map(any(), any())).thenReturn((UserSelectionDto) userSelectionDto);

		UserSelectionDto result2 = (UserSelectionDto) entityConverter.convertEntityToDto(user, userSelectionDto);

		assertNotNull(result2);
	}

	@Test
	public void convertStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto dto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		StandbyGroup entity = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		// create when
		when(mapper.map(any(), any())).thenReturn((StandbyGroup) entity);

		StandbyGroup result1 = (StandbyGroup) entityConverter.convertDtoToEntity(dto, entity);
		assertNotNull(result1);

		// create when
		when(mapper.map(any(), any())).thenReturn((StandbyGroupDto) dto);

		StandbyGroupDto result2 = (StandbyGroupDto) entityConverter.convertEntityToDto(entity, dto);

		assertNotNull(result2);
	}

	@Test
	public void convertEntityDtoListTest() {

		List<AbstractDto> listDto = new ArrayList<>();
		List<AbstractEntity> listEntities = new ArrayList<>();

		when(mapper.map(any(), any())).thenReturn(new BranchDto());
		listEntities.add(new Branch());
		entityConverter.convertEntityToDtoList(listEntities, listDto, BranchDto.class);
		assertTrue(listDto.get(0) instanceof BranchDto);
		listDto.remove(0);
		listEntities.remove(0);
		
		// null values, error cases and false classes
		entityConverter.convertEntityToDtoList(listEntities, null, StandbyDurationDto.class);
		entityConverter.convertEntityToDtoList(null, null, StandbyDurationDto.class);
		entityConverter.convertEntityToDtoList(listEntities, listDto, Exception.class);
	}

	@Test
	public void convertDtoToEntityListTest() {

		List<AbstractEntity> listEntity = new ArrayList<>();
		List<AbstractDto> listDtos = new ArrayList<>();
		listDtos.add(new StandbyDurationDto());
		entityConverter.convertDtoToEntityList(listDtos, listEntity, StandbyDuration.class);

		// null values, error cases
		entityConverter.convertDtoToEntityList(null, null, StandbyDurationDto.class);
	}
}
