/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.BodyDataCopyController;
import org.eclipse.openk.sp.controller.planning.PlannedDataController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyScheduleHistoryRepository;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/** Class to handle StandbyPlan operations. */
@RunWith(MockitoJUnitRunner.class)
public class StandbyScheduleControllerTest extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(StandbyScheduleController.class);

	@Mock
	private BodyDataCopyController bodyDataCopyController;

	@Mock
	private PlannedDataController plannedDataController;

	@Mock
	private PlanningController planningController;

	@Mock
	private StandbyScheduleHistoryRepository standbyScheduleHistoryRepository;

	@Mock
	private EntityConverter entityConverter;

	@InjectMocks
	StandbyScheduleController standbyScheduleController = new StandbyScheduleController();

	@Test
	public void getFilteredPlanPlanningTest() throws SpException {
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();

		StandbyScheduleDto sbsDto = new StandbyScheduleDto();
		Mockito.when(plannedDataController.getFilteredPlanByStatus((StandbyScheduleFilterDto) Mockito.any(),
				Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(sbsDto);

		StandbyScheduleDto result = standbyScheduleController.getFilteredPlanPlanning(standbyScheduleFilterDto);

		assertNotNull(result);
	}

	@Test
	public void getFilteredPlanByStatusIdTest() throws SpException {
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		Long statusId = new Long(1);

		StandbyScheduleDto sbsDto = new StandbyScheduleDto();
		Mockito.when(plannedDataController.getFilteredPlanByStatus((StandbyScheduleFilterDto) Mockito.any(),
				Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(sbsDto);

		StandbyScheduleDto result = standbyScheduleController.getFilteredPlanByStatusId(standbyScheduleFilterDto,
				statusId);

		assertNotNull(result);
	}

	@Test
	public void getFilteredPlanClosedPlanningTest() throws SpException {
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();

		StandbyScheduleDto sbsDto = new StandbyScheduleDto();
		Mockito.when(plannedDataController.getFilteredPlanByStatus((StandbyScheduleFilterDto) Mockito.any(),
				Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(sbsDto);

		StandbyScheduleDto result = standbyScheduleController.getFilteredPlanClosedPlanning(standbyScheduleFilterDto);

		assertNotNull(result);
	}

	@Test
	public void calculatePlan() throws SpException {
		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		PlanningMsgResponseDto tempResponse = new PlanningMsgResponseDto();
		String username = "user";
		List<Long> list = new ArrayList<>();

		Mockito.when(
				plannedDataController.getIdsOfExistingScheduleBodies(((StandbyScheduleBlueprintDto) Mockito.any())))
				.thenReturn(list);

		Mockito.when(planningController.startPlanning((StandbyScheduleBlueprintDto) Mockito.any(), Mockito.anyString()))
				.thenReturn(tempResponse);

		PlanningMsgResponseDto result = standbyScheduleController.calculatePlan(standbyBlueprintDto, username);
		assertNotNull(result);

	}

	@Test
	public void calculatePlan2() throws SpException {
		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		PlanningMsgResponseDto tempResponse = new PlanningMsgResponseDto();
		String username = "user";
		List<Long> list = new ArrayList<>();
		list.add(new Long(2));

		Mockito.when(
				plannedDataController.getIdsOfExistingScheduleBodies(((StandbyScheduleBlueprintDto) Mockito.any())))
				.thenReturn(list);

		Mockito.when(planningController.startPlanning((StandbyScheduleBlueprintDto) Mockito.any(), Mockito.anyString()))
				.thenReturn(tempResponse);

		PlanningMsgResponseDto result = standbyScheduleController.calculatePlan(standbyBlueprintDto, username);
		assertNotNull(result);

	}

	@Test
	public void deleteBodyDataTest() throws SpException {
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		String username = "user";
		PlanningMsgResponseDto response = new PlanningMsgResponseDto();

		Mockito.when(bodyDataCopyController.copyByGroups((StandbyScheduleCopyDto) Mockito.any(), Mockito.anyString()))
				.thenReturn(response);

		PlanningMsgResponseDto result = standbyScheduleController.deleteBodyData(standbyScheduleCopyDto, username);
		assertNull(result);

	}

	@Test
	public void copyBodyDataTest() throws SpException {
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		String username = "user";
		PlanningMsgResponseDto response = new PlanningMsgResponseDto();

		Mockito.when(bodyDataCopyController.copyByGroups((StandbyScheduleCopyDto) Mockito.any(), Mockito.anyString()))
				.thenReturn(response);

		PlanningMsgResponseDto result = standbyScheduleController.copyBodyData(standbyScheduleCopyDto, username);
		assertNotNull(result);

	}
}
