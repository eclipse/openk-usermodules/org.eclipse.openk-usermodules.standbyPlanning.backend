/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CopyDtoTest {

	@Test
	public void testGettersAndSetters() {
		CopyDto copyDto = new CopyDto();

		copyDto.setCopyRegion(true);
		assertEquals(true, copyDto.isCopyRegion());

		copyDto.setCopyFunction(true);
		assertEquals(true, copyDto.isCopyFunction());

		copyDto.setCopyUser(true);
		assertEquals(true, copyDto.isCopyUser());

		copyDto.setCopyBranch(true);
		assertEquals(true, copyDto.isCopyBranch());

		copyDto.setCopyCalendar(true);
		assertEquals(true, copyDto.isCopyCalendar());

		copyDto.setCopyDuration(true);
		assertEquals(true, copyDto.isCopyDuration());

		copyDto.setUserPlusOneYear(true);
		assertEquals(true, copyDto.isUserPlusOneYear());

	}

}
