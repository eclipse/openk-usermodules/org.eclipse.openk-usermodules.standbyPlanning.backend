/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.controller.planning.BodyDataCopyController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.StandbyDurationRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyDurationResponseDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupResponseDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle standbyGroup operations. */
@RunWith(MockitoJUnitRunner.class)
public class StandbyGroupControllerTest {
	protected static final Logger logger = Logger.getLogger(StandbyGroupControllerTest.class);

	@Mock
	private BranchRepository branchRepository;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private UserFunctionRepository userFunctionRepository;

	@Mock
	private UserInStandbyGroupRepository userInStandbyGroupRepository;

	@Mock
	private StandbyDurationRepository standbyDurationRepository;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private RegionRepository regionRepository;

	@Mock
	private CalendarRepository calendarRepository;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	private BodyDataCopyController bodyDataCopyController;

	@Mock
	private ValidationController validationController;

	@Mock
	private CopyController copyController;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Spy
	@InjectMocks
	StandbyGroupController standbyGroupController = new StandbyGroupController();

	@Test
	public void saveStandbyGroupTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroup.setId(1l);

		standbyGroup.setLsStandbyDurations(null);
		// standbyGroup.setLsStandbyLists(null);
		standbyGroup.setLsUserInStandbyGroups(null);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyGroup);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		// run test
		StandbyGroupDto standbyGroupResult = null;

		standbyGroupResult = (StandbyGroupDto) standbyGroupController.saveStandbyGroup(standbyGroupDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());
	}

	@Test
	public void pushPositionOfUserTest() {
		Long groupId = 1L;
		Integer pos = 1;

		List<UserInStandbyGroup> lsUserInStandbyGroup = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		uisg.setId(1L);
		uisg.setPosition(1);
		lsUserInStandbyGroup.add(uisg);

		UserInStandbyGroup uisg2 = new UserInStandbyGroup();
		uisg2.setId(2L);
		uisg2.setPosition(2);
		lsUserInStandbyGroup.add(uisg2);

		UserInStandbyGroup uisg3 = new UserInStandbyGroup();
		uisg3.setId(3L);
		uisg3.setPosition(3);
		lsUserInStandbyGroup.add(uisg3);

		// return list
		when(userInStandbyGroupRepository.findGroupMemberWithMinPosition(Mockito.anyLong(), Mockito.anyInt()))
				.thenReturn(lsUserInStandbyGroup);
		// when(userInStandbyGroupRepository.save(Mockito.any())).thenReturn(uisg);

		standbyGroupController.pushPositionOfUser(groupId, pos);
	}

	@Test
	public void saveStandbyGroup2Test() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroup.setId(1l);

		standbyGroup.setLsStandbyDurations(null);
		// standbyGroup.setLsStandbyLists(null);
		standbyGroup.setLsUserInStandbyGroups(null);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyGroup);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);

		// run test
		StandbyGroupDto standbyGroupResult = null;

		standbyGroupResult = (StandbyGroupDto) standbyGroupController.saveStandbyGroup(standbyGroupDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getStandbyGroupsTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroupList.json");
		List<StandbyGroup> lsStandbyGroupMock = (List<StandbyGroup>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYGROUP_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDtoMock = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDtoMock);
		when(standbyGroupRepository.findAllByOrderByTitleAsc()).thenReturn(lsStandbyGroupMock);

		// run test
		List<StandbyGroupDto> lsOutputUserDto = standbyGroupController.getStandbyGroups();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof StandbyGroupDto);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getStandbyGroupSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroupList.json");
		List<StandbyGroup> lsStandbyGroupMock = (List<StandbyGroup>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYGROUP_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupDtoMock = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDtoMock);
		when(standbyGroupRepository.findAllByOrderByTitleAsc()).thenReturn(lsStandbyGroupMock);

		// run test
		List<StandbyGroupSelectionDto> lsOutputUserDto = standbyGroupController.getStandbyGroupsSelection();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof StandbyGroupSelectionDto);
	}

	@Test
	public void getStandbyGroup() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroupMock = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroupMock.setId(id);
		StandbyGroupDto standbyGroupDtoMock = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDtoMock.setId(id);
		List<UserInStandbyGroupDto> lsUserInStandbyGroupDto = new ArrayList<>();
		UserInStandbyGroupDto uisg2 = new UserInStandbyGroupDto();
		uisg2.setPosition(2);
		lsUserInStandbyGroupDto.add(uisg2);
		UserInStandbyGroupDto uisg = new UserInStandbyGroupDto();
		uisg.setPosition(1);
		lsUserInStandbyGroupDto.add(uisg);
		standbyGroupDtoMock.setLsUserInStandbyGroups(lsUserInStandbyGroupDto);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDtoMock);
		when(standbyGroupRepository.findOne(id)).thenReturn(standbyGroupMock);

		// run test
		StandbyGroupDto standbyGroupDto = standbyGroupController.getStandbyGroup(id);
		assertNotNull(standbyGroupDto);
		assertTrue(standbyGroupDto.getId() == id);
	}

	@Test
	public void validateEntitysTest() {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		List list = new ArrayList();
		list.add(standbyGroup);

		when(standbyGroupRepository.findOne(standbyGroup.getId())).thenReturn(standbyGroup);

		List resultllist = standbyGroupController.validateEntitys(list, standbyGroupRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLogTest() {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		List list = new ArrayList();
		list.add(standbyGroup);

		when(standbyGroupRepository.findOne(standbyGroup.getId())).thenReturn(standbyGroup);

		List resultllist = standbyGroupController.validateEntitys(list, standbyGroupRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLog2Test() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		standbyGroup.setId(null);

		List list = new ArrayList();
		list.add(standbyGroup);

		when(standbyGroupRepository.findOne(standbyGroup.getId())).thenReturn(standbyGroup);

		List resultllist = standbyGroupController.validateEntitys(list, standbyGroupRepository, logger);

		assertNotNull(resultllist);
	}

	@Test
	public void saveBranchesForStandbyGroupTest() {
		// prepare data
		StandbyGroup standbyGroup = new StandbyGroup();

		List<BranchDto> lsBranchDtos = new ArrayList<>();
		lsBranchDtos.add(new BranchDto());

		List<Branch> lsBranch = new ArrayList<>();
		Branch b = new Branch();
		lsBranch.add(b);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(branchRepository.findOne(any())).thenReturn(b);
		when(branchRepository.save(any(Branch.class))).thenReturn(b);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsBranchDtos);

		// run test

		standbyGroupController.saveBranchesForStandbyGroup(1l, lsBranchDtos);

	}

	@Test
	public void deleteBranchesForStandbyGroup() {
		// prepare data
		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		StandbyGroup standbyGroup = new StandbyGroup();
		lsStandbyGroups.add(standbyGroup);

		List<BranchDto> lsBranchDtos = new ArrayList<>();
		lsBranchDtos.add(new BranchDto());

		List<Branch> lsBranch = new ArrayList<>();
		Branch b = new Branch();
		b.setLsStandbyGroups(lsStandbyGroups);
		lsBranch.add(b);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(branchRepository.findOne(any())).thenReturn(b);
		when(branchRepository.save(any(Branch.class))).thenReturn(b);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsBranchDtos);

		// run test

		standbyGroupController.deleteBranchesForStandbyGroup(1l, lsBranchDtos);

	}

	@Test
	public void saveDurationListTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		// StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json,
		// StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testStandbyDurationDtoList.json");
		List<StandbyDurationDto> lsDtos = (List<StandbyDurationDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYDURATION_DTO_ARRAY_LIST);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyDurationRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(validationController.startValidation(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any())).thenReturn(new ArrayList<PlanningMsgDto>());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsDtos);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(standbyGroup.getLsStandbyDurations().get(0));

		// run test

		StandbyDurationResponseDto result = standbyGroupController.saveDurationList(standbyGroup.getId(), lsDtos);

		assertEquals(3, result.getData().size());

	}

	@Test(expected = SpException.class)
	public void saveDurationListErrorTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		// StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json,
		// StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testStandbyDurationDtoList.json");
		List<StandbyDurationDto> lsDtos = (List<StandbyDurationDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYDURATION_DTO_ARRAY_LIST);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyDurationRepository.exists(Mockito.anyLong())).thenReturn(false);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsDtos);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(standbyGroup.getLsStandbyDurations().get(0));

		// run test

		StandbyDurationResponseDto result = standbyGroupController.saveDurationList(standbyGroup.getId(), lsDtos);

		assertEquals(3, result.getData().size());

	}

	@Test
	public void deleteDurationListTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testStandbyDurationDtoList.json");
		List<StandbyDurationDto> lsStandbyDurationsDto = (List<StandbyDurationDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYDURATION_DTO_ARRAY_LIST);

		List<StandbyDuration> lsStandbyDurations = standbyGroup.getLsStandbyDurations();

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(standbyDurationRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyDurationRepository.findOne(Mockito.anyLong())).thenReturn(lsStandbyDurations.get(0));

		// run test
		List<StandbyDurationDto> resultList = standbyGroupController.deleteDurationList(standbyGroupDto.getId(),
				lsStandbyDurationsDto);

		assertEquals(0, resultList.size());

	}

	@Test(expected = SpException.class)
	public void deleteDurationListErrorTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testStandbyDurationDtoList.json");
		List<StandbyDurationDto> lsStandbyDurationsDto = (List<StandbyDurationDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYDURATION_DTO_ARRAY_LIST);

		List<StandbyDuration> lsStandbyDurations = standbyGroup.getLsStandbyDurations();

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(standbyDurationRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyDurationRepository.findOne(Mockito.anyLong())).thenReturn(lsStandbyDurations.get(0));

		// run test
		List<StandbyDurationDto> resultList = standbyGroupController.deleteDurationList(standbyGroupDto.getId(),
				lsStandbyDurationsDto);

		assertEquals(0, resultList.size());

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(standbyDurationRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyDurationRepository.findOne(Mockito.anyLong())).thenReturn(lsStandbyDurations.get(0));

		// run test
		resultList = standbyGroupController.deleteDurationList(standbyGroupDto.getId(), lsStandbyDurationsDto);

		assertEquals(0, resultList.size());

	}

	@Test
	public void saveUserListTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		// StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json,
		// StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testUserInStandbyGroupDtoList.json");
		List<UserInStandbyGroupDto> lsDtos = (List<UserInStandbyGroupDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		// UserInStandbyGroup userInStandbyGroup = (UserInStandbyGroup) new
		// Gson().fromJson(json,
		// UserInStandbyGroup.class);
		// userInStandbyGroup.setId(30L);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(userRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userRepository.findOne(any())).thenReturn(user);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsDtos);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		// run test

		UserInStandbyGroupResponseDto result = standbyGroupController.saveUserList(standbyGroup.getId(), lsDtos);

		assertNotNull(result);
		assertEquals(0, result.getLsMsg().size());

	}

	@Test
	public void saveUserListTest1() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		// StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json,
		// StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testUserInStandbyGroupDtoList.json");
		List<UserInStandbyGroupDto> lsDtos = (List<UserInStandbyGroupDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		UserInStandbyGroup userInStandbyGroup = (UserInStandbyGroup) new Gson().fromJson(json,
				UserInStandbyGroup.class);
		userInStandbyGroup.setId(30L);
		userInStandbyGroup.setPosition(15);

		// define rules
		when(userInStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userInStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(userInStandbyGroup);
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(userRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userRepository.findOne(any())).thenReturn(user);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsDtos);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		// run test

		UserInStandbyGroupResponseDto result = standbyGroupController.saveUserList(standbyGroup.getId(), lsDtos);
		assertNotNull(result);
		assertEquals(0, result.getLsMsg().size());

	}

	@Test(expected = SpException.class)
	public void saveUserListErrorTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		// StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json,
		// StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testUserInStandbyGroupDtoList.json");
		List<UserInStandbyGroupDto> lsDtos = (List<UserInStandbyGroupDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		UserInStandbyGroup userInStandbyGroup = (UserInStandbyGroup) new Gson().fromJson(json,
				UserInStandbyGroup.class);
		userInStandbyGroup.setId(30L);

		// define rules
		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(standbyGroup);
		when(userRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(userRepository.findOne(any())).thenReturn(user);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsDtos);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		// run test
		UserInStandbyGroupResponseDto result = standbyGroupController.saveUserList(standbyGroup.getId(), lsDtos);
		assertNotNull(result);
		assertEquals(2, result.getLsMsg().size());

	}

	@Test
	public void deleteUserListTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testUserInStandbyGroupDtoList.json");
		List<UserInStandbyGroupDto> lsUserInStandbyGroupDto = (List<UserInStandbyGroupDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST);

		List<UserInStandbyGroup> lsUserInStandbyGroups = standbyGroup.getLsUserInStandbyGroups();
		for (UserInStandbyGroup userInGroup : lsUserInStandbyGroups) {
			User user = new User();
			user.setId(1L);
			userInGroup.setUser(user);
		}

		json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body = new Gson().fromJson(json, StandbyScheduleBody.class);
		List<StandbyScheduleBody> lsBodies = new ArrayList<StandbyScheduleBody>();
		lsBodies.add(body);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(userInStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userInStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(lsUserInStandbyGroups.get(0));
		when(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(Mockito.anyLong(), Mockito.anyLong(),
				Mockito.any(), Mockito.any(), Mockito.anyLong())).thenReturn(lsBodies);

		// run test
		UserInStandbyGroupResponseDto resultDto = standbyGroupController.deleteUserList(standbyGroupDto.getId(),
				lsUserInStandbyGroupDto);

		assertNotNull(resultDto);
		assertEquals(2, resultDto.getLsMsg().size());

	}

	@Test(expected = SpException.class)
	public void deleteUserListErrorTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		json = fh.loadStringFromResource("testUserInStandbyGroupDtoList.json");
		List<UserInStandbyGroupDto> lsUserInStandbyGroupDto = (List<UserInStandbyGroupDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST);

		List<UserInStandbyGroup> lsStandbyDurations = standbyGroup.getLsUserInStandbyGroups();

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(userInStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userInStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(lsStandbyDurations.get(0));

		// run test
		UserInStandbyGroupResponseDto resultDto = standbyGroupController.deleteUserList(standbyGroupDto.getId(),
				lsUserInStandbyGroupDto);

		assertNotNull(resultDto);
		assertEquals(0, resultDto.getLsMsg().size());

	}

	@Test
	public void saveUserFunctionListTest() {
		Long standbyGroupId = 1l;

		UserFunctionSelectionDto dto = new UserFunctionSelectionDto();
		dto.setFunctionId(1l);

		ArrayList<UserFunctionSelectionDto> lsUfsDto = new ArrayList<>();
		lsUfsDto.add(dto);

		UserFunction uf = new UserFunction();
		uf.setId(1l);

		ArrayList<UserFunction> lsUfs = new ArrayList<>();
		lsUfs.add(uf);

		StandbyGroup group = new StandbyGroup();
		group.setId(1l);
		group.setLsUserFunction(lsUfs);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(group);
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userFunctionRepository.findOne(any())).thenReturn(uf);
		when(userFunctionRepository.save(any(UserFunction.class))).thenReturn(uf);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(group.getLsUserFunction().get(0));
		Mockito.doNothing().when(standbyGroupRepository).flush();

		// run test
		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test 1
		standbyGroupId = null;
		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 2
		standbyGroupId = null;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 3
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 4
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		lsUfsDto.get(0).setId(null);

		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 5
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(false);
		lsUfsDto.get(0).setId(1l);
		try {
			standbyGroupController.saveUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void deleteUserFunctionList() {
		Long standbyGroupId = 1l;

		UserFunctionSelectionDto dto = new UserFunctionSelectionDto();
		dto.setFunctionId(1l);

		ArrayList<UserFunctionSelectionDto> lsUfsDto = new ArrayList<>();
		lsUfsDto.add(dto);

		UserFunction uf = new UserFunction();
		uf.setId(1l);

		ArrayList<UserFunction> lsUfs = new ArrayList<>();
		lsUfs.add(uf);

		StandbyGroup group = new StandbyGroup();
		group.setId(1l);
		group.setLsUserFunction(lsUfs);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(group);
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(userFunctionRepository.findOne(any())).thenReturn(uf);
		when(userFunctionRepository.save(any(UserFunction.class))).thenReturn(uf);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(group.getLsUserFunction().get(0));
		Mockito.doNothing().when(standbyGroupRepository).flush();

		// run test
		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test 1
		standbyGroupId = null;
		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 2
		standbyGroupId = null;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 3
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 4
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		lsUfsDto.get(0).setFunctionId(null);

		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 5
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(false);
		lsUfsDto.get(0).setFunctionId(1l);
		try {
			standbyGroupController.deleteUserFunctionList(standbyGroupId, lsUfsDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void saveRegionsForStandbyGroupTest() {
		Long standbyGroupId = 1l;

		RegionSelectionDto dto = new RegionSelectionDto();
		dto.setId(1l);

		Region r = new Region();
		r.setId(1l);

		ArrayList<RegionSelectionDto> lsRegionSelectionDto = new ArrayList<>();
		lsRegionSelectionDto.add(dto);

		StandbyGroup group = new StandbyGroup();
		group.setId(1l);
		group.getLsRegions().add(r);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(group);
		when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(regionRepository.findOne(any())).thenReturn(r);
		when(regionRepository.save(any(Region.class))).thenReturn(r);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(group.getLsRegions().get(0));
		Mockito.doNothing().when(standbyGroupRepository).flush();

		// run test
		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test 1
		standbyGroupId = null;
		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 2
		standbyGroupId = null;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 3
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 4
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		lsRegionSelectionDto.get(0).setId(null);

		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 5
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(false);
		lsRegionSelectionDto.get(0).setId(1l);
		try {
			standbyGroupController.saveRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void deleteRegionsForStandbyGroupTest() {
		Long standbyGroupId = 1l;

		RegionSelectionDto dto = new RegionSelectionDto();
		dto.setId(1l);

		Region r = new Region();
		r.setId(1l);

		ArrayList<RegionSelectionDto> lsRegionSelectionDto = new ArrayList<>();
		lsRegionSelectionDto.add(dto);

		StandbyGroup group = new StandbyGroup();
		group.setId(1l);
		group.getLsRegions().add(r);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(group);
		when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(regionRepository.findOne(any())).thenReturn(r);
		when(regionRepository.save(any(Region.class))).thenReturn(r);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn(group.getLsRegions().get(0));
		Mockito.doNothing().when(standbyGroupRepository).flush();

		// run test
		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test 1
		standbyGroupId = null;
		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 2
		standbyGroupId = null;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 3
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 4
		standbyGroupId = 1l;
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		lsRegionSelectionDto.get(0).setId(null);

		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 5
		when(userFunctionRepository.exists(Mockito.anyLong())).thenReturn(false);
		lsRegionSelectionDto.get(0).setId(1l);
		try {
			standbyGroupController.deleteRegionsForStandbyGroup(standbyGroupId, lsRegionSelectionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void saveIgnoredCalendarDayList() {
		Long standbyGroupId = 1L;
		List<CalendarDayDto> lsCalendarDayDto = new ArrayList<>();
		CalendarDayDto cdDto = new CalendarDayDto();
		cdDto.setId(1L);
		lsCalendarDayDto.add(cdDto);

		// calendar
		CalendarDay cd = new CalendarDay();
		cd.setId(1L);
		List<CalendarDay> lsCalendarDays = new ArrayList<>();
		lsCalendarDays.add(cd);
		StandbyGroup group = new StandbyGroup();
		group.setId(1L);
		group.setLsIgnoredCalendarDays(lsCalendarDays);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		when(standbyGroupRepository.save(any(StandbyGroup.class))).thenReturn(group);

		when(calendarRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(calendarRepository.findOne(any())).thenReturn(cd);
		when(calendarRepository.save(any(CalendarDay.class))).thenReturn(cd);

		// success test
		try {
			standbyGroupController.saveIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// failure because of no standby group with given id
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// failure because of no calendar day with given id
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(calendarRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.saveIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void deleteIgnoredCalendarDayList() {
		Long standbyGroupId = 1L;
		List<CalendarDayDto> lsCalendarDayDto = new ArrayList<>();
		CalendarDayDto cdDto = new CalendarDayDto();
		cdDto.setId(1L);
		lsCalendarDayDto.add(cdDto);

		// calendar
		CalendarDay cd = new CalendarDay();
		cd.setId(1L);
		List<CalendarDay> lsCalendarDays = new ArrayList<>();
		lsCalendarDays.add(cd);
		StandbyGroup group = new StandbyGroup();
		group.setId(1L);
		group.setLsIgnoredCalendarDays(lsCalendarDays);

		// define rules
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(any())).thenReturn(group);
		Mockito.doNothing().when(standbyGroupRepository).delete(any(StandbyGroup.class));

		when(calendarRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(calendarRepository.findOne(any())).thenReturn(cd);
		Mockito.doNothing().when(calendarRepository).delete(any(CalendarDay.class));

		// success test
		try {
			standbyGroupController.deleteIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// failure because of no standby group with given id
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// failure because of no calendar day with given id
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(calendarRepository.exists(Mockito.anyLong())).thenReturn(false);
		try {
			standbyGroupController.deleteIgnoredCalendarDayList(standbyGroupId, lsCalendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void getUserListUniqueTest() throws SpException {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserList.json");
		List<User> lsUserMock = (List<User>) new Gson().fromJson(json, GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);
		List<UserDto> lsUserDtoMock = (List<UserDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USER_DTO_ARRAY_LIST);

		when(userInStandbyGroupRepository.findUniqueById(Mockito.anyLong())).thenReturn(lsUserMock);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsUserDtoMock);

		List<UserSmallSelectionDto> resultllist = standbyGroupController.getUserListUnique(1L);

		assertNotNull(resultllist);

	}

	@Test
	public void getUserListUnique2Test() throws SpException {

		List<Object[]> lsUserMock = new ArrayList<>();
		Object[] obj = { 1L, "lastname", "firstname" };
		lsUserMock.add(obj);

		when(userInStandbyGroupRepository.findUniqueGroupUser(Mockito.anyLong())).thenReturn(lsUserMock);

		List<UserSmallSelectionDto> resultllist = standbyGroupController.getUserListUnique(1L);

		assertNotNull(resultllist);

	}

	@Test
	public void getStandbyGroupsAndListsTest() throws SpException {

		// FileHelperTest fh = new FileHelperTest();
		// // create test data
		// String json = fh.loadStringFromResource("testUserList.json");
		// List<User> lsUserMock = (List<User>) new Gson().fromJson(json,
		// GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);
		// List<UserDto> lsUserDtoMock = (List<UserDto>) new Gson().fromJson(json,
		// GsonTypeHelper.JSON_TYPE_USER_DTO_ARRAY_LIST);
		//
		// when(userInStandbyGroupRepository.findUniqueById(Mockito.anyLong())).thenReturn(lsUserMock);
		// when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(),
		// (List<AbstractDto>) any(), any()))
		// .thenReturn(lsUserDtoMock);

		List<TransferGroupDto> resultllist = standbyGroupController.getStandbyGroupsAndLists();

		assertNotNull(resultllist);

	}

	@Test
	public void getUserInGroupListTest() {

		Long standbyGroupId = new Long(2);

		List<UserInStandbyGroupSelectionDto> listUserInStandbyGroupDto = new ArrayList<>();

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");

		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		// define rules

		when(standbyGroupRepository.findOne(any())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn((List<UserInStandbyGroupSelectionDto>) listUserInStandbyGroupDto);

		List<UserInStandbyGroupSelectionDto> resultllist = standbyGroupController.getUserInGroupList(standbyGroupId);
		assertNotNull(resultllist);

	}

	@Test
	public void copyStandbyGroupTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroup.setId(1l);

		standbyGroup.setLsStandbyDurations(null);
		// standbyGroup.setLsStandbyLists(null);
		standbyGroup.setLsUserInStandbyGroups(null);

		CopyDto copyDto = new CopyDto();
		copyDto.setCopyBranch(true);
		copyDto.setCopyCalendar(true);
		copyDto.setCopyDuration(true);
		copyDto.setCopyFunction(true);
		copyDto.setCopyRegion(true);
		copyDto.setCopyUser(true);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyGroup);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(copyController.copy(Mockito.any(), Mockito.any())).thenReturn(standbyGroupDto);

		// run test
		StandbyGroupDto standbyGroupResult = null;

		standbyGroupResult = (StandbyGroupDto) standbyGroupController.copy(1L, copyDto);

		assertNotNull(standbyGroupResult);
		assertEquals(1l, standbyGroupResult.getId().longValue());
	}
}
