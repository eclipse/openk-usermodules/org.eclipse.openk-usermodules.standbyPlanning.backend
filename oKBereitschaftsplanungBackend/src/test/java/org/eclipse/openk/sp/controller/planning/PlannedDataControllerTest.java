/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.controller.CalendarController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyScheduleArchiveDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class PlannedDataControllerTest {

	protected static final Logger LOGGER = Logger.getLogger(PlannedDataControllerTest.class);

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	CalendarController calendarController;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Spy
	@InjectMocks
	PlannedDataController plannedDataController;

	@Test(expected = SpException.class)
	public void getFilteredPlanException1Test() throws SpException {

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 12, 31));
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));

		StandbyScheduleDto result = plannedDataController.getFilteredPlanByStatus(filter, SpMsg.STATUS_PLANNING, true);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanByStatusForArchiveException2Test() throws SpException {

		StandbyScheduleArchiveDto result = plannedDataController.getFilteredPlanByStatusForArchive(null,
				SpMsg.STATUS_PLANNING);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanByStatusForArchiveException1Test() throws SpException {

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 12, 31));
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));

		StandbyScheduleArchiveDto result = plannedDataController.getFilteredPlanByStatusForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(result);
	}

	@Test
	public void getFilteredPlanByStatusByGroupsForArchive1Test() throws SpException {

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 12, 31));
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));
		filter.setComment("test");

		List<TransferGroupDto> listTransferGroupDto = new ArrayList<>();
		filter.setLsTransferGroup(listTransferGroupDto);

		TransferGroupDto trsfGroup = new TransferGroupDto();
		trsfGroup.setGroupId(new Long(1));
		listTransferGroupDto.add(trsfGroup);

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setTitle("title");
		standbyGroup.setId(new Long(1));

		StandbyGroupSelectionDto sbgDto = new StandbyGroupSelectionDto();
		sbgDto.setId(new Long(1));

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) sbgDto);

		StandbyScheduleArchiveDto result = plannedDataController.getFilteredPlanByStatusByGroupsForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanByStatusByGroupsForArchive2Test() throws SpException {

		StandbyScheduleFilterDto filter = null;

		List<TransferGroupDto> listTransferGroupDto = new ArrayList<>();

		TransferGroupDto trsfGroup = new TransferGroupDto();
		trsfGroup.setGroupId(new Long(1));
		listTransferGroupDto.add(trsfGroup);

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setTitle("title");
		standbyGroup.setId(new Long(1));

		StandbyGroupSelectionDto sbgDto = new StandbyGroupSelectionDto();
		sbgDto.setId(new Long(1));

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) sbgDto);

		StandbyScheduleArchiveDto result = plannedDataController.getFilteredPlanByStatusByGroupsForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanByStatusByGroupsForArchive3Test() throws SpException {

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 12, 31));
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));
		filter.setComment("test");

		List<TransferGroupDto> listTransferGroupDto = null;
		filter.setLsTransferGroup(listTransferGroupDto);

		TransferGroupDto trsfGroup = new TransferGroupDto();
		trsfGroup.setGroupId(new Long(1));

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setTitle("title");
		standbyGroup.setId(new Long(1));

		StandbyGroupSelectionDto sbgDto = new StandbyGroupSelectionDto();
		sbgDto.setId(new Long(1));

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) sbgDto);

		StandbyScheduleArchiveDto result = plannedDataController.getFilteredPlanByStatusByGroupsForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanException2Test() throws SpException {

		StandbyScheduleDto result = plannedDataController.getFilteredPlanByStatus(null, SpMsg.STATUS_PLANNING, true);
		assertNotNull(result);
	}

	@Test(expected = SpException.class)
	public void getFilteredPlanExceptionTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupSelectionDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		List<StandbyGroupSelectionDto> listGroupDto = new ArrayList<StandbyGroupSelectionDto>();
		listGroupDto.add(standbyGroupSelectionDto);
		StandbyGroup standbyGroup = new StandbyGroup();

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(null);
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));

		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupSelectionDto);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenThrow(new ArithmeticException("test"));

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		StandbyScheduleDto plan = plannedDataController.getFilteredPlanByStatus(filter, SpMsg.STATUS_PLANNING, true);
		assertNotNull(plan);
	}

	@Test
	public void getFilteredPlanTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		json = fh.loadStringFromResource("testStandbyScheduleBodyDto.json");
		StandbyScheduleBodySelectionDto bodyDto = (StandbyScheduleBodySelectionDto) new Gson().fromJson(json,
				StandbyScheduleBodySelectionDto.class);

		json = fh.loadStringFromResource("testStandbyScheduleBodySearchList.json");
		List<StandbyScheduleBody> lsStandbyScheduleBody = (List<StandbyScheduleBody>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYBODY_ARRAY_LIST);

		List<StandbyScheduleBodySelectionDto> listBodyDto = (List<StandbyScheduleBodySelectionDto>) new Gson()
				.fromJson(json, GsonTypeHelper.JSON_TYPE_STANDBYBODY_SELECTION_DTO_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupSelectionDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		List<StandbyGroupSelectionDto> listGroupDto = new ArrayList<StandbyGroupSelectionDto>();
		listGroupDto.add(standbyGroupSelectionDto);

		StandbyGroup standbyGroup = new StandbyGroup(1L);

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 10, 10));
		filter.setValidTo(DateHelper.getDate(2018, 10, 12));

		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) bodyDto);
//		when(entityConverter.convertEntityToDtoList((List<StandbyListHasStandbyGroup>) any(), (List<AbstractDto>) any(),
//				StandbyGroupSelectionDto.class)).thenReturn(listGroupDto);

		Mockito.doReturn(listGroupDto).when(plannedDataController).mapGroupDto(any());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listBodyDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyScheduleBodyRepository.findByGroupsAndDateAndStatus(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any())).thenReturn(lsStandbyScheduleBody);

		StandbyScheduleDto plan = plannedDataController.getFilteredPlanByStatus(filter, SpMsg.STATUS_PLANNING, true);
		assertNotNull(plan);

	}

	@Test
	public void getFilteredPlanForArchiveTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		json = fh.loadStringFromResource("testStandbyScheduleBodySearchList.json");
		List<StandbyScheduleBody> lsStandbyScheduleBody = (List<StandbyScheduleBody>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYBODY_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyScheduleBodyDto.json");
		StandbyScheduleBodySelectionDto bodyDto = (StandbyScheduleBodySelectionDto) new Gson().fromJson(json,
				StandbyScheduleBodySelectionDto.class);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupSelectionDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		List<StandbyGroupSelectionDto> listGroupDto = new ArrayList<StandbyGroupSelectionDto>();
		listGroupDto.add(standbyGroupSelectionDto);
		StandbyGroup standbyGroup = new StandbyGroup();

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 10, 10));
		filter.setValidTo(DateHelper.getDate(2018, 10, 12));

		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) bodyDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listGroupDto);

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyScheduleBodyRepository.findByGroupsAndDateAndStatus(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any())).thenReturn(lsStandbyScheduleBody);

		StandbyScheduleArchiveDto plan = plannedDataController.getFilteredPlanByStatusForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(plan);

	}

	@Test(expected = SpException.class)
	public void getFilteredPlanForArchiveExceptionTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupSelectionDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		List<StandbyGroupSelectionDto> listGroupDto = new ArrayList<StandbyGroupSelectionDto>();
		listGroupDto.add(standbyGroupSelectionDto);
		StandbyGroup standbyGroup = new StandbyGroup();

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(1l);
		filter.setValidFrom(DateHelper.getDate(2017, 12, 31));
		filter.setValidTo(DateHelper.getDate(2018, 1, 3));

		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyGroupSelectionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenThrow(new ArithmeticException("test"));

		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		StandbyScheduleArchiveDto plan = plannedDataController.getFilteredPlanByStatusForArchive(filter,
				SpMsg.STATUS_PLANNING);
		assertNotNull(plan);

	}

	@Test
	public void setStyleTest() throws SpException {

		PlanRowsDto dto = new PlanRowsDto();
		Date date = DateHelper.getDate(2017, 12, 30);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_NOT_WORKING_DAY, dto.getStyle());

		dto.setStyle(null);
		date = DateHelper.getDate(2017, 12, 31);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_NOT_WORKING_DAY, dto.getStyle());

		dto.setStyle(null);
		date = DateHelper.getDate(2018, 1, 3);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_WORKING_DAY, dto.getStyle());

		dto.setStyle(null);
		when(calendarController.containsDate(Mockito.any())).thenReturn(true);
		date = DateHelper.getDate(2018, 1, 3);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_NOT_WORKING_DAY, dto.getStyle());

		dto.setStyle(null);
		when(calendarController.containsDate(Mockito.any())).thenReturn(false);
		date = DateHelper.getDate(2018, 1, 3);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_WORKING_DAY, dto.getStyle());

		when(calendarController.containsDate(Mockito.any())).thenReturn(false);
		date = DateHelper.getDate(2018, 1, 3);
		plannedDataController.setStyle(dto, date);
		assertEquals(SpMsg.TXT_WORKING_DAY, dto.getStyle());

	}

	@Test
	public void getPlannedBodysForGroupAndDayTest() {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		Date dateIndex = new Date();

		List<StandbyScheduleBody> listBodies = plannedDataController
				.getPlannedBodysForGroupAndDayPlanning(standbyGroupDto, dateIndex);
		assertNotNull(listBodies);

		listBodies = plannedDataController.getPlannedBodysForGroupAndDayClosedPlanning(standbyGroupDto, dateIndex);
		assertNotNull(listBodies);

		listBodies = plannedDataController.getPlannedBodysForGroupAndDayAndStatus(standbyGroupDto, dateIndex,
				SpMsg.STATUS_CLOSED_PLANNING);
		assertNotNull(listBodies);

		listBodies = plannedDataController.getPlannedBodysForGroupAndDayAndStatus(standbyGroupDto, dateIndex,
				SpMsg.STATUS_PLANNING);
		assertNotNull(listBodies);
	}

	@Test
	public void getIdsOfExistingScheduleBodiesTest() {

		StandbyScheduleBlueprintDto dto = new StandbyScheduleBlueprintDto();

		List<Long> result = plannedDataController.getIdsOfExistingScheduleBodies(dto);
		assertNotNull(result);
	}

	@Test
	public void getPlannedBodysForGroupIdAndIntervallAndStatusTest() {

		List<StandbyScheduleBody> result;
		Long standbyGroupId = new Long(1);
		Date validFrom = new Date();
		Date validTo = new Date();
		Long statusId = new Long(2);

		result = plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(standbyGroupId, validFrom,
				validTo, statusId);
		assertNotNull(result);
	}

}
