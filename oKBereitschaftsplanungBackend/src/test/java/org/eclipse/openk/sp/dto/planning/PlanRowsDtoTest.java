/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class PlanRowsDtoTest {

	@InjectMocks
	PlanRowsDto planRowsDto = new PlanRowsDto();

	@Test
	public void getSet() {
		String text = "test";

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBodySelectionDto bodyDto = (StandbyScheduleBodySelectionDto) new Gson().fromJson(json,
				StandbyScheduleBodySelectionDto.class);

		planRowsDto.setLabel(text);
		assertEquals(text, planRowsDto.getLabel());

		planRowsDto.setStyle(text);
		assertEquals(text, planRowsDto.getStyle());

		List<List<StandbyScheduleBodySelectionDto>> listGroupBodys = new ArrayList<>();
		List<StandbyScheduleBodySelectionDto> list = new ArrayList<>();

		list.add(bodyDto);
		listGroupBodys.add(list);

		planRowsDto.setListGroupBodys(listGroupBodys);
		assertNotNull(planRowsDto.getListGroupBodys());
		assertEquals(1, planRowsDto.getListGroupBodys().size());

		boolean result = planRowsDto.addStandbyBody(null);
		assertEquals(false, result);

		result = planRowsDto.addStandbyBody(bodyDto);
		assertEquals(false, result);

	}

	@Test
	public void mapTest() {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupSelectionDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);

		json = fh.loadStringFromResource("testStandbyScheduleBodyDto.json");
		StandbyScheduleBodySelectionDto bodyDto = (StandbyScheduleBodySelectionDto) new Gson().fromJson(json,
				StandbyScheduleBodySelectionDto.class);

		List<StandbyGroupSelectionDto> listGroupDto = new ArrayList<StandbyGroupSelectionDto>();
		listGroupDto.add(standbyGroupSelectionDto);

		planRowsDto = new PlanRowsDto(listGroupDto);

		planRowsDto.addStandbyBody(bodyDto);

		List<StandbyScheduleBodySelectionDto> result = planRowsDto.getGroupList(standbyGroupSelectionDto.getId());

		assertEquals(1, result.size());

	}

}
