/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserHasUserFunctionDto;
import org.eclipse.openk.sp.dto.UserInRegionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class UserRestServiceTest {

	@InjectMocks
	private UserRestService userRestService;

	@Mock
	private UserController userController;

	public UserRestServiceTest() {

	}

	@Test
	public void getUsersTest() {
		String jwt = "LET-ME-IN";
		Response result = userRestService.getUsers(jwt);
		assertNotNull(result);
	}

	@Test
	public void getUsersSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = userRestService.getUsersSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getUserTest() {
		String jwt = "LET-ME-IN";
		Response result = userRestService.getUser(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveUserTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);

		String jwt = "LET-ME-IN";
		Response result = userRestService.saveUser(jwt, userDto);
		assertNotNull(result);
	}

	@Test
	public void saveRegionsForUserTest() {

		List<UserInRegionDto> lsUserInRegionDto = new ArrayList<>();
		lsUserInRegionDto.add(new UserInRegionDto());

		String jwt = "LET-ME-IN";
		Response result = userRestService.saveRegionsForUser(1l, jwt, lsUserInRegionDto);
		assertNotNull(result);
	}

	@Test
	public void getFilteredUsersSelectionTest() {

		String jwt = "LET-ME-IN";
		Response result = userRestService.getFilteredUsersSelection(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void deleteRegionsForUserTest() {

		List<UserInRegionDto> lsUserInRegionDto = new ArrayList<>();
		lsUserInRegionDto.add(new UserInRegionDto());

		String jwt = "LET-ME-IN";
		Response result = userRestService.deleteRegionsForUser(1l, jwt, lsUserInRegionDto);
		assertNotNull(result);
	}

	@Test
	public void saveFunctionsForUserTest() {

		List<UserHasUserFunctionDto> lsDto = new ArrayList<>();
		lsDto.add(new UserHasUserFunctionDto());

		String jwt = "LET-ME-IN";
		Response result = userRestService.saveFunctionsForUser(1l, jwt, lsDto);
		assertNotNull(result);
	}

	@Test
	public void deleteFunctionsForUserTest() {

		List<UserHasUserFunctionDto> lsDto = new ArrayList<>();
		lsDto.add(new UserHasUserFunctionDto());

		String jwt = "LET-ME-IN";
		Response result = userRestService.deleteFunctionsForUser(1l, jwt, lsDto);
		assertNotNull(result);
	}

	@Test
	public void getLocationsSearchListTest() {

		String jwt = "LET-ME-IN";

		Response result = userRestService.getUsersDropDownList(jwt);
		assertNotNull(result);
	}

}
