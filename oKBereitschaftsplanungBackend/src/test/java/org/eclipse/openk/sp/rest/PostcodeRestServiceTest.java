/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.PostcodeController;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class PostcodeRestServiceTest {

	@InjectMocks
	private PostcodeRestService postcodeService;

	@Mock
	private PostcodeController postcodeController;

	public PostcodeRestServiceTest() {

	}

	@Test
	public void getPostcodesTest() {
		String jwt = "LET-ME-IN";
		Response result = postcodeService.getPostcodes(jwt);
		assertNotNull(result);
	}
	
	@Test
	public void getPostcodesSelectionsTest() {
		String jwt = "LET-ME-IN";
		Response result = postcodeService.getPostcodesSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getPostcodeTest() {
		String jwt = "LET-ME-IN";
		Response result = postcodeService.getPostcode(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void savePostcodeTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testPostcode.json");
		PostcodeDto postcodeDto = (PostcodeDto) new Gson().fromJson(json, PostcodeDto.class);

		String jwt = "LET-ME-IN";
		Response result = postcodeService.savePostcode(jwt, postcodeDto);
		assertNotNull(result);
	}
	

}
