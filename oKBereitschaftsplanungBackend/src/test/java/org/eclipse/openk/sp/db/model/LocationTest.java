/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class LocationTest {

	@Test
	public void testGettersAndSetters() {

		Location l = new Location();

		l.setId(1l);
		assertEquals(1l, l.getId().longValue());

		l.setCommunity("community");
		assertEquals("community", l.getCommunity());
		
		l.setDistrict("district");
		assertEquals("district", l.getDistrict());


		l.setLatitudedistrict("latitudedistrict");
		assertEquals("latitudedistrict", l.getLatitudedistrict());

		l.setLongitudedistrict("longitudedistrict");
		assertEquals("longitudedistrict", l.getLongitudedistrict());

		List<Postcode> lsPostcode = new ArrayList<Postcode>();
		lsPostcode.add(new Postcode("33100"));
		lsPostcode.add(new Postcode("33102"));
		lsPostcode.add(new Postcode("33105"));
		l.setLsPostcode(lsPostcode);
		assertEquals(lsPostcode.size(), l.getLsPostcode().size());

		List<Region> lsRegions = new ArrayList<>();
		Region region = new Region();
		lsRegions.add(region);
		l.setLsRegions(lsRegions);
		assertEquals(lsRegions.size(), l.getLsRegions().size());

		l.setShorttext("shorttext");
		assertEquals("shorttext", l.getShorttext());

		l.setTitle("title");
		assertEquals("title", l.getTitle());

		l.setWgs84zonedistrict("wgs84zonedistrict");
		assertEquals("wgs84zonedistrict", l.getWgs84zonedistrict());

	}

	@Test
	public void testCompare() throws MalformedURLException {
		Location obj1 = new Location();
		obj1.setId(5L);

		Location obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		Location obj3 = new Location();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

	@Test
	public void testConstructor() {
		Location obj1 = new Location();
		obj1.setId(5L);

		assertEquals(new Long(5), obj1.getId());

		Location obj2 = new Location(7L);
		assertEquals(new Long(7), obj2.getId());

	}
}
