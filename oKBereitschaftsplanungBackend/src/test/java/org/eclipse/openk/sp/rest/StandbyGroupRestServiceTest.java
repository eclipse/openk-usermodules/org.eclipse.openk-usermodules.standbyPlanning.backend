/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.StandbyGroupController;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class StandbyGroupRestServiceTest {

	@InjectMocks
	private StandbyGroupRestService standbyGroupRestService;

	@Mock
	private StandbyGroupController standbyGroupController;

	public StandbyGroupRestServiceTest() {

	}

	@Test
	public void getStandbyGroupsTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getStandbyGroups(jwt);
		assertNotNull(result);
	}

	@Test
	public void getStandbyGroupsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getStandbyGroupsSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getStandbyGroupTest() {
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getStandbyGroup(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveStandbyGroup(jwt, standbyGroupDto);
		assertNotNull(result);
	}

	@Test
	public void saveStandbyGroupUserListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<UserInStandbyGroupDto> listDto = standbyGroupDto.getLsUserInStandbyGroups();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveStandbyGroupUserList(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void deleteStandbyGroupUserListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<UserInStandbyGroupDto> listDto = standbyGroupDto.getLsUserInStandbyGroups();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteStandbyGroupUserList(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void saveBranchesForStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<BranchDto> listDto = standbyGroupDto.getLsBranches();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveBranchesForStandbyGroup(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void deleteBranchesForStandbyGroupTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<BranchDto> listDto = standbyGroupDto.getLsBranches();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteBranchesOfStandbyGroup(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void saveStandbyGroupDurationListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<StandbyDurationDto> listDto = standbyGroupDto.getLsStandbyDurations();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveStandbyGroupDurationList(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void deleteStandbyGroupDurationListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<StandbyDurationDto> listDto = standbyGroupDto.getLsStandbyDurations();

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteStandbyGroupDurationList(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void saveStandbyGroupUserFunctionListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<UserFunctionSelectionDto> listDto = new ArrayList<UserFunctionSelectionDto>();
		listDto.add(new UserFunctionSelectionDto());

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveFunctionsForGroup(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void deleteStandbyGroupUserFunctionListTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);

		Long standbyGroupId = standbyGroupDto.getId();
		List<UserFunctionSelectionDto> listDto = new ArrayList<UserFunctionSelectionDto>();
		listDto.add(new UserFunctionSelectionDto());

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteFunctionsFromGroup(standbyGroupId, jwt, listDto);

		assertNotNull(result);
	}

	@Test
	public void saveRegionForGroupTest() {

		Long id = 1l;
		List<RegionSelectionDto> lsRegionSelectionDtos = new ArrayList<>();
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveRegionForGroup(id, jwt, lsRegionSelectionDtos);
		assertNotNull(result);
	}

	@Test
	public void deleteRegionForGroupTest() {

		Long id = 1l;
		List<RegionSelectionDto> lsRegionSelectionDtos = new ArrayList<>();
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteRegionForGroup(id, jwt, lsRegionSelectionDtos);
		assertNotNull(result);
	}

	@Test
	public void saveIgnoreCalendarDayList() {

		Long id = 1l;
		List<CalendarDayDto> lsDtos = new ArrayList<>();
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.saveIgnoreCalendarDayList(id, jwt, lsDtos);
		assertNotNull(result);
	}

	@Test
	public void deleteIgnoreCalendarDayList() {

		Long id = 1l;
		List<CalendarDayDto> lsDtos = new ArrayList<>();
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.deleteIgnoreCalendarDayList(id, jwt, lsDtos);
		assertNotNull(result);
	}

	@Test
	public void getStandbyGroupUserListUniqueTest() {

		Long id = 1l;
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getStandbyGroupUserListUnique(id, jwt);
		assertNotNull(result);

	}

	@Test
	public void getListsTest() {

		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getLists(jwt);
		assertNotNull(result);

	}

	@Test
	public void getStandbyGroupUserListTest() {
		Long id = 1l;
		String jwt = "LET-ME-IN";
		Response result = standbyGroupRestService.getStandbyGroupUserList(id, jwt);
		assertNotNull(result);

	}

	@Test
	public void copyDeepTest() {
		Long id = 1l;
		String jwt = "LET-ME-IN";
		CopyDto copyDto = new CopyDto();

		Response result = standbyGroupRestService.copyDeep(id, jwt, copyDto);
		assertNotNull(result);

	}
}
