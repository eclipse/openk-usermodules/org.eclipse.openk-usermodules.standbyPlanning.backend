/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.openk.sp.controller.planning.PlannedDataController;
import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.dto.report.ReportGroupDto;
import org.eclipse.openk.sp.dto.report.ReportInputDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class ReportGroupDtoConverterTest {

	@Mock
	private PlannedDataController plannedDataController;

	@Mock
	EntityConverter entityConverter;

	@Mock
	StandbyGroupRepository standbyGroupRepository;

	@InjectMocks
	public ReportGroupDtoConverter reportGroupDtoConverter;

	private ReportDto getReport() {
		ReportDto report = null;

		report = new ReportDto();
		report.setReportName("Monatsübersicht");
		report.setPrintFormat("pdf");
		report.setUserId(1L);
		report.setValidFromDate(new Date());
		report.setValidToDate(new Date());
		report.setStandByGroup(new StandbyGroup());
		report.setStandByList(new StandbyList());
		report.setDefaultDayRange(5);
		report.setReportLevel("Ist-Ebene");
		report.setGroupPosition(1);

		return report;
	}

	@Test
	public void userStringTest() {
		FileHelperTest fh = new FileHelperTest();

		StandbyScheduleBodySelectionDto sb = new StandbyScheduleBodySelectionDto();

		ReportDto reportDto = new ReportDto();
		reportDto.setReportName(ReportController.REPORT_MAX10_GROUPS_ONLY_NAMES);

		sb.setValidFrom(DateHelper.getDate(2019, 2, 11, 0, 0, 0));
		sb.setValidTo(DateHelper.getDate(2019, 2, 11, 8, 0, 0));

		String json = fh.loadStringFromResource("testUser.json");
		UserSmallSelectionDto user = (UserSmallSelectionDto) new Gson().fromJson(json, UserSmallSelectionDto.class);
		sb.setUser(user);

		String result = reportGroupDtoConverter.userString(null, sb, reportDto, 1);
		assertNotNull(result);

		result = reportGroupDtoConverter.userString(sb, sb, reportDto, 2);
		assertNotNull(result);

		reportDto.setReportName(ReportController.REPORT_MAX10_GROUPS);
		result = reportGroupDtoConverter.userString(null, sb, reportDto, 1);
		assertNotNull(result);

		sb.setUser(null);
		result = reportGroupDtoConverter.userString(null, sb, reportDto, 1);
		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void addUserStringTest() throws SpException {

		FileHelperTest fh = new FileHelperTest();
		ReportGroupDto reportGroupDto = new ReportGroupDto();
		StandbyScheduleBodySelectionDto sb = new StandbyScheduleBodySelectionDto();
		ReportDto reportDto = new ReportDto();
		Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap = new HashMap<>();

		reportDto.setReportName(ReportController.REPORT_MAX10_GROUPS_ONLY_NAMES);

		sb.setId(1L);
		sb.setValidFrom(new Date());
		sb.setValidTo(new Date());

		String json = fh.loadStringFromResource("testUser.json");
		UserSmallSelectionDto user = (UserSmallSelectionDto) new Gson().fromJson(json, UserSmallSelectionDto.class);
		sb.setUser(user);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto group = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		sb.setStandbyGroup(group);

		List<StandbyScheduleBodySelectionDto> dayGroupList = new ArrayList<>();
		dayGroupList.add(sb);

		reportGroupDtoConverter.addUserString(1, sb, reportGroupDto, reportDto, lastBodyMap, dayGroupList);

		reportGroupDtoConverter.addUserString(1, null, reportGroupDto, reportDto, lastBodyMap, dayGroupList);

	}

	@Test(expected = SpException.class)
	public void addUserStringErrorTest() throws SpException {

		FileHelperTest fh = new FileHelperTest();
		ReportGroupDto reportGroupDto = new ReportGroupDto();
		StandbyScheduleBodySelectionDto sb = new StandbyScheduleBodySelectionDto();
		ReportDto reportDto = new ReportDto();
		Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap = new HashMap<>();
		reportDto.setReportName(ReportController.REPORT_MAX10_GROUPS_ONLY_NAMES);

		sb.setValidFrom(new Date());
		sb.setValidTo(new Date());

		String json = fh.loadStringFromResource("testUser.json");
		UserSmallSelectionDto user = (UserSmallSelectionDto) new Gson().fromJson(json, UserSmallSelectionDto.class);
		sb.setUser(user);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroup = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		sb.setStandbyGroup(standbyGroup);

		List<StandbyScheduleBodySelectionDto> dayGroupList = new ArrayList<>();
		dayGroupList.add(sb);

		reportGroupDtoConverter.addUserString(123, sb, reportGroupDto, reportDto, lastBodyMap, dayGroupList);

	}

	@Test
	public void addReportRowTest() {

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleDto.json");
		StandbyScheduleDto plan = (StandbyScheduleDto) new Gson().fromJson(json, StandbyScheduleDto.class);

		ReportDto reportDto = getReport();
		Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap = new HashMap<>();
		List<ReportInputDto> inputDto = new ArrayList<>();

		Date dateIndex = DateHelper.getDate(2019, 2, 11);
		Long[] groupIdArray = new Long[1];
		groupIdArray[0] = new Long(1);

		ReportGroupDto result = null;

		try {

			result = reportGroupDtoConverter.addReportRow(reportDto, inputDto, plan, dateIndex, groupIdArray,
					lastBodyMap);

		} catch (Exception e) {
			assertNull(e);
		}
//
//		Date resultDate = result.getFromDate();
//
////		assertNotNull(result);
//		assertEquals(dateIndex, resultDate);
////		assertNotNull(result.getFromDate());
	}

	@Test
	public void addReportTest() throws SpException, ParseException {

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleDto.json");
		StandbyScheduleDto plan = (StandbyScheduleDto) new Gson().fromJson(json, StandbyScheduleDto.class);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup sb = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		StandbyGroupSelectionDto dto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);

		ReportDto reportDto = getReport();
		reportDto.setValidFromDate(new Date());
		reportDto.setValidToDate(new Date());
		reportDto.setStandByList(new StandbyList(plan.getFilter().getStandbyListId()));

		Mockito.when(
				plannedDataController.getFilteredPlanByStatus(Mockito.any(), Mockito.anyLong(), Mockito.anyBoolean()))
				.thenReturn(plan);
		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(sb);
		Mockito.when(entityConverter.convertEntityToDto(Mockito.any(), Mockito.any())).thenReturn(dto);

		List<ReportInputDto> result = reportGroupDtoConverter.get10Rows(reportDto);

		assertNotNull(result);
	}

	@Test
	public void checkEmptyTest() {
		FileHelperTest fh = new FileHelperTest();

		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup sb = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		StandbyGroupSelectionDto dto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);

		List<StandbyScheduleBodySelectionDto> dayGroupList = new ArrayList<>();
		Long groupId = 1L;

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(sb);
		Mockito.when(entityConverter.convertEntityToDto(Mockito.any(), Mockito.any())).thenReturn(dto);

		reportGroupDtoConverter.checkEmpty(dayGroupList, groupId);

	}

}
