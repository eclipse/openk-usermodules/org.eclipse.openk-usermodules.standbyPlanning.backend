/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StandbyListHasStandbyGroupTest {

	@Test
	public void testGettersAndSetters() {

		StandbyListHasStandbyGroup slhsg = new StandbyListHasStandbyGroup();
		Long id = 1L;

		StandbyGroup group = new StandbyGroup();

		slhsg.setId(id);
		assertEquals(id.longValue(), slhsg.getId().longValue());

		slhsg.setPosition(1);
		assertEquals(1, slhsg.getPosition().intValue());

		slhsg.setStandbyGroup(group);
		assertEquals(group, slhsg.getStandbyGroup());

		StandbyList standbyList = new StandbyList();
		slhsg.setStandbyList(standbyList);
		assertEquals(standbyList, slhsg.getStandbyList());

		assertEquals(1, slhsg.compareTo(slhsg));

		StandbyListHasStandbyGroup slhsg2 = new StandbyListHasStandbyGroup();
		slhsg2.setId(2L);
		assertEquals(0, slhsg.compareTo(slhsg2));

	}
}
