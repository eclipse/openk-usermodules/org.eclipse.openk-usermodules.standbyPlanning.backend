/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;

public class UserInStandByGroupTest {

	@Test
	public void testGettersAndSetters() {
		Integer i = new Integer(1);
		Date d = DateHelper.getDate(2018, 10, 1);
		UserInStandbyGroup u = new UserInStandbyGroup();
		StandbyGroup group = new StandbyGroup();
		User user = new User();

		UserInStandbyGroup userInStandbyGroup = new UserInStandbyGroup(2L);
		assertEquals(2L, userInStandbyGroup.getId().longValue());

		u.setId(1l);
		assertEquals(1l, u.getId().longValue());

		u.setValidFrom(d);
		assertEquals(d, u.getValidFrom());

		u.setValidTo(d);
		assertEquals(d, u.getValidTo());

		u.setStandbyGroup(group);
		assertEquals(group, u.getStandbyGroup());

		u.setUser(user);
		assertEquals(user, u.getUser());

		u.setPosition(i);
		assertEquals(i, u.getPosition());

		UserInStandbyGroup u2 = u.copy();

		assertEquals(null, u2.getId());
		assertEquals(d, u2.getValidFrom());
		assertEquals(d, u2.getValidTo());
		assertEquals(group, u2.getStandbyGroup());
		assertEquals(user, u2.getUser());
		assertEquals(i, u2.getPosition());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		UserInStandbyGroup obj1 = new UserInStandbyGroup();
		obj1.setId(5L);

		UserInStandbyGroup obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		UserInStandbyGroup obj3 = new UserInStandbyGroup();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
}
