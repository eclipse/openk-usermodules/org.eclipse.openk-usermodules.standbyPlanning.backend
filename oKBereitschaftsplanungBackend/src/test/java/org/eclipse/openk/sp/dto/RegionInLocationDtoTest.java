/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class RegionInLocationDtoTest {

	@Test
	public void testGettersAndSetters() {
		RegionInLocationDto r = new RegionInLocationDto();

		r.setId(1l);
		assertEquals(1l, r.getId().longValue());

		r.setRegionName("RegionDto");
		assertEquals("RegionDto", r.getRegionName());



		RegionHasFunctionDto rhf = new RegionHasFunctionDto();
		RegionHasFunctionDto rhf2 = new RegionHasFunctionDto();
		ArrayList<RegionHasFunctionDto> lsRegionHasFunctions = new ArrayList<>();
		lsRegionHasFunctions.add(rhf);
		lsRegionHasFunctions.add(rhf2);
		r.setLsRegionHasFunctions(lsRegionHasFunctions);
		assertNotNull(lsRegionHasFunctions);
		assertTrue(lsRegionHasFunctions.size() == 2);
		assertNotNull(r.getLsRegionHasFunctions());
		assertTrue(r.getLsRegionHasFunctions().size() == 2);

	}

}
