/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StandbyScheduleDtoTest {

	@InjectMocks
	StandbyScheduleDto standbyScheduleDto;

	@Test
	public void getSet() {
		StandbyScheduleDto dto = new StandbyScheduleDto();
		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		dto.setFilter(filter);
		assertEquals(filter, dto.getFilter());

		List<PlanRowsDto> rows = new ArrayList<>();
		dto.setListPlanRows(rows);
		assertEquals(rows, dto.getListPlanRows());

		PlanHeaderDto planHeader = new PlanHeaderDto();
		dto.setPlanHeader(planHeader);
		assertEquals(planHeader, dto.getPlanHeader());

		StandbyGroupSelectionDto sbg = new StandbyGroupSelectionDto();
		sbg.setTitle("title");
		sbg.setId(1L);

		List<StandbyGroupSelectionDto> list = new ArrayList<>();
		list.add(sbg);

		dto = new StandbyScheduleDto(DateHelper.getDate(2019, 02, 03), DateHelper.getDate(2019, 02, 05), list);

		PlanRowsDto result = dto.getRow(DateHelper.getDate(2019, 02, 04));
		assertNotNull(result);

		result = dto.getRow(DateHelper.getDate(2019, 02, 06));
		assertNull(result);

		result = dto.getRow(null);
		assertNull(result);

	}

	@Test
	public void countTest() {
		Long groupId = 1L;

		StandbyScheduleBodySelectionDto stbyBody = new StandbyScheduleBodySelectionDto();
		StandbyGroupSelectionDto sbg = new StandbyGroupSelectionDto();
		sbg.setTitle("title");
		sbg.setId(groupId);
		stbyBody.setStandbyGroup(sbg);

		int startSize = standbyScheduleDto.groupSize(groupId);
		standbyScheduleDto.count(stbyBody);
		int endSize = standbyScheduleDto.groupSize(groupId);

		assertNotEquals(startSize, endSize);

		standbyScheduleDto.count(stbyBody);
		endSize = standbyScheduleDto.groupSize(groupId);

		assertEquals(startSize + 2, endSize);

	}

}
