/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.ws.rs.core.Response;
import javax.xml.ws.http.HTTPException;

import org.apache.http.HttpStatus;
import org.eclipse.openk.core.controller.ResponseBuilderWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.HealthController;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HealthRestServiceTest {

	@Mock
	HealthController healthController;

	@InjectMocks
	HealthRestService healthRestService = new HealthRestService();

	@Test
	public void getVersionTest() {
		Response version = healthRestService.getVersion();
		assertNotNull(version);
		assertEquals(200, version.getStatus());
	}
	
	@Test
	public void getVersionExceptionTest() {
		
		Mockito.when(healthController.getVersion()).thenThrow(new HTTPException(HttpStatus.SC_SERVICE_UNAVAILABLE));
		
		Response version = healthRestService.getVersion();
		assertNull(version);
	
	}

	@Test
	public void getAppStatusTeste() throws HttpStatusException {
		Response response = ResponseBuilderWrapper.INSTANCE.buildOKResponse(SpExceptionMapper.getGeneralOKJson());
		
		Mockito.when(healthController.checkDB()).thenReturn(true);

		Response version = healthRestService.getAppStatus();
		assertNotNull(version);
		assertEquals(200, version.getStatus());
	}
	
	@Test 
	public void getAppStatusExceptionTest() throws HttpStatusException {
		
		Mockito.when(healthController.checkDB()).thenThrow(new HttpStatusException(HttpStatus.SC_BAD_REQUEST));

		Response version = healthRestService.getAppStatus();
		assertNull(version);
		
	}

}
