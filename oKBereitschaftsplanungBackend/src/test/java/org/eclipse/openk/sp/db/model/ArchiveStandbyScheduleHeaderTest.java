/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArchiveStandbyScheduleHeaderTest {

	@Test
	public void ArchiveStandbyScheduleHeaderTest() {
		Long id = new Long(3);
		String text = "test";
		Date d = new Date();

		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();

		header.setComment(text);
		assertEquals(text, header.getComment());

		header.setId(id);
		assertEquals(id, header.getId());

		header.setJsonPlan(text);
		assertEquals(text, header.getJsonPlan());

		header.setModificationDate(d);
		assertEquals(d, header.getModificationDate());

		header.setModifiedBy(text);
		assertEquals(text, header.getModifiedBy());

		header.setModifiedCause(text);
		assertEquals(text, header.getModifiedCause());

		header.setTitle(text);
		assertEquals(text, header.getTitle());

		header.setStatusName(text);
		assertEquals(text, header.getStatusName());

		header.setStatusId(id);
		assertEquals(id.longValue(), header.getStatusId().longValue());

		header.setValidFrom(d);
		assertEquals(d, header.getValidFrom());

		header.setValidTo(d);
		assertEquals(d, header.getValidTo());

	}

	@Test
	public void testCompare() throws MalformedURLException {
		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();
		header.setId(5L);

		ArchiveStandbyScheduleHeader header2 = header;
		assertEquals(1, header.compareTo(header2));

		ArchiveStandbyScheduleHeader header3 = new ArchiveStandbyScheduleHeader();
		header3.setId(6L);
		assertEquals(0, header.compareTo(header3));

	}

}
