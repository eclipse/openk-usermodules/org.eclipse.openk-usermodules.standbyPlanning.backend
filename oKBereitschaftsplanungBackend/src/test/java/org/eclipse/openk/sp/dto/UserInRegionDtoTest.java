/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class UserInRegionDtoTest {

	@Test
	public void testGettersAndSetters() {
		UserInRegionDto uir = new UserInRegionDto();
		uir.setId(1l);
		assertNotNull(uir.getId());
		assertTrue(uir.getId() == 1);

		uir.setUserId(1l);
		assertNotNull(uir.getUserId());
		assertTrue(uir.getUserId() == 1);

		uir.setRegionId(1l);
		assertNotNull(uir.getRegionId());
		assertTrue(uir.getRegionId() == 1);

		uir.setRegionName("Name");
		assertNotNull(uir.getRegionName());
		assertTrue(uir.getRegionName().equals("Name"));
	}
}
