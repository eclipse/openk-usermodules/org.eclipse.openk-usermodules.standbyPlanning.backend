/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest.filter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.Test;

public class ValidationFilterTest {

	ValidationFilter vf = new ValidationFilter();

	@Test
	public void initTest() {
		Throwable th = null;
		FilterConfig filterConfig = null;
		try {
			vf.init(filterConfig);
		} catch (ServletException e) {
			th = e;
		}
		assertNull(th);
	}

	@Test
	public void destroyTest() {
		vf.destroy();
	}

	@Test
	public void doFilterTest() {
		ServletRequest request = null;
		ServletResponse response = null;
		FilterChain chain = new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {
				// TODO Auto-generated method stub
				
			}
		};

		Throwable th = null;

		try {
			vf.doFilter(request, response, chain);
		} catch (IOException e) {
			th = e;
		} catch (ServletException e) {
			th = e;
		}

		assertNull(th);
	}
}
