/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.StandbyScheduleController;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.SpMsg;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StandbyScheduleRestServiceTest {

	@InjectMocks
	private StandbyScheduleRestService standbyScheduleRestService;

	@Mock
	private StandbyScheduleController sCo;

	@Test
	public void filterPlanningDataByStatusIdTest() {
		String jwt = "LET-ME-IN";
		Long statusId = SpMsg.STATUS_CLOSED_PLANNING;
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		Response result = standbyScheduleRestService.filterPlanningDataByStatusId(statusId, jwt,
				standbyScheduleFilterDto);
		assertNotNull(result);

	}

	@Test
	public void filterPlanningDataByStatusIdAsZipTest() {
		String jwt = "LET-ME-IN";
		Long statusId = SpMsg.STATUS_CLOSED_PLANNING;
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		Response result = standbyScheduleRestService.filterPlanningDataByStatusIdAsZip(statusId, jwt,
				standbyScheduleFilterDto);
		assertNotNull(result);

	}

	@Test
	public void calculateTest() {
		String jwt = "LET-ME-IN";

		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		Response result = standbyScheduleRestService.calculate(jwt, standbyBlueprintDto);
		assertNotNull(result);
	}

	@Test
	public void copyBodiesTest() {
		String jwt = "LET-ME-IN";

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		Response result = standbyScheduleRestService.copyBodies(jwt, standbyScheduleCopyDto);
		assertNotNull(result);
	}

	@Test
	public void replaceUserInPlanTest() {
		String jwt = "LET-ME-IN";

		StandbyScheduleActionDto replaceUserDto = new StandbyScheduleActionDto();
		Response result;
		try {
			result = standbyScheduleRestService.replaceUserInPlan(jwt, replaceUserDto);
			assertNotNull(result);
		} catch (SpException e) {
			assertNull(e);
		}

	}

	@Test
	public void moveUserInPlanTest() {
		String jwt = "LET-ME-IN";

		StandbyScheduleActionDto replaceUserDto = new StandbyScheduleActionDto();
		Response result;
		try {
			result = standbyScheduleRestService.moveUserInPlan(jwt, replaceUserDto);
			assertNotNull(result);
		} catch (SpException e) {
			assertNull(e);
		}
	}

	@Test
	public void importArchiveHeaderTest() {
		String jwt = "LET-ME-IN";

		Response result = standbyScheduleRestService.importArchiveHeader(1L, jwt);
		assertNotNull(result);
	}

	@Test
	public void searchnowTest() {
		String jwt = "LET-ME-IN";

		StandbyScheduleSearchDto searchDto = new StandbyScheduleSearchDto();
		Response result = standbyScheduleRestService.searchnow(jwt, searchDto);
		assertNotNull(result);
	}

	@Test
	public void deleteBodiesTest() {
		String jwt = "LET-ME-IN";
		Long statusId = SpMsg.STATUS_CLOSED_PLANNING;
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		Response result = standbyScheduleRestService.deleteBodies(statusId, jwt, standbyScheduleCopyDto);
		assertNotNull(result);
	}

}
