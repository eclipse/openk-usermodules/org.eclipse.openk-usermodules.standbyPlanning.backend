/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FileHelperTest {

	@InjectMocks
	FileHelper fh = new FileHelper();

	@Test
	public void getPropertyPositiveTest() throws IOException {
		// positive test
		String value = fh.getProperty("logging.properties", "handlers");
		assertNotNull(value);
		assertTrue(value.equals("org.slf4j.bridge.SLF4JBridgeHandler"));
	}

	@Test(expected = RuntimeException.class)
	public void getPropertyNegativeTest() throws IOException {
		// negative test
		String value = fh.getProperty("logging.properties", "NoPropertie");
		assertNull(value);
	}

	@Test
	public void loadPropertiesFromResourcePositiveTest() throws IOException {
		// positive test
		Properties properties = fh.loadPropertiesFromResource("logging.properties");
		assertNotNull(properties);

		String key = properties.getProperty("handlers");
		assertNotNull(key);
	}

	@Test
	public void loadPropertiesFromResourceNegativeTest() throws IOException {
		// negative test (no key in properties)
		Properties properties = fh.loadPropertiesFromResource("logging.properties");
		assertNotNull(properties);

		String key = properties.getProperty("handlers");
		assertNotNull(key);
	}

	@Test(expected = NullPointerException.class)
	public void loadPropertiesFromResourceNegative2Test() throws IOException {
		// negative test (no file found with the searched name)
		Properties property = fh.loadPropertiesFromResource("notExisting.properties");
		assertNotNull(property);
	}

	@Test
	public void loadFileFromResourcePositiveTest() throws IOException {
		// negative test (no file found with the searched name)
		InputStream inputStream = fh.loadFileFromResource("logging.properties");
		assertNotNull(inputStream);
	}

	@Test
	public void loadFileFromResourceNegativeTest() throws IOException {
		// negative test (no file found with the searched name)
		InputStream inputStream = fh.loadFileFromResource(null);
		assertNull(inputStream);
	}

	@Test
	public void loadStringFromResourceTest() {
		FileHelperTest fht = new FileHelperTest();
		String str = fht.loadStringFromResource("testCase.txt");
		assertNotNull(str);
	}

	@Test
	public void loadStringFromResourceURITest() {
		// ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		// Throwable toBeThrown = new URISyntaxException("Nothing", "Nothing");
		FileHelperTest fht = new FileHelperTest();
		String str = fht.loadStringFromResource("testCaseURI.txt");
		assertNotNull(str);
	}

	@Test
	public void loadStringFromResourceNegativTest() {
		FileHelperTest fht = new FileHelperTest();
		String str = fht.loadStringFromResource("UNKNOWN.txt");
		assertNull(str);
	}

	@Test
	public void loadFolderFromFileSystemOrResource() {
		File file = fh.loadFolderFromFileSystemOrResource("reports", true);
		assertNotNull(file);
		assertTrue(file.exists());

		fh.loadFolderFromFileSystemOrResource("reports", false);
		assertNotNull(file);
		assertTrue(file.exists());

		file = fh.loadFolderFromFileSystemOrResource("fehlschlag", true);
		assertNull(file);

		file = fh.loadFolderFromFileSystemOrResource("fehlschlag", false);
		assertNotNull(file);
		assertFalse(file.exists());
	}

	@Test
	public void loadFileFromFileSystemOrResource() {
		File file = fh.loadFileFromFileSystemOrResource("reports", "Negativtest.txt", true);
		assertNotNull(file);
		assertTrue(file.exists());

		file = fh.loadFileFromFileSystemOrResource("fehlschlag", "Negativtest.txt", true);
		assertNull(file);
	}

	public InputStream loadFromResource(String filename) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream jsonstream = classLoader.getResourceAsStream(filename);
		return jsonstream;
	}

	public String loadStringFromResource(String filename) {

		InputStream jsonstream = loadFromResource(filename);
		if (jsonstream == null) {
			return null;
		}

		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource(filename);
			if (resource != null) {
				URI uri = resource.toURI();
			}
		} catch (URISyntaxException e) {

		}

		return stream2String(jsonstream, filename);
	}

	private String stream2String(InputStream is, String filename) {
		StringWriter writer = new StringWriter();
		BOMInputStream bomInputStream = new BOMInputStream(is, false, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE,
				ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE);

		try {
			IOUtils.copy(bomInputStream, writer, "UTF-8");
		} catch (IOException e) {

			return "";
		}

		return writer.toString();
	}

}
