/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArchiveHelperTest {

	@InjectMocks
	private ArchiveHelper archiveHelper;
	
	@Test
	public void testCopyFile() {
		String srcPathString = "notExists";
		String dstPathString = "alsoNotExists";
		try {
			archiveHelper.copyFile(srcPathString, dstPathString);
			fail("Should have thrown IOException");
		} catch (IOException e) {
			// pass
		}
	}

}
