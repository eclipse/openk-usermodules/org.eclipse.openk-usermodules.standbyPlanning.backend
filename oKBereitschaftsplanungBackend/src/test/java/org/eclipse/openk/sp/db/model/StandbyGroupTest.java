/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class StandbyGroupTest {

	@Test
	public void testGettersAndSetters() {
		Boolean b = new Boolean(true);
		StandbyGroup group1 = new StandbyGroup(1l);
		assertEquals(1l, group1.getId().longValue());

		Date d = new Date();
		StandbyGroup group = new StandbyGroup();
		group.setId(1l);
		assertEquals(1l, group.getId().longValue());

		group.setModificationDate(d);
		assertEquals(d, group.getModificationDate());

		group.setTitle("Title");
		assertEquals("Title", group.getTitle());

		group.setNote("Test Note");
		assertEquals("Test Note", group.getNote());

		group.setNextUserInNextCycle(b);
		assertEquals(b, group.getNextUserInNextCycle());

		group.setExtendStandbyTime(b);
		assertEquals(b, group.getExtendStandbyTime());

		ArrayList<StandbyDuration> lsDurations = new ArrayList<>();
		StandbyDuration dur = new StandbyDuration();
		lsDurations.add(dur);
		group.setLsStandbyDurations(lsDurations);
		assertNotNull(group.getLsStandbyDurations());
		assertEquals(dur, group.getLsStandbyDurations().get(0));

//		ArrayList<StandbyList> lsStandbyList = new ArrayList<>();
//		StandbyList ls = new StandbyList();
//		lsStandbyList.add(ls);
//		group.setLsStandbyLists(lsStandbyList);
//		assertNotNull(group.getLsStandbyLists());
//		assertEquals(ls, group.getLsStandbyLists().get(0));

		ArrayList<UserInStandbyGroup> lsUserInStandbyGroups = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		lsUserInStandbyGroups.add(uisg);
		group.setLsUserInStandbyGroups(lsUserInStandbyGroups);
		assertNotNull(group.getLsUserInStandbyGroups());
		assertEquals(uisg, group.getLsUserInStandbyGroups().get(0));

		group.setLsBranches(new ArrayList<Branch>());
		assertNotNull(group.getLsBranches());

		group.setLsUserFunction(new ArrayList<UserFunction>());
		assertNotNull(group.getLsUserFunction());

		group.setLsRegions(new ArrayList<Region>());
		assertNotNull(group.getLsRegions());

		List<CalendarDay> lsCalendar = new ArrayList<>();
		lsCalendar.add(new CalendarDay());
		group.setLsIgnoredCalendarDays(lsCalendar);
		assertNotNull(group.getLsIgnoredCalendarDays());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		StandbyGroup obj1 = new StandbyGroup();
		obj1.setId(5L);

		StandbyGroup obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyGroup obj3 = new StandbyGroup();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));
	}
}
