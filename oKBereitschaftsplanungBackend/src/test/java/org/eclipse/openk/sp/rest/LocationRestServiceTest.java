/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.LocationController;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.LocationForBranchDto;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.dto.RegionSmallDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class LocationRestServiceTest {

	@InjectMocks
	private LocationRestService locationRestService;

	@Mock
	private LocationController locationController;

	public LocationRestServiceTest() {

	}

	@Test
	public void getLocationsTest() {
		String jwt = "LET-ME-IN";
		Response result = locationRestService.getLocations(jwt);
		assertNotNull(result);
	}

	@Test
	public void getLocationsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = locationRestService.getLocationsSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getLocationTest() {
		String jwt = "LET-ME-IN";
		Response result = locationRestService.getLocation(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveLocationTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		String jwt = "LET-ME-IN";
		Response result = locationRestService.saveLocation(jwt, locationDto);
		assertNotNull(result);
	}

	@Test
	public void saveRegionsForLocationTest() {
		List<RegionSmallDto> lsRegionSmallDtos = new ArrayList<RegionSmallDto>();
		lsRegionSmallDtos.add(new RegionSmallDto());

		String jwt = "LET-ME-IN";
		Response result = locationRestService.saveRegionsForLocation(1l, jwt, lsRegionSmallDtos);
		assertNotNull(result);
	}

	@Test
	public void deleteRegionsForLocationTest() {
		List<RegionSmallDto> lsRegionsSmallDtos = new ArrayList<>();
		lsRegionsSmallDtos.add(new RegionSmallDto());

		String jwt = "LET-ME-IN";
		Response result = locationRestService.deleteRegionsForLocation(1l, jwt, lsRegionsSmallDtos);
		assertNotNull(result);
	}

	@Test
	public void saveBranchesForLocationTest() {
		List<LocationForBranchDto> lsLocationForBranchDto = new ArrayList<LocationForBranchDto>();
		lsLocationForBranchDto.add(new LocationForBranchDto());

		String jwt = "LET-ME-IN";
		Response result = locationRestService.saveBranchesForLocation(1l, jwt, lsLocationForBranchDto);
		assertNotNull(result);
	}

	@Test
	public void deleteBranchesForLocationTest() {
		List<LocationForBranchDto> lsLocationForBranchDto = new ArrayList<LocationForBranchDto>();
		lsLocationForBranchDto.add(new LocationForBranchDto());

		String jwt = "LET-ME-IN";
		Response result = locationRestService.deleteBranchesForLocation(1l, jwt, lsLocationForBranchDto);
		assertNotNull(result);
	}

	@Test
	public void savePostcodesForLocationTest() {
		List<PostcodeDto> lsPostcodeDto = new ArrayList<PostcodeDto>();
		lsPostcodeDto.add(new PostcodeDto("12345"));
		String jwt = "LET-ME-IN";

		Response result = locationRestService.savePostcodesForLocation(1l, jwt, lsPostcodeDto);
		assertNotNull(result);
	}

	@Test
	public void deletePostcodesForLocationTest() {
		List<PostcodeDto> lsPostcodeDto = new ArrayList<PostcodeDto>();
		lsPostcodeDto.add(new PostcodeDto("43225"));
		String jwt = "LET-ME-IN";

		Response result = locationRestService.deletePostcodesForLocation(1l, jwt, lsPostcodeDto);
		assertNotNull(result);
	}

	@Test
	public void getLocationsSearchListTest() {

		String jwt = "LET-ME-IN";

		Response result = locationRestService.getLocationsSearchList(jwt);
		assertNotNull(result);
	}
}
