/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanRowsArchiveDtoTest {

	@InjectMocks
	PlanRowsArchiveDto planRowsDto = new PlanRowsArchiveDto();

	@Test
	public void getSet() {
		String text = "test";

		planRowsDto.setLabel(text);
		assertEquals(text, planRowsDto.getLabel());

		planRowsDto.setStyle(text);
		assertEquals(text, planRowsDto.getStyle());

		List<List<StandbyScheduleBodySelectionDto>> listGroupBodys = new ArrayList<>();
		List<StandbyScheduleBodySelectionDto> list = new ArrayList<>();
		StandbyScheduleBodySelectionDto bodyDto = new StandbyScheduleBodySelectionDto();
		list.add(bodyDto);
		listGroupBodys.add(list);
		planRowsDto.setListGroupBodys(listGroupBodys);
		assertNotNull(planRowsDto.getListGroupBodys());
		assertEquals(1, planRowsDto.getListGroupBodys().size());

	}

}
