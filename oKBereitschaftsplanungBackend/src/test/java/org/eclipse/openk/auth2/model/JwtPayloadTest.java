/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class JwtPayloadTest {

    @Test
    public void testGettersAndSetters() {

        JwtPayload jwtPayload = new JwtPayload();

        jwtPayload.setJti("jtiTest");
        assertEquals("jtiTest", jwtPayload.getJti());

        jwtPayload.setExp(8);
        assertEquals(8, jwtPayload.getExp());

        jwtPayload.setNbf(9);
        assertEquals(9, jwtPayload.getNbf());

        jwtPayload.setIat(10);
        assertEquals(10, jwtPayload.getIat());

        jwtPayload.setiss("issTest");
        assertEquals("issTest", jwtPayload.getiss());

        jwtPayload.setaud("audTest");
        assertEquals("audTest", jwtPayload.getaud());

        jwtPayload.setsub("subTest");
        assertEquals("subTest", jwtPayload.getsub());

        jwtPayload.settyp("typTest");
        assertEquals("typTest", jwtPayload.gettyp());

        jwtPayload.setAzp("azpTest");
        assertEquals("azpTest", jwtPayload.getAzp());

        jwtPayload.setNonce("nonceTest");
        assertEquals("nonceTest", jwtPayload.getNonce());

        jwtPayload.setAuthTime(12);
        assertEquals((Integer)12, jwtPayload.getAuthTime());

        jwtPayload.setSessionState("sessionStateTest");
        assertEquals("sessionStateTest", jwtPayload.getSessionState());

        jwtPayload.setAcr("acrTest");
        assertEquals("acrTest", jwtPayload.getAcr());

        List aoList = new ArrayList<String>();
        jwtPayload.setAllowedOrigins(aoList);
        assertEquals(aoList, jwtPayload.getAllowedOrigins());

        JwtRealmAccess jwtRealmAccess = new JwtRealmAccess();
        jwtPayload.setRealmAccess(jwtRealmAccess);
        assertEquals(jwtRealmAccess, jwtPayload.getRealmAccess());

        JwtResourceAccess jwtResourceAccess = new JwtResourceAccess();
        jwtPayload.setResourceAccess(jwtResourceAccess);
        assertEquals(jwtResourceAccess, jwtPayload.getResourceAccess());

        jwtPayload.setName("name");
        assertEquals("name", jwtPayload.getName());

        jwtPayload.setPreferredUsername("preferred_username");
        assertEquals("preferred_username", jwtPayload.getPreferredUsername());

        jwtPayload.setGivenName("given_name");
        assertEquals("given_name", jwtPayload.getGivenName());

        jwtPayload.setFamilyName("family_name");
        assertEquals("family_name", jwtPayload.getFamilyName());

    }
}
