/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static junit.framework.TestCase.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class JwtResourceAccessTest {



    @Test
    public void testGettersAndSetters() {

      JwtResourceAccess resAcc = new JwtResourceAccess();
      JwtAccount acc = new JwtAccount();

      List rolesList = new ArrayList<String>();
      rolesList.add("role4");
      rolesList.add("role5");
      rolesList.add("role6");

      acc.setRoles(rolesList);
      resAcc.setAccount(acc);

      assertEquals(resAcc.getAccount(), acc);
      assertEquals(resAcc.getAccount().getRoles(), rolesList);
    }
}
