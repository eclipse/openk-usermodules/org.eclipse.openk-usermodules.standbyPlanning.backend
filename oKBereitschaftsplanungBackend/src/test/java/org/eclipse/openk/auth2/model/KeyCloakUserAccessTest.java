/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class KeyCloakUserAccessTest {



    @Test
    public void testGettersAndSetters(){

        KeyCloakUserAccess kcUserAccess = new KeyCloakUserAccess();

        kcUserAccess.setManageGroupMembership(false);
        assertEquals(false, kcUserAccess.getManageGroupMembership());

        kcUserAccess.setView(false);
        assertEquals(false, kcUserAccess.getView());

        kcUserAccess.setMapRoles(false);
        assertEquals(false, kcUserAccess.getMapRoles());

        kcUserAccess.setImpersonate(false);
        assertEquals(false, kcUserAccess.getImpersonate());

        kcUserAccess.setManage(false);
        assertEquals(false, kcUserAccess.getManage());
    }


}
