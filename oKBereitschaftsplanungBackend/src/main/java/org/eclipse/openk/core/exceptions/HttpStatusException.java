/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.exceptions;

public class HttpStatusException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int httpStatus;
    private final transient Object payload;

    public HttpStatusException(int httpStatus ) {
        this.httpStatus = httpStatus;
        this.payload = null;
    }

    public HttpStatusException(int httpStatus, String msg ) {
        super( msg );
        this.httpStatus = httpStatus;
        this.payload = null;
    }

    public HttpStatusException(int httpStatus, String msg, Object payload ) {
        super( msg );
        this.httpStatus = httpStatus;
        this.payload = payload;
    }

    public HttpStatusException(int httpStatus, String message, Throwable t) {
        super(message, t);
        this.httpStatus = httpStatus;
        this.payload = null;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public Object getPayload() { return payload; }
}
