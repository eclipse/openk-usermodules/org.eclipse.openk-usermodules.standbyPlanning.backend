/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.controller;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.auth2.model.JwtPayload;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public class TokenManager {
	private static final Logger LOGGER = Logger.getLogger(TokenManager.class.getName());

	private static final TokenManager INSTANCE = new TokenManager();

	private TokenManager() {
	}

	public static TokenManager getInstance() {
		return INSTANCE;
	}

	public void checkAutLevel(String token, String[] secureType) throws HttpStatusException {
		JwtPayload jwtPayload = JwtHelper.getJwtPayload(token);

		boolean hasAccess = false;

		for (String roll : secureType) {
			if (jwtPayload.getRealmAccess().isInRole(roll)) {
				hasAccess = true;
				break;
			}
		}

		if (!hasAccess) {
			LOGGER.warn("Security level not sufficent ");
			throw new HttpStatusException(HttpStatus.SC_FORBIDDEN);
		}
	}

}
