/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.common;

public final class Globals {
	
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	public static final String HEADER_JSON_UTF8 = "application/json; charset=utf-8";
	public static final String KEYCLOAK_AUTH_TAG = "Authorization";

	public static final String SESSION_TOKEN_TAG = "X-XSRF-TOKEN";
	public static final String FORCE_DELETE_LOCK = "FORCE";

	// Usergroups
	public static final String KEYCLOAK_ROLE_BP_LESEBERECHTIGTE = "BP_Leseberechtigte";
	public static final String KEYCLOAK_ROLE_BP_GRUPPENLEITER = "BP_Gruppenleiter";
	public static final String KEYCLOAK_ROLE_BP_SACHBEARBEITER = "BP_Sachbearbeiter";
	public static final String KEYCLOAK_ROLE_BP_ADMIN = "BP_Admin";
	protected static final String[] KEYCLOAK_ROLE_BP_ALL = { KEYCLOAK_ROLE_BP_ADMIN, KEYCLOAK_ROLE_BP_SACHBEARBEITER,
			KEYCLOAK_ROLE_BP_GRUPPENLEITER, KEYCLOAK_ROLE_BP_LESEBERECHTIGTE };

	public static final String ENCODING = "UTF-8";

	public static final String APP_PROP_FILE_NAME = "application.properties";
	public static final String PROP_USE_HTTPS = "portal.useHttps";
	public static final String PROP_BASE_URL = "portal.url";
	public static final String PROP_VERSION = "backend.version";
	public static final String PROP_BASE_TIMEZONE = "backend.timezone";

	public static final String PROP_DISTANCE_URL = "distance.url";
	public static final String PROP_DISTANCE_USE_HTTPS = "distance.useHttps";
	public static final String PROP_DISTANCE_SKIP = "distance.skip";

	public static final String AUTH_AUTH_BASE_URL = "http://localhost:8080/portal/rest/beservice";

	// Mail
	public static final String MAIL_TEMPLATE_DIRECTORY = "mail.template.path";
	public static final String SMTP_ACTIVE = "smtp.active";
	public static final String SMTP_MAIL_HOST = "smtp.host";
	public static final String SMTP_MAIL_PORT = "smtp.port";
	public static final String SMTP_MAIL_SSL = "smtp.use.ssl";
	public static final String SMTP_MAIL_AUTH_USER = "smtp.auth.user";
	public static final String SMTP_MAIL_AUTH_SECRET = "smtp.auth.pw";
	public static final String SMTP_MAIL_SENDER_NAME = "smtp.app.sender.name";
	public static final String SMTP_MAIL_SENDER_EMAIL = "smtp.app.sender.email";
	public static final String SMTP_BACKUP_RECIPIENT = "smtp.backup.mail.recipient";

	public static final String LINK_DASHBOARD = "frontend.dashboard.url";

	public static final String VALIDATE_AFTER_TRANSFER = "validate.transfer";
	public static final String VALIDATE_AFTER_DELETE = "validate.delete";

	public static final String REPORT_TEXT1 = "/reports/custom_text1.html";
	public static final String REPORT_TEXT2 = "/reports/custom_text2.html";
	public static final String REPORT_TEXT3 = "/reports/custom_text3.html";

	private Globals() {
	}

	public static String[] getAllRolls() {
		return KEYCLOAK_ROLE_BP_ALL;
	}
}
