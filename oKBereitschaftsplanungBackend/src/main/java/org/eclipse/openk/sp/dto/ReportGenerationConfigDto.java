/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "ReportGenerationConfig" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "ReportGenerationConfigDto")
@JsonInclude(Include.NON_NULL)
public class ReportGenerationConfigDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull(message = "Name is not set")
	@Size(max = 256, message = "Name with max. 256 characters")
	private String name;

	@NotNull(message = "FileNamePattern is not set")
	@Size(max = 128, message = "FileNamePattern with max. 128 characters")
	private String fileNamePattern;

	@NotNull(message = "Subject is not set")
	@Size(max = 128, message = "Subject with max. 128 characters")
	private String subject;

	@NotNull(message = "To is not set")
	private Set<String> to;

	@NotNull(message = "PrintFormat is not set")
	private String printFormat;

	@NotNull(message = "ReportName is not set")
	@Size(max = 256, message = "ReportName with max. 256 characters")
	private String reportName;

	@NotNull(message = "StandByListId is not set")
	private Long standByListId;

	@NotNull(message = "StatusId is not set")
	@Min(0)
	private Long statusId;

	@Min(1)
	@Max(7)
	@NotNull(message = "TriggerWeekDay is not set")
	private Integer triggerWeekDay;

	@Min(0)
	@Max(23)
	@NotNull(message = "TriggerHour is not set")
	private Integer triggerHour;

	@Min(0)
	@Max(59)
	@NotNull(message = "TriggerMinute is not set")
	private Integer triggerMinute;

	@NotNull(message = "ValidFromDayOffset is not set")
	private Integer validFromDayOffset;

	@Min(0)
	@Max(23)
	@NotNull(message = "ValidFromHour is not set")
	private Integer validFromHour;

	@Min(0)
	@Max(59)
	@NotNull(message = "ValidFromMinute is not set")
	private Integer validFromMinute;

	@NotNull(message = "ValidToDayOffset is not set")
	private Integer validToDayOffset;

	@Min(0)
	@Max(23)
	@NotNull(message = "ValidToHour not set")
	private Integer validToHour;

	@Min(0)
	@Max(59)
	@NotNull(message = "ValidToMinute is not set")
	private Integer validToMinute;
	

	@NotNull(message = "EmailText is not set")
	private String emailText;

	public void setId(final Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getFileNamePattern() {
		return fileNamePattern;
	}

	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Set<String> getTo() {
		return to;
	}

	public void setTo(Set<String> to) {
		this.to = to;
	}

	public String getPrintFormat() {
		return printFormat;
	}

	public void setPrintFormat(String printFormat) {
		this.printFormat = printFormat;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Long getStandByListId() {
		return standByListId;
	}

	public void setStandByListId(Long standByListId) {
		this.standByListId = standByListId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Integer getTriggerWeekDay() {
		return triggerWeekDay;
	}

	public void setTriggerWeekDay(Integer triggerWeekDay) {
		this.triggerWeekDay = triggerWeekDay;
	}

	public Integer getTriggerHour() {
		return triggerHour;
	}

	public void setTriggerHour(Integer triggerHour) {
		this.triggerHour = triggerHour;
	}

	public Integer getTriggerMinute() {
		return triggerMinute;
	}

	public void setTriggerMinute(Integer triggerMinute) {
		this.triggerMinute = triggerMinute;
	}

	public Integer getValidFromDayOffset() {
		return validFromDayOffset;
	}

	public void setValidFromDayOffset(Integer validFromDayOffset) {
		this.validFromDayOffset = validFromDayOffset;
	}

	public Integer getValidFromHour() {
		return validFromHour;
	}

	public void setValidFromHour(Integer validFromHour) {
		this.validFromHour = validFromHour;
	}

	public Integer getValidFromMinute() {
		return validFromMinute;
	}

	public void setValidFromMinute(Integer validFromMinute) {
		this.validFromMinute = validFromMinute;
	}

	public Integer getValidToDayOffset() {
		return validToDayOffset;
	}

	public void setValidToDayOffset(Integer validToDayOffset) {
		this.validToDayOffset = validToDayOffset;
	}

	public Integer getValidToHour() {
		return validToHour;
	}

	public void setValidToHour(Integer validToHour) {
		this.validToHour = validToHour;
	}

	public Integer getValidToMinute() {
		return validToMinute;
	}

	public void setValidToMinute(Integer validToMinute) {
		this.validToMinute = validToMinute;
	}
	
	public String getEmailText() {
		return emailText;
	}
	
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

}
