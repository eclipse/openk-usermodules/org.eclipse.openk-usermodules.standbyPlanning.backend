package org.eclipse.openk.sp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

public class FileStreamingOutput implements StreamingOutput {

	private File file;

	public FileStreamingOutput(File file) {
		this.file = file;
	}

	@Override
	public void write(OutputStream output) throws IOException {

		try (FileInputStream input = new FileInputStream(file)) {
			int bytes;
			while ((bytes = input.read()) != -1) {
				output.write(bytes);
			}
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

}
