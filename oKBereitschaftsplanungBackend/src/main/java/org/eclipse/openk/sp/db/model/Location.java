/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "LOCATION" database table.
 */
@Entity
@Table(name = "Location")
public class Location extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOCATION_ID_SEQ")
	@SequenceGenerator(name = "LOCATION_ID_SEQ", sequenceName = "LOCATION_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = true)
	private String title;

	@Column(name = "district", length = 256, nullable = true)
	private String district;

	@Column(name = "shorttext", length = 8, nullable = true)
	private String shorttext;

	@Column(name = "community", length = 256, nullable = true)
	private String community;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_location_has_postcode", joinColumns = {
			@JoinColumn(name = "location_id") }, inverseJoinColumns = { @JoinColumn(name = "postcode_id") })
	private List<Postcode> lsPostcode = new ArrayList<>();

	@Column(name = "wgs_84_zone_district", length = 256, nullable = true)
	private String wgs84zonedistrict;

	@Column(name = "wgs_84_latitude_district", length = 32, nullable = true)
	private String latitudedistrict;

	@Column(name = "wgs_84_longitude_district", length = 32, nullable = true)
	private String longitudedistrict;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_regions_in_location", joinColumns = @JoinColumn(name = "location_id"), inverseJoinColumns = @JoinColumn(name = "region_id"))
	private List<Region> lsRegions = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "location")
	private List<LocationForBranch> lsLocationForBranches = new ArrayList<>();

	public Location() {
		/** default constructor. */
	}

	public Location(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getShorttext() {
		return shorttext;
	}

	public void setShorttext(String shorttext) {
		this.shorttext = shorttext;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public List<Postcode> getLsPostcode() {
		return lsPostcode;
	}

	public void setLsPostcode(List<Postcode> lsPostcode) {
		this.lsPostcode = lsPostcode;
	}

	public String getWgs84zonedistrict() {
		return wgs84zonedistrict;
	}

	public void setWgs84zonedistrict(String wgs84zonedistrict) {
		this.wgs84zonedistrict = wgs84zonedistrict;
	}

	public String getLatitudedistrict() {
		return latitudedistrict;
	}

	public void setLatitudedistrict(String latitudedistrict) {
		this.latitudedistrict = latitudedistrict;
	}

	public String getLongitudedistrict() {
		return longitudedistrict;
	}

	public void setLongitudedistrict(String longitudedistrict) {
		this.longitudedistrict = longitudedistrict;
	}

	public List<Region> getLsRegions() {
		return lsRegions;
	}

	public void setLsRegions(List<Region> lsRegions) {
		this.lsRegions = lsRegions;
	}

	/**
	 * @return the lsLocationForBranches
	 */
	public List<LocationForBranch> getLsLocationForBranches() {
		return lsLocationForBranches;
	}

	/**
	 * @param lsLocationForBranches
	 *            the lsLocationForBranches to set
	 */
	public void setLsLocationForBranches(List<LocationForBranch> lsLocationForBranches) {
		this.lsLocationForBranches = lsLocationForBranches;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
