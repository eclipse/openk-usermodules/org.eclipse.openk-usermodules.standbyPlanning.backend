/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.core.viewmodel.GeneralReturnItem;

public final class SpExceptionMapper {
	private SpExceptionMapper() {
	}

	public static String unknownErrorToJson() {
		ErrorReturn er = new ErrorReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Unknown Error");
		return JsonGeneratorBase.getGson().toJson(er);
	}

	public static String toJson(HttpStatusException e) {
		ErrorReturn er = new ErrorReturn(e.getHttpStatus(), e.getMessage());
		return JsonGeneratorBase.getGson().toJson(er);
	}

	public static String getGeneralErrorJson() {
		return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("NOK"));
	}

	public static String getGeneralOKJson() {
		return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("OK"));
	}

	public static String getExceptionStacktrace(Throwable exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}
