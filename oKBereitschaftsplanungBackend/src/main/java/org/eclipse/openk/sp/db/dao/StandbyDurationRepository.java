/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StandbyDurationRepository extends JpaRepository<StandbyDuration, Long> {
	List<StandbyDuration> findAll();

	@Query(value = "SELECT dur FROM StandbyDuration dur WHERE dur.standbyGroup.id = :groupId "
			+ "ORDER BY dur.validFrom ASC")
	public List<StandbyDuration> findById(@Param("groupId") Long groupId);

	@Query(value = "SELECT dur FROM StandbyDuration dur WHERE (dur.standbyGroup.id = :groupId AND dur.validDayFrom = :validFromDay) "
			+ "ORDER BY dur.validFrom ASC")
	public List<StandbyDuration> findDurationForValidFromDay(@Param("groupId") Long groupId,
			@Param("validFromDay") Integer validFromDay);

	@Query(value = "SELECT dur FROM StandbyDuration dur WHERE (dur.standbyGroup.id = :groupId AND dur.validDayTo = :validToDay) "
			+ "ORDER BY dur.validTo ASC")
	public List<StandbyDuration> findDurationForValidToDay(@Param("groupId") Long groupId,
			@Param("validToDay") Integer validToDay);

	@Query(value = "SELECT dur FROM StandbyDuration dur WHERE (dur.standbyGroup.id = :groupId AND "
			+ "( (dur.validDayTo >= :validDay AND dur.validDayFrom <= :validDay) "
			+ "OR (dur.validDayFrom >= dur.validDayTo AND :validDay <= dur.validTo AND :validDay >= dur.validFrom))) ORDER BY dur.validTo ASC")
	public List<StandbyDuration> findDurationsAroundValidDay(@Param("groupId") Long groupId,
			@Param("validDay") Integer validDay);
}