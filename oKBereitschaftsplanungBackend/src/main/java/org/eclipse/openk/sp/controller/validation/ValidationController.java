/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.dto.ValidationDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle planning operations. */
public class ValidationController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(ValidationController.class);
	public static final String BEAN_USER_AVAILABLE_VALIDATOR = "userAvailableValidator";
	public static final String BEAN_USER_AVAILABLE_FOR_GROUP_VALIDATOR = "userAvailableForGroupValidator";
	public static final String BEAN_GROUP_COVERAGE_VALIDATOR = "groupCoverageValidator";
	public static final String BEAN_DOUBLE_PLANNED_VALIDATOR = "doublePlannedValidator";
	public static final String BEAN_PLACEHOLDER_STANDBY_USER_VALIDATOR = "placeholderStandbyUserValidator";

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private UserController userController;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@Autowired
	private StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyStatusRepository standbyStatusRepository;

	@Autowired
	private PlanningController planningController;

	public List<PlanningMsgDto> startValidation(Date from, Date till, List<StandbyGroup> lsStandbyGroups,
			List<String> lsValidationBeanNames, Long statusId, Long userId) throws SpException {
		try {
			planningController
					.addMessagesToList(ValidationMsgController.createMsgStartValidation(from, till, lsStandbyGroups));
			for (StandbyGroup group : lsStandbyGroups) {
				for (String beanName : lsValidationBeanNames) {
					if (appContext.containsBean(beanName) && planningController.getWarningCounter() < 200) {
						if (statusId == null) {
							// check for every status in DB
							for (StandbyStatus stbyStatus : standbyStatusRepository.findAll()) {
								// check for given status
								planningController.addMessagesToList(((IValidator) appContext.getBean(beanName))
										.execute(from, till, group, lsStandbyGroups, stbyStatus.getId(), userId));
							}
						} else {
							// check for given status
							planningController.addMessagesToList(((IValidator) appContext.getBean(beanName))
									.execute(from, till, group, lsStandbyGroups, statusId, userId));
						}
					}
				}
			}
			ValidationMsgController.createMsgEndValidation(planningController.getLsMsgDto());

			return planningController.getLsMsgDto();
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}
	}

	public PlanningMsgResponseDto startValidation(ValidationDto validatonDto) throws SpException {
		PlanningMsgResponseDto responseDto = new PlanningMsgResponseDto();
		planningController.startWithNewList();
		try {
			// check if to validate groups or list
			List<StandbyGroup> lsStandbyGroups = null;
			if (validatonDto.getStandbyListId() != null) {
				StandbyList sbList = standbyListRepository.findOne(validatonDto.getStandbyListId());
				lsStandbyGroups = sbList.getLsStandbyGroups();
			} else if (validatonDto.getStandbyGroupId() != null) {
				lsStandbyGroups = new ArrayList<>();
				lsStandbyGroups.add(standbyGroupRepository.findOne(validatonDto.getStandbyGroupId()));
			} else {
				SpErrorEntry ee = SpExceptionEnum.MISSING_PARAMETERS_EXCEPTION.getEntry();
				throw new SpException(ee);
			}

			// start validation
			List<String> lsValidationBeanNames = new ArrayList<>();
			lsValidationBeanNames.add(ValidationController.BEAN_GROUP_COVERAGE_VALIDATOR);
			lsValidationBeanNames.add(ValidationController.BEAN_DOUBLE_PLANNED_VALIDATOR);
			lsValidationBeanNames.add(ValidationController.BEAN_PLACEHOLDER_STANDBY_USER_VALIDATOR);
			this.startValidation(validatonDto.getValidFromDate(), validatonDto.getValidToDate(), lsStandbyGroups,
					lsValidationBeanNames, validatonDto.getStatusId(), validatonDto.getNewUserId());
			responseDto.getLsMsg().addAll(planningController.getLsMsgDto());
			return responseDto;
		} catch (SpException e) {
			throw e;
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}
	}
}
