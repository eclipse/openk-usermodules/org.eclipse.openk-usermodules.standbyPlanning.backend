/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.LocationController;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.LocationForBranchDto;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.dto.RegionSmallDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("location")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LocationRestService extends BaseResource {

	@Autowired
	private LocationController locationController;

	public LocationRestService() {
		super(Logger.getLogger(LocationRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getLocations(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<LocationDto>> invokable = modusr -> locationController.getLocations();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getLocationsSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<LocationSelectionDto>> invokable = modusr -> locationController.getLocationsSelection();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/searchlist")
	public Response getLocationsSearchList(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<LabelValueDto>> invokable = modusr -> locationController.getLocationsSearchList();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/{id}")
	public Response getLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<LocationDto> invokable = modusr -> locationController.getLocation(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/{id}/branch/save/list")
	public Response saveBranchesForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<LocationForBranchDto> lsLocationForBranchDto) {

		ModifyingInvokable<List<LocationForBranchDto>> invokable = modusr -> locationController
				.saveBranchesForLocation(id, lsLocationForBranchDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/branch/delete/list")
	public Response deleteBranchesForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<LocationForBranchDto> lsLocationForBranchDto) {

		ModifyingInvokable<List<LocationForBranchDto>> invokable = modusr -> locationController
				.deleteBranchesForLocation(id, lsLocationForBranchDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/region/save/list")
	public Response saveRegionsForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<RegionSmallDto> lsRegionDto) {

		ModifyingInvokable<List<RegionSmallDto>> invokable = modusr -> locationController.saveRegionsForLocation(id,
				lsRegionDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/region/delete/list")
	public Response deleteRegionsForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<RegionSmallDto> lsRegionDto) {

		ModifyingInvokable<List<RegionSmallDto>> invokable = modusr -> locationController.deleteRegionsForLocation(id,
				lsRegionDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/postcode/save/list")
	public Response savePostcodesForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<PostcodeDto> lsPostcodeDto) {

		ModifyingInvokable<List<PostcodeDto>> invokable = modusr -> locationController.savePostcodeForLocation(id,
				lsPostcodeDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/postcode/delete/list")
	public Response deletePostcodesForLocation(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<PostcodeDto> lsPostcodeDto) {

		ModifyingInvokable<List<PostcodeDto>> invokable = modusr -> locationController.deletePostcodeForLocation(id,
				lsPostcodeDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/save")
	public Response saveLocation(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid LocationDto locationDto) {

		ModifyingInvokable<LocationDto> invokable = modusr -> locationController.saveLocation(locationDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
