/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBY_GROUP" database table.
 */
@Entity
@Table(name = "STANDBY_GROUP")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class StandbyGroup extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_GROUP_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_GROUP_ID_SEQ", sequenceName = "STANDBY_GROUP_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = false)
	private String title;

	@Column(name = "note", length = 2048, nullable = true)
	private String note;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "next_user_in_next_cycle", nullable = false)
	@Convert("booleanConverter")
	private Boolean nextUserInNextCycle;

	@Column(name = "extend_standby_time", nullable = false)
	@Convert("booleanConverter")
	private Boolean extendStandbyTime;

	// @ManyToMany(mappedBy = "lsStandbyGroups")
	// private List<StandbyList> lsStandbyLists = new ArrayList<>();

	@ManyToMany(mappedBy = "lsStandbyGroups")
	private List<Branch> lsBranches = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "standbyGroup")
	private List<StandbyDuration> lsStandbyDurations = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "standbyGroup")
	private List<UserInStandbyGroup> lsUserInStandbyGroups = new ArrayList<>();

	@ManyToMany(mappedBy = "lsStandbyGroups")
	private List<UserFunction> lsUserFunction = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_regions_for_standby_group", joinColumns = @JoinColumn(name = "standby_group_id"), inverseJoinColumns = @JoinColumn(name = "region_id"))
	private List<Region> lsRegions = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_ignored_calendar_for_group", joinColumns = @JoinColumn(name = "standby_group_id"), inverseJoinColumns = @JoinColumn(name = "calendar_id"))
	private List<CalendarDay> lsIgnoredCalendarDays = new ArrayList<>();

	public StandbyGroup() {
		/** default constructor. */
	}

	public StandbyGroup(Long id) {
		super();
		setId(id);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the lsUserInStandbyGroups
	 */
	public List<UserInStandbyGroup> getLsUserInStandbyGroups() {
		return lsUserInStandbyGroups;
	}

	/**
	 * @param lsUserInStandbyGroups
	 *            the lsUserInStandbyGroups to set
	 */
	public void setLsUserInStandbyGroups(List<UserInStandbyGroup> lsUserInStandbyGroups) {
		this.lsUserInStandbyGroups = lsUserInStandbyGroups;
	}

	/**
	 * @return the lsStandbyDurations
	 */
	public List<StandbyDuration> getLsStandbyDurations() {
		return lsStandbyDurations;
	}

	/**
	 * @param lsStandbyDurations
	 *            the lsStandbyDurations to set
	 */
	public void setLsStandbyDurations(List<StandbyDuration> lsStandbyDurations) {
		this.lsStandbyDurations = lsStandbyDurations;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the lsBranch
	 */
	public List<Branch> getLsBranches() {
		return lsBranches;
	}

	/**
	 * @param lsBranch
	 *            the lsBranch to set
	 */
	public void setLsBranches(List<Branch> lsBranch) {
		this.lsBranches = lsBranch;
	}

	/**
	 * @return the nextUserInNextCycle
	 */
	public Boolean getNextUserInNextCycle() {
		return nextUserInNextCycle;
	}

	/**
	 * @param nextUserInNextCycle
	 *            the nextUserInNextCycle to set
	 */
	public void setNextUserInNextCycle(Boolean nextUserInNextCycle) {
		this.nextUserInNextCycle = nextUserInNextCycle;
	}

	/**
	 * @return the extendDuty
	 */
	public Boolean getExtendStandbyTime() {
		return extendStandbyTime;
	}

	/**
	 * @param extendStandbyTime
	 *            the extendDuty to set
	 */
	public void setExtendStandbyTime(Boolean extendStandbyTime) {
		this.extendStandbyTime = extendStandbyTime;
	}

	/**
	 * @return the lsUserFunction
	 */
	public List<UserFunction> getLsUserFunction() {
		return lsUserFunction;
	}

	/**
	 * @param lsUserFunction
	 *            the lsUserFunction to set
	 */
	public void setLsUserFunction(List<UserFunction> lsUserFunction) {
		this.lsUserFunction = lsUserFunction;
	}

	/**
	 * @return the lsRegions
	 */
	public List<Region> getLsRegions() {
		return lsRegions;
	}

	/**
	 * @param lsRegions
	 *            the lsRegions to set
	 */
	public void setLsRegions(List<Region> lsRegions) {
		this.lsRegions = lsRegions;
	}

	/**
	 * @return the lsIgnoredCalendarDays
	 */
	public List<CalendarDay> getLsIgnoredCalendarDays() {
		return lsIgnoredCalendarDays;
	}

	/**
	 * @param lsIgnoredCalendarDays
	 *            the lsIgnoredCalendarDays to set
	 */
	public void setLsIgnoredCalendarDays(List<CalendarDay> lsIgnoredCalendarDays) {
		this.lsIgnoredCalendarDays = lsIgnoredCalendarDays;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}
}
