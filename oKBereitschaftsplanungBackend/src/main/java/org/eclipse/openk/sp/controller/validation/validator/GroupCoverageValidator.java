/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.controller.validation.IValidator;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to validate if all time slots are set for a group. */
public class GroupCoverageValidator extends AbstractController implements IValidator {
	protected static final Logger LOGGER = Logger.getLogger(GroupCoverageValidator.class);
	protected static final long DIVISOR_MS_TO_MIN = 60000;
	@Autowired
	private PlanningController planningController;

	@Autowired
	private StandbyScheduleBodyRepository scheduleBodyRepository;

	@Override
	public List<PlanningMsgDto> execute(Date from, Date till, StandbyGroup currentGroup,
			List<StandbyGroup> lsStandbyGroups, Long statusId, Long userId) throws SpException {
		List<PlanningMsgDto> planningMsgDtos = new ArrayList<>();

		Date tempDate = from;
		while (DateHelper.isDateBefore(tempDate, till)) {
			List<StandbyDuration> lsDurations = new ArrayList<>();
			if (planningController.isHoliday(tempDate, currentGroup)) {
				// create temp duration for the whole day if it is a holiday / calendar entry
				StandbyDuration duration = new StandbyDuration();
				duration.setValidFrom(DateHelper.getStartOfDay(tempDate));
				duration.setValidDayFrom(DateHelper.getDayOfWeek(tempDate));
				duration.setValidTo(DateHelper.addDaysToDate(DateHelper.getEndOfDay(tempDate), 1));
				duration.setValidDayTo(DateHelper.getDayOfWeek(DateHelper.addDaysToDate(tempDate, 1)));
				lsDurations.add(duration);
			} else {
				lsDurations = planningController.getFittingDurationsOfGroup(currentGroup, tempDate);
			}

			if (lsDurations != null) {
				for (StandbyDuration duration : lsDurations) {
					int days = DateHelper.calculateDifferenceOfDays(duration.getValidDayFrom(),
							duration.getValidDayTo(), duration.getValidFrom(), duration.getValidTo());
					Date validFrom = DateHelper.getDateWithTime(tempDate, duration.getValidFrom());
					Date validTo = DateHelper.getDateWithTime(DateHelper.addDaysToDate(tempDate, days),
							duration.getValidTo());

					Interval durationInterval = new Interval(validFrom.getTime(), validTo.getTime());

					List<StandbyScheduleBody> lsBodies = scheduleBodyRepository.findByGroupAndStatusHittingDateInterval(
							currentGroup.getId(), validFrom, validTo, statusId);
					List<Interval> lsScheduleBodyIntervals = this.getBodiesIntervalList(lsBodies);

					if (!DateHelper.isCoveredBy(durationInterval, lsScheduleBodyIntervals)) {
						planningMsgDtos.addAll(
								ValidationMsgController.createMsgMissingTimeSlot(validFrom, validTo, currentGroup));
					}
				}
			} else {
				LOGGER.warn("Keine Duration gefunden");
			}
			tempDate = DateHelper.addDaysToDate(tempDate, 1);
		}
		return planningMsgDtos;
	}

	/**
	 * Method to put all time slots of the given {@link StandbyScheduleBody} into a
	 * List of intervals.
	 * 
	 * 
	 * @param lsBodies
	 * @return
	 */
	public List<Interval> getBodiesIntervalList(List<StandbyScheduleBody> lsBodies) {
		List<Interval> lsInterval = new ArrayList<>();
		if (lsBodies != null && !lsBodies.isEmpty()) {
			for (StandbyScheduleBody body : lsBodies) {
				lsInterval.add(new Interval(body.getValidFrom().getTime(), body.getValidTo().getTime()));
			}
		}
		return lsInterval;
	}

}
