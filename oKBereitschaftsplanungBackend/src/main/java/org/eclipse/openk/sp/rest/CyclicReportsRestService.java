/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.CyclicReportsController;
import org.eclipse.openk.sp.dto.ReportGenerationConfigDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("auto-reports")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CyclicReportsRestService extends BaseResource {

	@Autowired
	private CyclicReportsController automaticReportsController;

	public CyclicReportsRestService() {
		super(Logger.getLogger(CyclicReportsRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/")
	public Response getAllReportGenerationConfig(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<ReportGenerationConfigDto>> invokable = modusr -> automaticReportsController.getAllReportGenerationConfig();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/")
	public Response addReportGenerationConfig(@Valid ReportGenerationConfigDto reportGenerationConfig,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<ReportGenerationConfigDto> invokable = modusr -> automaticReportsController.addReportGenerationConfig(reportGenerationConfig);

		String[] securityRoles ={Globals.KEYCLOAK_ROLE_BP_ADMIN};

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@DELETE
	@Path("/{configId}")
	public Response deleteReportGenerationConfig(@PathParam("configId") Long configId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<Response> invokable = modusr -> automaticReportsController.deleteReportGenerationConfig(configId);

		String[] securityRoles ={Globals.KEYCLOAK_ROLE_BP_ADMIN};

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@POST
	@Path("/{configId}")
	public Response editReportGenerationConfig(@PathParam("configId") Long configId,
			@Valid ReportGenerationConfigDto reportGenerationConfig,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<ReportGenerationConfigDto> invokable = modusr -> automaticReportsController.editReportGenerationConfig(configId, reportGenerationConfig);

		String[] securityRoles ={Globals.KEYCLOAK_ROLE_BP_ADMIN};

		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
