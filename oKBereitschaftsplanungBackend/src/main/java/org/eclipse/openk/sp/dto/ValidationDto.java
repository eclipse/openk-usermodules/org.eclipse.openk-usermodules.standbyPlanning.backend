package org.eclipse.openk.sp.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "ValidationDto")
@JsonInclude(Include.NON_NULL)
public class ValidationDto extends StandbyScheduleActionDto {
	/***/
	private static final long serialVersionUID = 1L;
	private Long standbyListId;

	/**
	 * @return the standbyListId
	 */
	public Long getStandbyListId() {
		return standbyListId;
	}

	/**
	 * @param standbyListId
	 *            the standbyListId to set
	 */
	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

}
