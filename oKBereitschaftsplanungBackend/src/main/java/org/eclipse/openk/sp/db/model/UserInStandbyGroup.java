/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "USER_IN_STAND_BY_GROUP" database table.
 */
@Entity
@Table(name = "USER_IN_STANDBY_GROUP")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class UserInStandbyGroup extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_IN_STANDBY_GROUP_ID_SEQ")
	@SequenceGenerator(name = "USER_IN_STANDBY_GROUP_ID_SEQ", sequenceName = "USER_IN_STANDBY_GROUP_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "standby_group_id", referencedColumnName = "id", nullable = false)
	private StandbyGroup standbyGroup = new StandbyGroup();

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user = new User();

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "position", nullable = false)
	private Integer position;

	public UserInStandbyGroup() {
		/** default constructor. */
	}

	public UserInStandbyGroup(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the standbyGroup
	 */
	public StandbyGroup getStandbyGroup() {
		return standbyGroup;
	}

	/**
	 * @param standbyGroup
	 *            the standbyGroup to set
	 */
	public void setStandbyGroup(StandbyGroup standbyGroup) {
		this.standbyGroup = standbyGroup;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param pos
	 *            the position to set
	 */
	public void setPosition(Integer pos) {
		this.position = pos;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	@Override
	public UserInStandbyGroup copy() {
		UserInStandbyGroup newUser = new UserInStandbyGroup();
		newUser.setId(null);
		newUser.setPosition(this.getPosition());
		newUser.setStandbyGroup(this.getStandbyGroup());
		newUser.setUser(this.getUser());
		newUser.setValidFrom(this.getValidFrom());
		newUser.setValidTo(this.getValidTo());

		return newUser;
	}
}
