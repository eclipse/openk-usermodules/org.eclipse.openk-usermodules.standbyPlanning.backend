package org.eclipse.openk.sp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.persistence.Inheritance;

@Target(ElementType.TYPE)
@Inheritance
@Retention(RetentionPolicy.RUNTIME)
public @interface EntitySchema {

	String name();

}
