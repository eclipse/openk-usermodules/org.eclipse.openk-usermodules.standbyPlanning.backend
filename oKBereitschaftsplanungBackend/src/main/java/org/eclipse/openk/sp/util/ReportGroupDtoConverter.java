/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.PlannedDataController;
import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.dto.report.ReportGroupDto;
import org.eclipse.openk.sp.dto.report.ReportInputDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportGroupDtoConverter {

	@Autowired
	StandbyGroupRepository standbyGroupRepository;

	@Autowired
	StandbyListRepository standbyListRepository;

	@Autowired
	private PlannedDataController plannedDataController;

	@Autowired
	private EntityConverter entityConverter;

	protected static final Logger LOGGER = Logger.getLogger(ReportGroupDtoConverter.class);

	public List<ReportInputDto> get10Rows(ReportDto reportDto) throws SpException {

		List<ReportInputDto> inputDto = new ArrayList<>();

		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		standbyScheduleFilterDto.setStandbyListId(reportDto.getStandByListId());
		standbyScheduleFilterDto.setValidFrom(reportDto.getValidFromDate());
		standbyScheduleFilterDto.setValidTo(reportDto.getValidToDate());
		boolean allDaySearch = false;

		Long statusId = reportDto.getStatusId();

		// get plan
		StandbyScheduleDto plan = plannedDataController.getFilteredPlanByStatus(standbyScheduleFilterDto, statusId,
				allDaySearch);

		// get first 10 groups with some data
		// empty groups are not represented
		Long[] groupIdArray = new Long[10];
		int index = 0;

		List<StandbyGroupSelectionDto> groupList = plan.getPlanHeader().getListGroups();
		for (StandbyGroupSelectionDto standbyGroupSelectionDto : groupList) {
			Long groupId = standbyGroupSelectionDto.getId();
			if (plan.groupSize(groupId) > 0 && index < 10) {
				groupIdArray[index++] = groupId;
			}
		}

		Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap = new HashMap<>();

		// convert to rows
		Date dateIndex = DateHelper.getStartOfDay(reportDto.getValidFromDate());

		while (DateHelper.isDateBefore(dateIndex, reportDto.getValidToDate())) {

			// fill groups from 1 to 10
			// only groups that are not empty in the interval of time
			addReportRow(reportDto, inputDto, plan, dateIndex, groupIdArray, lastBodyMap);

			dateIndex = DateHelper.addDaysToDate(dateIndex, 1);

		}

		return inputDto;
	}

	public ReportGroupDto addReportRow(ReportDto reportDto, List<ReportInputDto> inputDto, StandbyScheduleDto plan,
			Date dateIndex, Long[] groupIdArray, Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap) {

		PlanRowsDto row = plan.getRow(dateIndex);

		if (row == null) {
			LOGGER.debug("no plan row for day found: " + dateIndex);
			return null;
		}

		ReportInputDto reportInputRow = new ReportInputDto();
		reportInputRow.setReportDto(reportDto);

		ReportGroupDto reportGroupDto = new ReportGroupDto();
		reportInputRow.setReportGroupDto(reportGroupDto);

		reportGroupDto.setFromDate(dateIndex);
		inputDto.add(reportInputRow);

		for (int i = 0; i < groupIdArray.length; i++) {
			Long groupId = groupIdArray[i];

			if (groupId != null && plan.groupSize(groupId) > 0) {

				List<StandbyScheduleBodySelectionDto> dayGroupList = row.getGroupList(groupId);

				if (dayGroupList != null) {

					checkEmpty(dayGroupList, groupId);

					for (StandbyScheduleBodySelectionDto standbyScheduleBodySelectionDto : dayGroupList) {

						try {

							addUserString(i + 1, standbyScheduleBodySelectionDto, reportGroupDto, reportDto,
									lastBodyMap, dayGroupList);
						} catch (Exception e) {
							LOGGER.info("Can't set ScheduleBody " + standbyScheduleBodySelectionDto.getId()
									+ ". Error on User or Group!");
						}

					}

				}
			}

		}

		return reportGroupDto;
	}

	public void checkEmpty(List<StandbyScheduleBodySelectionDto> dayGroupList, Long groupId) {
		if (dayGroupList.isEmpty()) {
			// add empty placeholder
			StandbyScheduleBodySelectionDto placeHoder = new StandbyScheduleBodySelectionDto();
			dayGroupList.add(placeHoder);
			StandbyGroup standbyGroup = standbyGroupRepository.findOne(groupId);

			StandbyGroupSelectionDto dto = (StandbyGroupSelectionDto) entityConverter.convertEntityToDto(standbyGroup,
					new StandbyGroupSelectionDto());
			placeHoder.setStandbyGroup(dto);

		}
	}

	public void addUserString(int i, StandbyScheduleBodySelectionDto standbyScheduleBodySelectionDto,
			ReportGroupDto reportGroupDto, ReportDto reportDto, Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap,
			List<StandbyScheduleBodySelectionDto> dayGroupList) throws SpException {
		StandbyScheduleBodySelectionDto lastBody = null;

		if (standbyScheduleBodySelectionDto == null || standbyScheduleBodySelectionDto.getStandbyGroup() == null) {
			throw new SpException(HttpStatus.SC_BAD_REQUEST, "Eine Bereitschaft muss einer Gruppe zugeordnet sein!",
					null);
		}

		lastBody = lastBodyMap.get(standbyScheduleBodySelectionDto.getStandbyGroup().getId());

		String userString = userString(lastBody, standbyScheduleBodySelectionDto, reportDto, dayGroupList.size());

		String str = reportGroupDto.getUserX(i);
		str = ((str == null || str.equals("")) ? "" : (str + "\n")) + userString;
		str = str.trim();

		reportGroupDto.setUserX(i, str);
		reportGroupDto.setGroupX(i, standbyScheduleBodySelectionDto.getStandbyGroup());

		if (standbyScheduleBodySelectionDto.getId() != null) {
			// remember last body in this group
			lastBodyMap.put(standbyScheduleBodySelectionDto.getStandbyGroup().getId(), standbyScheduleBodySelectionDto);
		}
	}

	public String userString(StandbyScheduleBodySelectionDto lastBody, StandbyScheduleBodySelectionDto body,
			ReportDto reportDto, int numberOfEntries) {

		String userString = "";

		if (body.getUser() != null && reportDto != null) {

			Long lastBodyId = (lastBody != null) ? lastBody.getUser().getId().longValue() : null;
			long bodyId = body.getUser().getId().longValue();

			String reportName = reportDto.getReportName();
			userString = body.getUser().getFirstname() + " " + body.getUser().getLastname();

			if (reportName.equals(ReportController.REPORT_MAX10_GROUPS)) {

				userString += "\n" + body.getUser().getPrivateContactData().getPhone() + "\n"
						+ body.getUser().getBusinessContactData().getCellphone() + "\n";

				// print every body on this report
				lastBodyId = null;
			}

			Date dayStart = DateHelper.getStartOfDay(body.getValidFrom());

			if ((lastBodyId != null) && (lastBodyId.longValue() == bodyId)
					&& (body.getValidFrom().getTime() == dayStart.getTime() && (numberOfEntries > 1))) {
				userString = "";
			}

		}

		return userString;
	}

}
