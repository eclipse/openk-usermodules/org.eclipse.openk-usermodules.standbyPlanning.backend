/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "STANDBY_GROUP" database table.
 */
@Entity
@Table(name = "STANDBY_LIST")
public class StandbyList extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_LIST_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_LIST_ID_SEQ", sequenceName = "STANDBY_LIST_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = false)
	private String title;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "standbyList")
	private List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = new ArrayList<>();

	@ManyToMany(mappedBy = "lsStandbyLists")
	private List<StandbyScheduleHeader> lsScheduleHeader = new ArrayList<>();

	public StandbyList() {
		/** default constructor. */
	}

	public StandbyList(Long id) {
		/** default constructor. */
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the lsStandbyListHasStandbyGroup
	 */
	public List<StandbyListHasStandbyGroup> getLsStandbyListHasStandbyGroup() {
		return lsStandbyListHasStandbyGroup;
	}

	/**
	 * @param lsStandbyListHasStandbyGroup
	 *            the lsStandbyListHasStandbyGroup to set
	 */
	public void setLsStandbyListHasStandbyGroup(List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup) {
		this.lsStandbyListHasStandbyGroup = lsStandbyListHasStandbyGroup;
	}

	/**
	 * @return the lsScheduleHeader
	 */
	public List<StandbyScheduleHeader> getLsScheduleHeader() {
		return lsScheduleHeader;
	}

	/**
	 * @param lsScheduleHeader
	 *            the lsScheduleHeader to set
	 */
	public void setLsScheduleHeader(List<StandbyScheduleHeader> lsScheduleHeader) {
		this.lsScheduleHeader = lsScheduleHeader;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	public List<StandbyGroup> getLsStandbyGroups() {
		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();

		List<StandbyListHasStandbyGroup> slhgList = this.getLsStandbyListHasStandbyGroup();
		Collections.sort(slhgList, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));

		for (StandbyListHasStandbyGroup standbyListHasStandbyGroup : slhgList) {
			if (standbyListHasStandbyGroup.getStandbyGroup() != null) {
				lsStandbyGroups.add(standbyListHasStandbyGroup.getStandbyGroup());
			}
		}

		return lsStandbyGroups;
	}

}
