/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.report;

import java.lang.reflect.Method;
import java.util.Date;

import org.apache.http.HttpStatus;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.exceptions.SpException;

public class ReportGroupDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date fromDate;

	private String user1;
	private String user2;
	private String user3;
	private String user4;
	private String user5;
	private String user6;
	private String user7;
	private String user8;
	private String user9;
	private String user10;
	private String user11;
	private String user12;
	private String user13;
	private String user14;
	private String user15;
	private String user16;
	private String user17;
	private String user18;
	private String user19;
	private String user20;
	private String user21;
	private String user22;
	private String user23;
	private String user24;
	private String user25;
	private String user26;
	private String user27;
	private String user28;
	private String user29;
	private String user30;
	private String user31;
	private String user32;
	private String user33;
	private String user34;
	private String user35;
	private String user36;
	private String user37;
	private String user38;
	private String user39;
	private String user40;
	private String user41;
	private String user42;
	private String user43;
	private String user44;
	private String user45;
	private String user46;
	private String user47;
	private String user48;
	private String user49;
	private String user50;
	private String user51;
	private String user52;
	private String user53;
	private String user54;
	private String user55;
	private String user56;
	private String user57;
	private String user58;
	private String user59;
	private String user60;

	private StandbyGroupSelectionDto group1;
	private StandbyGroupSelectionDto group2;
	private StandbyGroupSelectionDto group3;
	private StandbyGroupSelectionDto group4;
	private StandbyGroupSelectionDto group5;
	private StandbyGroupSelectionDto group6;
	private StandbyGroupSelectionDto group7;
	private StandbyGroupSelectionDto group8;
	private StandbyGroupSelectionDto group9;
	private StandbyGroupSelectionDto group10;
	private StandbyGroupSelectionDto group11;
	private StandbyGroupSelectionDto group12;
	private StandbyGroupSelectionDto group13;
	private StandbyGroupSelectionDto group14;
	private StandbyGroupSelectionDto group15;
	private StandbyGroupSelectionDto group16;
	private StandbyGroupSelectionDto group17;
	private StandbyGroupSelectionDto group18;
	private StandbyGroupSelectionDto group19;
	private StandbyGroupSelectionDto group20;
	private StandbyGroupSelectionDto group21;
	private StandbyGroupSelectionDto group22;
	private StandbyGroupSelectionDto group23;
	private StandbyGroupSelectionDto group24;
	private StandbyGroupSelectionDto group25;
	private StandbyGroupSelectionDto group26;
	private StandbyGroupSelectionDto group27;
	private StandbyGroupSelectionDto group28;
	private StandbyGroupSelectionDto group29;
	private StandbyGroupSelectionDto group30;
	private StandbyGroupSelectionDto group31;
	private StandbyGroupSelectionDto group32;
	private StandbyGroupSelectionDto group33;
	private StandbyGroupSelectionDto group34;
	private StandbyGroupSelectionDto group35;
	private StandbyGroupSelectionDto group36;
	private StandbyGroupSelectionDto group37;
	private StandbyGroupSelectionDto group38;
	private StandbyGroupSelectionDto group39;
	private StandbyGroupSelectionDto group40;
	private StandbyGroupSelectionDto group41;
	private StandbyGroupSelectionDto group42;
	private StandbyGroupSelectionDto group43;
	private StandbyGroupSelectionDto group44;
	private StandbyGroupSelectionDto group45;
	private StandbyGroupSelectionDto group46;
	private StandbyGroupSelectionDto group47;
	private StandbyGroupSelectionDto group48;
	private StandbyGroupSelectionDto group49;
	private StandbyGroupSelectionDto group50;
	private StandbyGroupSelectionDto group51;
	private StandbyGroupSelectionDto group52;
	private StandbyGroupSelectionDto group53;
	private StandbyGroupSelectionDto group54;
	private StandbyGroupSelectionDto group55;
	private StandbyGroupSelectionDto group56;
	private StandbyGroupSelectionDto group57;
	private StandbyGroupSelectionDto group58;
	private StandbyGroupSelectionDto group59;
	private StandbyGroupSelectionDto group60;

	public ReportGroupDto() {
		super();
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getUser1() {
		return user1;
	}

	public void setUser1(String user1) {
		this.user1 = user1;
	}

	public String getUser2() {
		return user2;
	}

	public void setUser2(String user2) {
		this.user2 = user2;
	}

	public String getUser3() {
		return user3;
	}

	public void setUser3(String user3) {
		this.user3 = user3;
	}

	public String getUser4() {
		return user4;
	}

	public void setUser4(String user4) {
		this.user4 = user4;
	}

	public String getUser5() {
		return user5;
	}

	public void setUser5(String user5) {
		this.user5 = user5;
	}

	public String getUser6() {
		return user6;
	}

	public void setUser6(String user6) {
		this.user6 = user6;
	}

	public String getUser7() {
		return user7;
	}

	public void setUser7(String user7) {
		this.user7 = user7;
	}

	public String getUser8() {
		return user8;
	}

	public void setUser8(String user8) {
		this.user8 = user8;
	}

	public String getUser9() {
		return user9;
	}

	public void setUser9(String user9) {
		this.user9 = user9;
	}

	public String getUser10() {
		return user10;
	}

	public void setUser10(String user10) {
		this.user10 = user10;
	}

	public StandbyGroupSelectionDto getGroup1() {
		return group1;
	}

	public void setGroup1(StandbyGroupSelectionDto standbyGroupSelectionDto) {
		this.group1 = standbyGroupSelectionDto;
	}

	public StandbyGroupSelectionDto getGroup2() {
		return group2;
	}

	public void setGroup2(StandbyGroupSelectionDto group2) {
		this.group2 = group2;
	}

	public StandbyGroupSelectionDto getGroup3() {
		return group3;
	}

	public void setGroup3(StandbyGroupSelectionDto group3) {
		this.group3 = group3;
	}

	public StandbyGroupSelectionDto getGroup4() {
		return group4;
	}

	public void setGroup4(StandbyGroupSelectionDto group4) {
		this.group4 = group4;
	}

	public StandbyGroupSelectionDto getGroup5() {
		return group5;
	}

	public void setGroup5(StandbyGroupSelectionDto group5) {
		this.group5 = group5;
	}

	public StandbyGroupSelectionDto getGroup6() {
		return group6;
	}

	public void setGroup6(StandbyGroupSelectionDto group6) {
		this.group6 = group6;
	}

	public StandbyGroupSelectionDto getGroup7() {
		return group7;
	}

	public void setGroup7(StandbyGroupSelectionDto group7) {
		this.group7 = group7;
	}

	public StandbyGroupSelectionDto getGroup8() {
		return group8;
	}

	public void setGroup8(StandbyGroupSelectionDto group8) {
		this.group8 = group8;
	}

	public StandbyGroupSelectionDto getGroup9() {
		return group9;
	}

	public void setGroup9(StandbyGroupSelectionDto group9) {
		this.group9 = group9;
	}

	public StandbyGroupSelectionDto getGroup10() {
		return group10;
	}

	public void setGroup10(StandbyGroupSelectionDto group10) {
		this.group10 = group10;
	}

	/**
	 * @return the user11
	 */
	public String getUser11() {
		return user11;
	}

	/**
	 * @param user11 the user11 to set
	 */
	public void setUser11(String user11) {
		this.user11 = user11;
	}

	/**
	 * @return the user12
	 */
	public String getUser12() {
		return user12;
	}

	/**
	 * @param user12 the user12 to set
	 */
	public void setUser12(String user12) {
		this.user12 = user12;
	}

	/**
	 * @return the user13
	 */
	public String getUser13() {
		return user13;
	}

	/**
	 * @param user13 the user13 to set
	 */
	public void setUser13(String user13) {
		this.user13 = user13;
	}

	/**
	 * @return the user14
	 */
	public String getUser14() {
		return user14;
	}

	/**
	 * @param user14 the user14 to set
	 */
	public void setUser14(String user14) {
		this.user14 = user14;
	}

	/**
	 * @return the user15
	 */
	public String getUser15() {
		return user15;
	}

	/**
	 * @param user15 the user15 to set
	 */
	public void setUser15(String user15) {
		this.user15 = user15;
	}

	/**
	 * @return the user16
	 */
	public String getUser16() {
		return user16;
	}

	/**
	 * @param user16 the user16 to set
	 */
	public void setUser16(String user16) {
		this.user16 = user16;
	}

	/**
	 * @return the user17
	 */
	public String getUser17() {
		return user17;
	}

	/**
	 * @param user17 the user17 to set
	 */
	public void setUser17(String user17) {
		this.user17 = user17;
	}

	/**
	 * @return the user18
	 */
	public String getUser18() {
		return user18;
	}

	/**
	 * @param user18 the user18 to set
	 */
	public void setUser18(String user18) {
		this.user18 = user18;
	}

	/**
	 * @return the user19
	 */
	public String getUser19() {
		return user19;
	}

	/**
	 * @param user19 the user19 to set
	 */
	public void setUser19(String user19) {
		this.user19 = user19;
	}

	/**
	 * @return the user20
	 */
	public String getUser20() {
		return user20;
	}

	/**
	 * @param user20 the user20 to set
	 */
	public void setUser20(String user20) {
		this.user20 = user20;
	}

	/**
	 * @return the user21
	 */
	public String getUser21() {
		return user21;
	}

	/**
	 * @param user21 the user21 to set
	 */
	public void setUser21(String user21) {
		this.user21 = user21;
	}

	/**
	 * @return the user22
	 */
	public String getUser22() {
		return user22;
	}

	/**
	 * @param user22 the user22 to set
	 */
	public void setUser22(String user22) {
		this.user22 = user22;
	}

	/**
	 * @return the user23
	 */
	public String getUser23() {
		return user23;
	}

	/**
	 * @param user23 the user23 to set
	 */
	public void setUser23(String user23) {
		this.user23 = user23;
	}

	/**
	 * @return the user24
	 */
	public String getUser24() {
		return user24;
	}

	/**
	 * @param user24 the user24 to set
	 */
	public void setUser24(String user24) {
		this.user24 = user24;
	}

	/**
	 * @return the user25
	 */
	public String getUser25() {
		return user25;
	}

	/**
	 * @param user25 the user25 to set
	 */
	public void setUser25(String user25) {
		this.user25 = user25;
	}

	/**
	 * @return the user26
	 */
	public String getUser26() {
		return user26;
	}

	/**
	 * @param user26 the user26 to set
	 */
	public void setUser26(String user26) {
		this.user26 = user26;
	}

	/**
	 * @return the user27
	 */
	public String getUser27() {
		return user27;
	}

	/**
	 * @param user27 the user27 to set
	 */
	public void setUser27(String user27) {
		this.user27 = user27;
	}

	/**
	 * @return the user28
	 */
	public String getUser28() {
		return user28;
	}

	/**
	 * @param user28 the user28 to set
	 */
	public void setUser28(String user28) {
		this.user28 = user28;
	}

	/**
	 * @return the user29
	 */
	public String getUser29() {
		return user29;
	}

	/**
	 * @param user29 the user29 to set
	 */
	public void setUser29(String user29) {
		this.user29 = user29;
	}

	/**
	 * @return the user30
	 */
	public String getUser30() {
		return user30;
	}

	/**
	 * @param user30 the user30 to set
	 */
	public void setUser30(String user30) {
		this.user30 = user30;
	}

	/**
	 * @return the user31
	 */
	public String getUser31() {
		return user31;
	}

	/**
	 * @param user31 the user31 to set
	 */
	public void setUser31(String user31) {
		this.user31 = user31;
	}

	/**
	 * @return the user32
	 */
	public String getUser32() {
		return user32;
	}

	/**
	 * @param user32 the user32 to set
	 */
	public void setUser32(String user32) {
		this.user32 = user32;
	}

	/**
	 * @return the user33
	 */
	public String getUser33() {
		return user33;
	}

	/**
	 * @param user33 the user33 to set
	 */
	public void setUser33(String user33) {
		this.user33 = user33;
	}

	/**
	 * @return the user34
	 */
	public String getUser34() {
		return user34;
	}

	/**
	 * @param user34 the user34 to set
	 */
	public void setUser34(String user34) {
		this.user34 = user34;
	}

	/**
	 * @return the user35
	 */
	public String getUser35() {
		return user35;
	}

	/**
	 * @param user35 the user35 to set
	 */
	public void setUser35(String user35) {
		this.user35 = user35;
	}

	/**
	 * @return the user36
	 */
	public String getUser36() {
		return user36;
	}

	/**
	 * @param user36 the user36 to set
	 */
	public void setUser36(String user36) {
		this.user36 = user36;
	}

	/**
	 * @return the user37
	 */
	public String getUser37() {
		return user37;
	}

	/**
	 * @param user37 the user37 to set
	 */
	public void setUser37(String user37) {
		this.user37 = user37;
	}

	/**
	 * @return the user38
	 */
	public String getUser38() {
		return user38;
	}

	/**
	 * @param user38 the user38 to set
	 */
	public void setUser38(String user38) {
		this.user38 = user38;
	}

	/**
	 * @return the user39
	 */
	public String getUser39() {
		return user39;
	}

	/**
	 * @param user39 the user39 to set
	 */
	public void setUser39(String user39) {
		this.user39 = user39;
	}

	/**
	 * @return the user40
	 */
	public String getUser40() {
		return user40;
	}

	/**
	 * @param user40 the user40 to set
	 */
	public void setUser40(String user40) {
		this.user40 = user40;
	}

	/**
	 * @return the user41
	 */
	public String getUser41() {
		return user41;
	}

	/**
	 * @param user41 the user41 to set
	 */
	public void setUser41(String user41) {
		this.user41 = user41;
	}

	/**
	 * @return the user42
	 */
	public String getUser42() {
		return user42;
	}

	/**
	 * @param user42 the user42 to set
	 */
	public void setUser42(String user42) {
		this.user42 = user42;
	}

	/**
	 * @return the user43
	 */
	public String getUser43() {
		return user43;
	}

	/**
	 * @param user43 the user43 to set
	 */
	public void setUser43(String user43) {
		this.user43 = user43;
	}

	/**
	 * @return the user44
	 */
	public String getUser44() {
		return user44;
	}

	/**
	 * @param user44 the user44 to set
	 */
	public void setUser44(String user44) {
		this.user44 = user44;
	}

	/**
	 * @return the user45
	 */
	public String getUser45() {
		return user45;
	}

	/**
	 * @param user45 the user45 to set
	 */
	public void setUser45(String user45) {
		this.user45 = user45;
	}

	/**
	 * @return the user46
	 */
	public String getUser46() {
		return user46;
	}

	/**
	 * @param user46 the user46 to set
	 */
	public void setUser46(String user46) {
		this.user46 = user46;
	}

	/**
	 * @return the user47
	 */
	public String getUser47() {
		return user47;
	}

	/**
	 * @param user47 the user47 to set
	 */
	public void setUser47(String user47) {
		this.user47 = user47;
	}

	/**
	 * @return the user48
	 */
	public String getUser48() {
		return user48;
	}

	/**
	 * @param user48 the user48 to set
	 */
	public void setUser48(String user48) {
		this.user48 = user48;
	}

	/**
	 * @return the user49
	 */
	public String getUser49() {
		return user49;
	}

	/**
	 * @param user49 the user49 to set
	 */
	public void setUser49(String user49) {
		this.user49 = user49;
	}

	/**
	 * @return the user50
	 */
	public String getUser50() {
		return user50;
	}

	/**
	 * @param user50 the user50 to set
	 */
	public void setUser50(String user50) {
		this.user50 = user50;
	}

	/**
	 * @return the user51
	 */
	public String getUser51() {
		return user51;
	}

	/**
	 * @param user51 the user51 to set
	 */
	public void setUser51(String user51) {
		this.user51 = user51;
	}

	/**
	 * @return the user52
	 */
	public String getUser52() {
		return user52;
	}

	/**
	 * @param user52 the user52 to set
	 */
	public void setUser52(String user52) {
		this.user52 = user52;
	}

	/**
	 * @return the user53
	 */
	public String getUser53() {
		return user53;
	}

	/**
	 * @param user53 the user53 to set
	 */
	public void setUser53(String user53) {
		this.user53 = user53;
	}

	/**
	 * @return the user54
	 */
	public String getUser54() {
		return user54;
	}

	/**
	 * @param user54 the user54 to set
	 */
	public void setUser54(String user54) {
		this.user54 = user54;
	}

	/**
	 * @return the user55
	 */
	public String getUser55() {
		return user55;
	}

	/**
	 * @param user55 the user55 to set
	 */
	public void setUser55(String user55) {
		this.user55 = user55;
	}

	/**
	 * @return the user56
	 */
	public String getUser56() {
		return user56;
	}

	/**
	 * @param user56 the user56 to set
	 */
	public void setUser56(String user56) {
		this.user56 = user56;
	}

	/**
	 * @return the user57
	 */
	public String getUser57() {
		return user57;
	}

	/**
	 * @param user57 the user57 to set
	 */
	public void setUser57(String user57) {
		this.user57 = user57;
	}

	/**
	 * @return the user58
	 */
	public String getUser58() {
		return user58;
	}

	/**
	 * @param user58 the user58 to set
	 */
	public void setUser58(String user58) {
		this.user58 = user58;
	}

	/**
	 * @return the user59
	 */
	public String getUser59() {
		return user59;
	}

	/**
	 * @param user59 the user59 to set
	 */
	public void setUser59(String user59) {
		this.user59 = user59;
	}

	/**
	 * @return the user60
	 */
	public String getUser60() {
		return user60;
	}

	/**
	 * @param user60 the user60 to set
	 */
	public void setUser60(String user60) {
		this.user60 = user60;
	}

	/**
	 * @return the group11
	 */
	public StandbyGroupSelectionDto getGroup11() {
		return group11;
	}

	/**
	 * @param group11 the group11 to set
	 */
	public void setGroup11(StandbyGroupSelectionDto group11) {
		this.group11 = group11;
	}

	/**
	 * @return the group12
	 */
	public StandbyGroupSelectionDto getGroup12() {
		return group12;
	}

	/**
	 * @param group12 the group12 to set
	 */
	public void setGroup12(StandbyGroupSelectionDto group12) {
		this.group12 = group12;
	}

	/**
	 * @return the group13
	 */
	public StandbyGroupSelectionDto getGroup13() {
		return group13;
	}

	/**
	 * @param group13 the group13 to set
	 */
	public void setGroup13(StandbyGroupSelectionDto group13) {
		this.group13 = group13;
	}

	/**
	 * @return the group14
	 */
	public StandbyGroupSelectionDto getGroup14() {
		return group14;
	}

	/**
	 * @param group14 the group14 to set
	 */
	public void setGroup14(StandbyGroupSelectionDto group14) {
		this.group14 = group14;
	}

	/**
	 * @return the group15
	 */
	public StandbyGroupSelectionDto getGroup15() {
		return group15;
	}

	/**
	 * @param group15 the group15 to set
	 */
	public void setGroup15(StandbyGroupSelectionDto group15) {
		this.group15 = group15;
	}

	/**
	 * @return the group16
	 */
	public StandbyGroupSelectionDto getGroup16() {
		return group16;
	}

	/**
	 * @param group16 the group16 to set
	 */
	public void setGroup16(StandbyGroupSelectionDto group16) {
		this.group16 = group16;
	}

	/**
	 * @return the group17
	 */
	public StandbyGroupSelectionDto getGroup17() {
		return group17;
	}

	/**
	 * @param group17 the group17 to set
	 */
	public void setGroup17(StandbyGroupSelectionDto group17) {
		this.group17 = group17;
	}

	/**
	 * @return the group18
	 */
	public StandbyGroupSelectionDto getGroup18() {
		return group18;
	}

	/**
	 * @param group18 the group18 to set
	 */
	public void setGroup18(StandbyGroupSelectionDto group18) {
		this.group18 = group18;
	}

	/**
	 * @return the group19
	 */
	public StandbyGroupSelectionDto getGroup19() {
		return group19;
	}

	/**
	 * @param group19 the group19 to set
	 */
	public void setGroup19(StandbyGroupSelectionDto group19) {
		this.group19 = group19;
	}

	/**
	 * @return the group20
	 */
	public StandbyGroupSelectionDto getGroup20() {
		return group20;
	}

	/**
	 * @param group20 the group20 to set
	 */
	public void setGroup20(StandbyGroupSelectionDto group20) {
		this.group20 = group20;
	}

	/**
	 * @return the group21
	 */
	public StandbyGroupSelectionDto getGroup21() {
		return group21;
	}

	/**
	 * @param group21 the group21 to set
	 */
	public void setGroup21(StandbyGroupSelectionDto group21) {
		this.group21 = group21;
	}

	/**
	 * @return the group22
	 */
	public StandbyGroupSelectionDto getGroup22() {
		return group22;
	}

	/**
	 * @param group22 the group22 to set
	 */
	public void setGroup22(StandbyGroupSelectionDto group22) {
		this.group22 = group22;
	}

	/**
	 * @return the group23
	 */
	public StandbyGroupSelectionDto getGroup23() {
		return group23;
	}

	/**
	 * @param group23 the group23 to set
	 */
	public void setGroup23(StandbyGroupSelectionDto group23) {
		this.group23 = group23;
	}

	/**
	 * @return the group24
	 */
	public StandbyGroupSelectionDto getGroup24() {
		return group24;
	}

	/**
	 * @param group24 the group24 to set
	 */
	public void setGroup24(StandbyGroupSelectionDto group24) {
		this.group24 = group24;
	}

	/**
	 * @return the group25
	 */
	public StandbyGroupSelectionDto getGroup25() {
		return group25;
	}

	/**
	 * @param group25 the group25 to set
	 */
	public void setGroup25(StandbyGroupSelectionDto group25) {
		this.group25 = group25;
	}

	/**
	 * @return the group26
	 */
	public StandbyGroupSelectionDto getGroup26() {
		return group26;
	}

	/**
	 * @param group26 the group26 to set
	 */
	public void setGroup26(StandbyGroupSelectionDto group26) {
		this.group26 = group26;
	}

	/**
	 * @return the group27
	 */
	public StandbyGroupSelectionDto getGroup27() {
		return group27;
	}

	/**
	 * @param group27 the group27 to set
	 */
	public void setGroup27(StandbyGroupSelectionDto group27) {
		this.group27 = group27;
	}

	/**
	 * @return the group28
	 */
	public StandbyGroupSelectionDto getGroup28() {
		return group28;
	}

	/**
	 * @param group28 the group28 to set
	 */
	public void setGroup28(StandbyGroupSelectionDto group28) {
		this.group28 = group28;
	}

	/**
	 * @return the group29
	 */
	public StandbyGroupSelectionDto getGroup29() {
		return group29;
	}

	/**
	 * @param group29 the group29 to set
	 */
	public void setGroup29(StandbyGroupSelectionDto group29) {
		this.group29 = group29;
	}

	/**
	 * @return the group30
	 */
	public StandbyGroupSelectionDto getGroup30() {
		return group30;
	}

	/**
	 * @param group30 the group30 to set
	 */
	public void setGroup30(StandbyGroupSelectionDto group30) {
		this.group30 = group30;
	}

	/**
	 * @return the group31
	 */
	public StandbyGroupSelectionDto getGroup31() {
		return group31;
	}

	/**
	 * @param group31 the group31 to set
	 */
	public void setGroup31(StandbyGroupSelectionDto group31) {
		this.group31 = group31;
	}

	/**
	 * @return the group32
	 */
	public StandbyGroupSelectionDto getGroup32() {
		return group32;
	}

	/**
	 * @param group32 the group32 to set
	 */
	public void setGroup32(StandbyGroupSelectionDto group32) {
		this.group32 = group32;
	}

	/**
	 * @return the group33
	 */
	public StandbyGroupSelectionDto getGroup33() {
		return group33;
	}

	/**
	 * @param group33 the group33 to set
	 */
	public void setGroup33(StandbyGroupSelectionDto group33) {
		this.group33 = group33;
	}

	/**
	 * @return the group34
	 */
	public StandbyGroupSelectionDto getGroup34() {
		return group34;
	}

	/**
	 * @param group34 the group34 to set
	 */
	public void setGroup34(StandbyGroupSelectionDto group34) {
		this.group34 = group34;
	}

	/**
	 * @return the group35
	 */
	public StandbyGroupSelectionDto getGroup35() {
		return group35;
	}

	/**
	 * @param group35 the group35 to set
	 */
	public void setGroup35(StandbyGroupSelectionDto group35) {
		this.group35 = group35;
	}

	/**
	 * @return the group36
	 */
	public StandbyGroupSelectionDto getGroup36() {
		return group36;
	}

	/**
	 * @param group36 the group36 to set
	 */
	public void setGroup36(StandbyGroupSelectionDto group36) {
		this.group36 = group36;
	}

	/**
	 * @return the group37
	 */
	public StandbyGroupSelectionDto getGroup37() {
		return group37;
	}

	/**
	 * @param group37 the group37 to set
	 */
	public void setGroup37(StandbyGroupSelectionDto group37) {
		this.group37 = group37;
	}

	/**
	 * @return the group38
	 */
	public StandbyGroupSelectionDto getGroup38() {
		return group38;
	}

	/**
	 * @param group38 the group38 to set
	 */
	public void setGroup38(StandbyGroupSelectionDto group38) {
		this.group38 = group38;
	}

	/**
	 * @return the group39
	 */
	public StandbyGroupSelectionDto getGroup39() {
		return group39;
	}

	/**
	 * @param group39 the group39 to set
	 */
	public void setGroup39(StandbyGroupSelectionDto group39) {
		this.group39 = group39;
	}

	/**
	 * @return the group40
	 */
	public StandbyGroupSelectionDto getGroup40() {
		return group40;
	}

	/**
	 * @param group40 the group40 to set
	 */
	public void setGroup40(StandbyGroupSelectionDto group40) {
		this.group40 = group40;
	}

	/**
	 * @return the group41
	 */
	public StandbyGroupSelectionDto getGroup41() {
		return group41;
	}

	/**
	 * @param group41 the group41 to set
	 */
	public void setGroup41(StandbyGroupSelectionDto group41) {
		this.group41 = group41;
	}

	/**
	 * @return the group42
	 */
	public StandbyGroupSelectionDto getGroup42() {
		return group42;
	}

	/**
	 * @param group42 the group42 to set
	 */
	public void setGroup42(StandbyGroupSelectionDto group42) {
		this.group42 = group42;
	}

	/**
	 * @return the group43
	 */
	public StandbyGroupSelectionDto getGroup43() {
		return group43;
	}

	/**
	 * @param group43 the group43 to set
	 */
	public void setGroup43(StandbyGroupSelectionDto group43) {
		this.group43 = group43;
	}

	/**
	 * @return the group44
	 */
	public StandbyGroupSelectionDto getGroup44() {
		return group44;
	}

	/**
	 * @param group44 the group44 to set
	 */
	public void setGroup44(StandbyGroupSelectionDto group44) {
		this.group44 = group44;
	}

	/**
	 * @return the group45
	 */
	public StandbyGroupSelectionDto getGroup45() {
		return group45;
	}

	/**
	 * @param group45 the group45 to set
	 */
	public void setGroup45(StandbyGroupSelectionDto group45) {
		this.group45 = group45;
	}

	/**
	 * @return the group46
	 */
	public StandbyGroupSelectionDto getGroup46() {
		return group46;
	}

	/**
	 * @param group46 the group46 to set
	 */
	public void setGroup46(StandbyGroupSelectionDto group46) {
		this.group46 = group46;
	}

	/**
	 * @return the group47
	 */
	public StandbyGroupSelectionDto getGroup47() {
		return group47;
	}

	/**
	 * @param group47 the group47 to set
	 */
	public void setGroup47(StandbyGroupSelectionDto group47) {
		this.group47 = group47;
	}

	/**
	 * @return the group48
	 */
	public StandbyGroupSelectionDto getGroup48() {
		return group48;
	}

	/**
	 * @param group48 the group48 to set
	 */
	public void setGroup48(StandbyGroupSelectionDto group48) {
		this.group48 = group48;
	}

	/**
	 * @return the group49
	 */
	public StandbyGroupSelectionDto getGroup49() {
		return group49;
	}

	/**
	 * @param group49 the group49 to set
	 */
	public void setGroup49(StandbyGroupSelectionDto group49) {
		this.group49 = group49;
	}

	/**
	 * @return the group50
	 */
	public StandbyGroupSelectionDto getGroup50() {
		return group50;
	}

	/**
	 * @param group50 the group50 to set
	 */
	public void setGroup50(StandbyGroupSelectionDto group50) {
		this.group50 = group50;
	}

	/**
	 * @return the group51
	 */
	public StandbyGroupSelectionDto getGroup51() {
		return group51;
	}

	/**
	 * @param group51 the group51 to set
	 */
	public void setGroup51(StandbyGroupSelectionDto group51) {
		this.group51 = group51;
	}

	/**
	 * @return the group52
	 */
	public StandbyGroupSelectionDto getGroup52() {
		return group52;
	}

	/**
	 * @param group52 the group52 to set
	 */
	public void setGroup52(StandbyGroupSelectionDto group52) {
		this.group52 = group52;
	}

	/**
	 * @return the group53
	 */
	public StandbyGroupSelectionDto getGroup53() {
		return group53;
	}

	/**
	 * @param group53 the group53 to set
	 */
	public void setGroup53(StandbyGroupSelectionDto group53) {
		this.group53 = group53;
	}

	/**
	 * @return the group54
	 */
	public StandbyGroupSelectionDto getGroup54() {
		return group54;
	}

	/**
	 * @param group54 the group54 to set
	 */
	public void setGroup54(StandbyGroupSelectionDto group54) {
		this.group54 = group54;
	}

	/**
	 * @return the group55
	 */
	public StandbyGroupSelectionDto getGroup55() {
		return group55;
	}

	/**
	 * @param group55 the group55 to set
	 */
	public void setGroup55(StandbyGroupSelectionDto group55) {
		this.group55 = group55;
	}

	/**
	 * @return the group56
	 */
	public StandbyGroupSelectionDto getGroup56() {
		return group56;
	}

	/**
	 * @param group56 the group56 to set
	 */
	public void setGroup56(StandbyGroupSelectionDto group56) {
		this.group56 = group56;
	}

	/**
	 * @return the group57
	 */
	public StandbyGroupSelectionDto getGroup57() {
		return group57;
	}

	/**
	 * @param group57 the group57 to set
	 */
	public void setGroup57(StandbyGroupSelectionDto group57) {
		this.group57 = group57;
	}

	/**
	 * @return the group58
	 */
	public StandbyGroupSelectionDto getGroup58() {
		return group58;
	}

	/**
	 * @param group58 the group58 to set
	 */
	public void setGroup58(StandbyGroupSelectionDto group58) {
		this.group58 = group58;
	}

	/**
	 * @return the group59
	 */
	public StandbyGroupSelectionDto getGroup59() {
		return group59;
	}

	/**
	 * @param group59 the group59 to set
	 */
	public void setGroup59(StandbyGroupSelectionDto group59) {
		this.group59 = group59;
	}

	/**
	 * @return the group60
	 */
	public StandbyGroupSelectionDto getGroup60() {
		return group60;
	}

	/**
	 * @param group60 the group60 to set
	 */
	public void setGroup60(StandbyGroupSelectionDto group60) {
		this.group60 = group60;
	}

	public String getUserX(int i) throws SpException {
		String str = null;
		try {

			String methodName = "getUser" + i;
			Method m = getClass().getMethod(methodName);
			str = (String) m.invoke(this);

		} catch (Exception e) {
			throw new SpException(HttpStatus.SC_BAD_REQUEST, "Bereitschaft kann nicht gesetzt werden!", null);
		}
		return str;
	}

	public void setUserX(int i, String userString) throws SpException {
		try {

			String methodName = "setUser" + i;
			Method m = getClass().getMethod(methodName, String.class);
			m.invoke(this, userString);

		} catch (Exception e) {
			throw new SpException(HttpStatus.SC_BAD_REQUEST, "Bereitschaft kann nicht gesetzt werden!", null);
		}
	}

	public void setGroupX(int i, StandbyGroupSelectionDto standbyGroup) throws SpException {
		try {

			String methodName = "setGroup" + i;
			Method m = getClass().getMethod(methodName, StandbyGroupSelectionDto.class);
			m.invoke(this, standbyGroup);

		} catch (Exception e) {
			throw new SpException(HttpStatus.SC_BAD_REQUEST, "Gruppe kann nicht gesetzt werden!", null);
		}
	}

}
