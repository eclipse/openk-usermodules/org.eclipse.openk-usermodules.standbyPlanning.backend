/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest.reports;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiParam;

@Path("report")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReportRestService extends BaseResource {

	public static final String EXCEL_MEDIA_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	public ReportRestService() {
		super(Logger.getLogger(ReportRestService.class.getName()), new FileHelper());
	}

	@Autowired
	private ReportController reportController;

	@Autowired
	private StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@GET
	@Path("/list")
	public Response getReports(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<ReportDto>> invokable = modusr -> reportController.getReports();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Path("/generate/{reportName}")
	@Deprecated
	public Response generateReport(@PathParam("reportName") String reportName,
			@QueryParam("standByListId") Long standByListId, @QueryParam("standByGroupId") Long standByGroupId,
			@QueryParam("userId") Long userId, @QueryParam("printFormat") String printFormat,
			@QueryParam("fromDate") Date fromDate, @QueryParam("toDate") Date toDate,
			@QueryParam("reportLevel") String reportLevel, @Context HttpServletResponse response,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt)
			throws IOException {

		ReportDto reportDto = new ReportDto();
		reportDto.setReportName(reportName);
		reportDto.setPrintFormat(printFormat.toLowerCase());
		if (standByGroupId != null) {
			reportDto.setStandByGroup(standbyGroupRepository.findOne(standByGroupId));
		}
		if (standByListId != null) {
			reportDto.setStandByList(standbyListRepository.findOne(standByListId));
		}
		reportDto.setUserId(userId);
		reportDto.setReportLevel(reportLevel);

		reportDto.setValidFromDate(fromDate);
		reportDto.setValidToDate(toDate);

		try {
			File reportFile = reportController.generateReport(reportDto);
			ModifyingInvokable<File> invokable = modusr -> reportFile;
			reportController.setResponseData(response, reportDto);
			reportController.flushReportFile(reportFile, response.getOutputStream());
			String[] securityRoles = Globals.getAllRolls();
			return invokeRunnable(jwt, securityRoles, invokable);
		} catch (SpException spException) {

			return Response.status(HttpStatus.BAD_REQUEST.value()).entity(spException).type(MediaType.APPLICATION_JSON)
					.build();
		}

	}

	@PUT
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Path("/generate")
	public Response generateReportNew(@Context HttpServletResponse response,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			ReportDto reportDto) throws IOException {

		try {
			reportDto.createDatesFromStrings();
			File reportFile = reportController.generateReport(reportDto);
			

			reportController.setResponseData(response, reportDto);
			reportController.flushReportFile(reportFile, response.getOutputStream());
			String[] securityRoles = Globals.getAllRolls();
			ModifyingInvokable<HttpServletResponse> invokable = modusr -> response;
			return invokeRunnableStreaming(jwt, securityRoles, invokable);

		} catch (SpException spException) {
			logger.error(spException, spException);
			throw new IOException();
//			return Response.status(HttpStatus.BAD_REQUEST.value()).entity(spException).type(MediaType.APPLICATION_JSON)
//					.build();
		}

	}

	/**
	 * GET Resource to generate Reports.
	 * 
	 * @param response
	 * @param jwt
	 * @param printFormat
	 * @param reportName
	 * @param standByListId
	 * @param statusId
	 * @param validFrom
	 * @param validTo
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON, EXCEL_MEDIA_TYPE })
	@Path("/generate/get")
	public Response generateReportNew2(@Context HttpServletResponse response,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@QueryParam(value = "printFormat") String printFormat, @QueryParam(value = "reportName") String reportName,
			@QueryParam(value = "standByListId") String standByListId, @QueryParam(value = "statusId") String statusId,
			@QueryParam(value = "validFrom") String validFrom, @QueryParam(value = "validTo") String validTo)
			throws IOException {

		try {
			ReportDto reportDto = new ReportDto();
			reportDto.setPrintFormat(printFormat);
			reportDto.setReportName(reportName);
			reportDto.setStandByListId(Long.parseLong(standByListId));
			reportDto.setStatusId(Long.parseLong(statusId));
			reportDto.setValidFromDate(new DateTime(validFrom).toDate());
			reportDto.setValidToDate(new DateTime(validTo).toDate());

			File reportFile = reportController.generateReport(reportDto);
			ModifyingInvokable<File> invokable = modusr -> reportFile;
			reportController.setResponseData(response, reportDto);
			reportController.flushReportFile(reportFile, response.getOutputStream());
			String[] securityRoles = Globals.getAllRolls();
			return invokeRunnableStreaming(jwt, securityRoles, invokable);

		} catch (SpException spException) {
			logger.error(spException, spException);
			throw new IOException();
		}
	}
}
