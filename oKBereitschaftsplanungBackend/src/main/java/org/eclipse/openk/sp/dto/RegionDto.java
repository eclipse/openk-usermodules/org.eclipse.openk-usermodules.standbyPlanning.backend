/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "REGION" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "RegionDto")
@JsonInclude(Include.NON_NULL)
public class RegionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String regionName;
	private List<RegionHasFunctionDto> lsRegionHasFunctions = new ArrayList<>();
	private List<LocationSelectionDto> lsLocations = new ArrayList<>();

	public RegionDto() {
	}

	public RegionDto(RegionSelectionDto dto) {
		this.id = dto.getId();
		this.regionName = dto.getRegionName();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the lsLocation
	 */
	public List<LocationSelectionDto> getLsLocations() {
		return lsLocations;
	}

	/**
	 * @param lsLocation
	 *            the lsLocation to set
	 */
	public void setLsLocations(List<LocationSelectionDto> lsLocations) {
		this.lsLocations = lsLocations;
	}

	/**
	 * @return the lsRegionHasFunctionsDto
	 */
	public List<RegionHasFunctionDto> getLsRegionHasFunctions() {
		return lsRegionHasFunctions;
	}

	/**
	 * @param lsRegionHasFunctionsDto
	 *            the lsRegionHasFunctionsDto to set
	 */
	public void setLsRegionHasFunctions(List<RegionHasFunctionDto> lsRegionHasFunctionsDto) {
		this.lsRegionHasFunctions = lsRegionHasFunctionsDto;
	}

}
