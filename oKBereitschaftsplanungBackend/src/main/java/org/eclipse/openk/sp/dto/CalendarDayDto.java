/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "CALENDAR" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "CalendarDto")
@JsonInclude(Include.NON_NULL)
public class CalendarDayDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Name ist nicht gesetzt")
	@Size(max = 256, message = "Name mit max. 256 Zeichen")
	private String name;
	@NotNull(message = "Datum ist nicht gesetzt")
	private Date dateIndex;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param title the title to set
	 */
	public void setName(String title) {
		this.name = title;
	}

	/**
	 * @return the date
	 */
	public Date getDateIndex() {
		return dateIndex;
	}

	/**
	 * @param date the date to set
	 */
	public void setDateIndex(Date date) {
		this.dateIndex = date;
	}

}