/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.CalendarController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleArchiveDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsArchiveDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class PlannedDataController extends AbstractController {

	protected static final Logger LOGGER = Logger.getLogger(PlannedDataController.class);

	@Autowired
	private StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@Autowired
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	CalendarController calendarController;

	public StandbyScheduleDto getFilteredPlanByStatus(StandbyScheduleFilterDto standbyScheduleFilterDto, Long statusId,
			boolean allDay) throws SpException {

		try {

			if (standbyScheduleFilterDto == null) {
				SpErrorEntry ee = SpExceptionEnum.MISSING_PARAMETERS_EXCEPTION.getEntry();
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), ee.getMessage());
				LOGGER.error(spE, spE);
				throw spE;
			}

			Long listId = standbyScheduleFilterDto.getStandbyListId();

			if (listId == null || !standbyListRepository.exists(listId)) {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_LIST_WITH_ID, listId);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}

			StandbyList standbyList = standbyListRepository.findOne(listId);

			if (standbyList.getLsStandbyGroups().isEmpty()) {
				SpErrorEntry ee = SpExceptionEnum.ERROR_ON_LIST.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_LIST_WITH_ID_NO_GROUPS, listId);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}

			List<StandbyGroupSelectionDto> listStandbyGroupDto = mapGroupDto(standbyList.getLsStandbyGroups());

			Date startDate = standbyScheduleFilterDto.getValidFrom();
			Date endDate = standbyScheduleFilterDto.getValidTo();

			StandbyScheduleDto plan = new StandbyScheduleDto(startDate, endDate, listStandbyGroupDto);
			plan.setFilter(standbyScheduleFilterDto);
			PlanHeaderDto planHeader = new PlanHeaderDto();
			planHeader.setLabel("Datum/Gruppe");

			planHeader.setListGroups(listStandbyGroupDto);
			plan.setPlanHeader(planHeader);

			List<Long> listIds = new ArrayList<>();
			for (StandbyGroupSelectionDto sbg : listStandbyGroupDto) {
				listIds.add(sbg.getId());
			}

			Long[] groupIds = new Long[listIds.size()];
			listIds.toArray(groupIds);

			// query all bodys for that plan
			List<StandbyScheduleBody> listBodyMatrix = matrixOfPlannedBodysByStatus(groupIds, startDate, endDate,
					statusId, allDay);

			LOGGER.info("query for " + listBodyMatrix.size() + " bodies");

			// convert to dto
			List<StandbyScheduleBodySelectionDto> listBodyDto = new ArrayList<>();
			listBodyDto = entityConverter.convertEntityToDtoList(listBodyMatrix, listBodyDto,
					StandbyScheduleBodySelectionDto.class);

			LOGGER.info("convertetd " + listBodyDto.size() + " bodies");

			for (StandbyScheduleBodySelectionDto standbyScheduleBody : listBodyDto) {

				Date date = DateHelper.getStartOfDay(standbyScheduleBody.getValidFrom());

				PlanRowsDto row = plan.getRow(date);

				if (row != null) {
					row.addStandbyBody(standbyScheduleBody);

					plan.count(standbyScheduleBody);
				} else {
					LOGGER.info("no row for that date found (" + date + ")");
				}
			}

			LOGGER.info("set style for " + plan.getListPlanRows().size() + " row's.");
			// set style
			for (PlanRowsDto row : plan.getListPlanRows()) {
				Date date = DateHelper.getDateFromLabel(row.getLabel());
				setStyle(row, date);
			}

			return plan;

		} catch (SpException e) {
			LOGGER.error(e, e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}

	}

	public List<StandbyGroupSelectionDto> mapGroupDto(List<StandbyGroup> lsStandbyGroups) {
		List<StandbyGroupSelectionDto> listStandbyGroupDto = new ArrayList<>();
		listStandbyGroupDto = entityConverter.convertEntityToDtoList(lsStandbyGroups, listStandbyGroupDto,
				StandbyGroupSelectionDto.class);
		return listStandbyGroupDto;
	}

	@SuppressWarnings("unchecked")
	public StandbyScheduleArchiveDto createPlan(StandbyScheduleFilterDto standbyScheduleFilterDto,
			PlanHeaderDto planHeader, Long statusId) throws SpException {

		Date startDate = DateHelper.getStartOfDay(standbyScheduleFilterDto.getValidFrom());
		Date endDate = DateHelper.getEndOfDay(standbyScheduleFilterDto.getValidTo());

		StandbyScheduleDto plan = new StandbyScheduleDto(startDate, endDate, planHeader.getListGroups());
		plan.setFilter(standbyScheduleFilterDto);
		plan.setPlanHeader(planHeader);

		List<Long> listIds = new ArrayList<>();
		for (StandbyGroupSelectionDto sbg : planHeader.getListGroups()) {
			listIds.add(sbg.getId());
		}

		Long[] groupIds = new Long[listIds.size()];
		listIds.toArray(groupIds);

		// query all bodys for that plan
		List<StandbyScheduleBody> listBodyMatrix = matrixOfPlannedBodysByStatus(groupIds, startDate, endDate, statusId,
				true);

		for (StandbyScheduleBody standbyScheduleBody : listBodyMatrix) {

			StandbyScheduleBodySelectionDto standbyScheduleBodySelectionDto = (StandbyScheduleBodySelectionDto) entityConverter
					.convertEntityToDto(standbyScheduleBody, new StandbyScheduleBodySelectionDto());

			Date date = DateHelper.getStartOfDay(standbyScheduleBodySelectionDto.getValidFrom());

			PlanRowsDto row = plan.getRow(date);
			if (row != null) {
				setStyle(row, date);

				row.addStandbyBody(standbyScheduleBodySelectionDto);
			} else {
				LOGGER.info("no row for that date found (" + date + ")");
			}

		}

		List<PlanRowsArchiveDto> arcRows = new ArrayList<>();
		for (PlanRowsDto row : plan.getListPlanRows()) {
			PlanRowsArchiveDto arcRow = new PlanRowsArchiveDto();
			arcRow.setLabel(row.getLabel());
			arcRow.setListGroupBodys(row.getListGroupBodys());
			arcRows.add(arcRow);
		}

		StandbyScheduleArchiveDto planArchiv = new StandbyScheduleArchiveDto();
		planArchiv.setFilter(plan.getFilter());
		planArchiv.setPlanHeader(plan.getPlanHeader());
		planArchiv.setListPlanRows(arcRows);

		return planArchiv;
	}

	public void setStyle(PlanRowsDto dto, Date date) throws SpException {

		if (dto == null || dto.getStyle() != null) {
			return;
		}

		int weekday = DateHelper.getDayOfWeek(date);

		if (weekday == 6 || weekday == 7 || calendarController.containsDate(date)) {
			dto.setStyle(SpMsg.TXT_NOT_WORKING_DAY);
		} else {
			dto.setStyle(SpMsg.TXT_WORKING_DAY);
		}
	}

	public StandbyScheduleArchiveDto getFilteredPlanByStatusForArchive(
			StandbyScheduleFilterDto standbyScheduleFilterDto, Long statusId) throws SpException {

		try {

			if (standbyScheduleFilterDto == null) {
				SpErrorEntry ee = SpExceptionEnum.MISSING_PARAMETERS_EXCEPTION.getEntry();
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), ee.getMessage());
				LOGGER.error(spE, spE);
				throw spE;
			}

			Long listId = standbyScheduleFilterDto.getStandbyListId();

			if (listId == null || !standbyListRepository.exists(listId)) {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_LIST_WITH_ID, listId);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}

			StandbyList standbyList = standbyListRepository.findOne(listId);

			if (standbyScheduleFilterDto.getTitle() == null || standbyScheduleFilterDto.getTitle().equals("")) {
				standbyScheduleFilterDto.setTitle("Liste: " + standbyList.getTitle());
			}

			StringBuilder comment = new StringBuilder();
			comment.append("Gruppen: ");

			List<StandbyGroup> listStandbyGroup = standbyList.getLsStandbyGroups();
			for (StandbyGroup standbyGroup : listStandbyGroup) {
				comment.append(standbyGroup.getTitle() + ", ");
			}

			if (standbyScheduleFilterDto.getComment() == null || standbyScheduleFilterDto.getComment().equals("")) {
				standbyScheduleFilterDto.setComment(comment.toString());
			}

			PlanHeaderDto planHeader = new PlanHeaderDto();
			planHeader.setLabel("Datum/Gruppe");
			List<StandbyGroupSelectionDto> listStandbyGroupDto = entityConverter.convertEntityToDtoList(
					standbyList.getLsStandbyGroups(), planHeader.getListGroups(), StandbyGroupSelectionDto.class);
			planHeader.setListGroups(listStandbyGroupDto);

			StandbyScheduleArchiveDto plan = createPlan(standbyScheduleFilterDto, planHeader, statusId);

			return plan;

		} catch (SpException e) {
			LOGGER.error(e, e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}

	}

	public StandbyScheduleArchiveDto getFilteredPlanByStatusByGroupsForArchive(
			StandbyScheduleFilterDto standbyScheduleFilterDto, Long statusId) throws SpException {

		try {

			if (standbyScheduleFilterDto == null) {
				SpErrorEntry ee = SpExceptionEnum.MISSING_PARAMETERS_EXCEPTION.getEntry();
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), ee.getMessage());
				LOGGER.error(spE, spE);
				throw spE;
			}

			List<TransferGroupDto> listTransferGroupDto = standbyScheduleFilterDto.getLsTransferGroup();

			if (listTransferGroupDto == null) {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}

			String comment = "";

			if (standbyScheduleFilterDto.getComment() != null) {
				comment = standbyScheduleFilterDto.getComment();
			}
			comment += "  Gruppen: ";

			List<StandbyGroupSelectionDto> listStandbyGroupDto = new ArrayList<>();
			for (TransferGroupDto transferGroupDto : listTransferGroupDto) {
				Long groupId = transferGroupDto.getGroupId();
				StandbyGroup standbyGroup = standbyGroupRepository.findOne(groupId);

				comment += standbyGroup.getTitle() + ", ";

				StandbyGroupSelectionDto sbgDto = (StandbyGroupSelectionDto) entityConverter
						.convertEntityToDto(standbyGroup, new StandbyGroupSelectionDto());
				listStandbyGroupDto.add(sbgDto);
			}

			standbyScheduleFilterDto.setComment(comment);

			PlanHeaderDto planHeader = new PlanHeaderDto();
			planHeader.setLabel("Datum/Gruppe");
			planHeader.setListGroups(listStandbyGroupDto);

			StandbyScheduleArchiveDto plan = createPlan(standbyScheduleFilterDto, planHeader, statusId);

			return plan;

		} catch (SpException e) {
			LOGGER.error(e, e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}

	}

	/**
	 * Method to return the list of @{link StandbyScheduleBody} ids that are
	 * available for the current dateTime slot.
	 * 
	 * @param dto
	 * @return
	 */
	public List<Long> getIdsOfExistingScheduleBodies(StandbyScheduleBlueprintDto dto) {
		Long standbyGroupId = dto.getStandbyGroupId();
		Date validFrom = dto.getValidFrom();
		Date validTo = DateHelper.getEndOfDay(dto.getValidTo());
		Long statusId = dto.getStatusId();
		return standbyScheduleBodyRepository.findIdsByGroupAndDateAndStatus(standbyGroupId, validFrom, validTo,
				statusId);
	}

	public List<StandbyScheduleBody> getPlannedBodysForGroupAndDayPlanning(StandbyGroupSelectionDto standbyGroup,
			Date dateIndex) {

		// planning
		Long statusId = SpMsg.STATUS_PLANNING;
		List<StandbyScheduleBody> listBodies = getPlannedBodysForGroupAndDayAndStatus(standbyGroup, dateIndex,
				statusId);

		return listBodies;

	}

	public List<StandbyScheduleBody> getPlannedBodysForGroupAndDayClosedPlanning(StandbyGroupSelectionDto standbyGroup,
			Date dateIndex) {

		// planning
		Long statusId = SpMsg.STATUS_CLOSED_PLANNING;
		List<StandbyScheduleBody> listBodies = getPlannedBodysForGroupAndDayAndStatus(standbyGroup, dateIndex,
				statusId);

		return listBodies;

	}

	public List<StandbyScheduleBody> getPlannedBodysForGroupAndDayAndStatus(StandbyGroupSelectionDto standbyGroup,
			Date dateIndex, Long statusId) {

		return getPlannedBodysForGroupAndDayAndStatus(standbyGroup.getId(), dateIndex, statusId);

	}

	public List<StandbyScheduleBody> getPlannedBodysForGroupAndDayAndStatus(Long standbyGroupId, Date dateIndex,
			Long statusId) {

		return getPlannedBodysForGroupIdAndIntervallAndStatus(standbyGroupId, dateIndex, dateIndex, statusId);

	}

	public List<StandbyScheduleBody> getPlannedBodysForGroupIdAndIntervallAndStatus(Long standbyGroupId, Date validFrom,
			Date validTo, Long statusId) {

		validFrom = DateHelper.getStartOfDay(validFrom);
		validTo = DateHelper.getEndOfDay(validTo);
		List<StandbyScheduleBody> listBodies = standbyScheduleBodyRepository.findByGroupAndDateAndStatus(standbyGroupId,
				validFrom, validTo, statusId);

		return listBodies;

	}

	public List<StandbyScheduleBody> matrixOfPlannedBodysByStatus(Long[] groupIds, Date validFrom, Date validTo,
			Long statusId, boolean allDaySearch) {

		Date startDate = validFrom;
		Date endDate = validTo;

		if (allDaySearch) {
			startDate = DateHelper.getStartOfDay(validFrom);
			endDate = DateHelper.getEndOfDay(validTo);
		}

		List<StandbyScheduleBody> listBodies = standbyScheduleBodyRepository.findData(groupIds, startDate, endDate,
				statusId);

		return listBodies;

	}

}
