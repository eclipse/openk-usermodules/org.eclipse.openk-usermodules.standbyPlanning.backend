/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.OrganisationRepository;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.dto.OrganisationSelectionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle USER operations. */
public class OrganisationController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(OrganisationController.class);

	@Autowired
	private OrganisationRepository organisationRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * Method to query all organisations.
	 * 
	 * @param jwt
	 * @return
	 */
	public List<OrganisationDto> getOrganisations() {
		ArrayList<OrganisationDto> listOrganisation = new ArrayList<>();
		Iterable<Organisation> itOrganisationList = organisationRepository.findAllByOrderByOrgaNameAsc();
		for (Organisation organisation : itOrganisationList) {
			listOrganisation
					.add((OrganisationDto) entityConverter.convertEntityToDto(organisation, new OrganisationDto()));
		}
		return listOrganisation;
	}

	/**
	 * Method to query all organisations.
	 * 
	 * @param jwt
	 * @return
	 */
	public List<OrganisationSelectionDto> getOrganisationsSelection() {
		ArrayList<OrganisationSelectionDto> listOrganisation = new ArrayList<>();
		Iterable<Organisation> itOrganisationList = organisationRepository.findAllByOrderByOrgaNameAsc();
		for (Organisation organisation : itOrganisationList) {
			listOrganisation.add((OrganisationSelectionDto) entityConverter.convertEntityToDto(organisation,
					new OrganisationSelectionDto()));
		}
		return listOrganisation;
	}

	/**
	 * Method to select a organisation by its id.
	 * 
	 * @param jwt
	 * @param organisationid
	 * @return
	 */
	public OrganisationDto getOrganisation(Long organisationid) {
		Organisation organisation = organisationRepository.findOne(organisationid);
		return (OrganisationDto) entityConverter.convertEntityToDto(organisation, new OrganisationDto());
	}

	/**
	 * Method to persist a organisation.
	 * 
	 * @param jwt
	 * @param organisationid
	 * @return
	 * @throws HttpStatusException
	 */
	public OrganisationDto saveOrganisation(OrganisationDto organisationDto) {

		Organisation dbOrga = null;
		if (organisationDto.getId() != null) {
			// check if organisation is existing.
			dbOrga = organisationRepository.findOne(organisationDto.getId());
		}

		if (dbOrga == null) {
			// if entity is not existing in db. a new one is getting created.
			dbOrga = new Organisation();
		}

		Organisation organisation = (Organisation) entityConverter.convertDtoToEntity(organisationDto, dbOrga);

		organisation = organisationRepository.save(organisation);

		return (OrganisationDto) entityConverter.convertEntityToDto(organisation, new OrganisationDto());
	}

}
