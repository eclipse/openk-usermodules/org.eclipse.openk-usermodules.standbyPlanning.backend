/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.CustomConverter;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserHasUserFunction;

public class UserHasFunctionToStringConverter implements CustomConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
		if (source instanceof List) {

			return uhfListToString((List) source);

		} else if (source instanceof String) {

			return stringToUHF((String) source);
		}
		return null;
	}

	public String uhfListToString(List source) {
		StringBuilder resultBuf = new StringBuilder("");
		for (Object object : source) {
			UserHasUserFunction userHasUserFunction = (UserHasUserFunction) object;
			if (!resultBuf.toString().equals("")) {
				resultBuf.append(",");
			}
			resultBuf.append(userHasUserFunction.getUserFunction().getFunctionName());
		}

		return resultBuf.toString();
	}

	public List<UserHasUserFunction> stringToUHF(String source) {
		List<UserHasUserFunction> lsUserHasUserFunctions = new ArrayList<>();

		if (source.contains(",")) {
			String[] arr = source.split(",");
			for (String str : arr) {
				addUserHasUserFunction(lsUserHasUserFunctions, str);
			}
		} else {
			addUserHasUserFunction(lsUserHasUserFunctions, source);
		}
		return lsUserHasUserFunctions;
	}
	
	public List<UserHasUserFunction> addUserHasUserFunction(List<UserHasUserFunction> lsUserHasUserFunctions, String str) {
		UserHasUserFunction uhuf = new UserHasUserFunction();
		UserFunction uf = new UserFunction();
		uf.setFunctionName(str);
		uhuf.setUserFunction(uf);
		lsUserHasUserFunctions.add(uhuf);
		
		return lsUserHasUserFunctions;
	}
}
