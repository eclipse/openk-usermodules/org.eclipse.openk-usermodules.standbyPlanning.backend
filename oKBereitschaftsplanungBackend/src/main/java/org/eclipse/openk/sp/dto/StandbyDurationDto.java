/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBYDURATION" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyDurationDto")
@JsonInclude(Include.NON_NULL)
public class StandbyDurationDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long standbyDurationId;
	@NotNull(message = "Zeit ist nicht gesetzt")
	private TimeDto validFrom;
	@NotNull(message = "Zeit ist nicht gesetzt")
	private Integer validDayFrom;
	@NotNull(message = "Tag ist nicht gesetzt")
	private TimeDto validTo;
	@NotNull(message = "Tag ist nicht gesetzt")
	private Integer validDayTo;
	private Date modificationDate;
	@NotNull(message = "Gruppe ist nicht gesetzt")
	private Long standbyGroupId;
	@NotNull(message = "Vorschub ist nicht gesetzt")
	private Boolean nextUserInNextDuration;

	/**
	 * @return the standbyDurationId
	 */
	public Long getStandbyDurationId() {
		return standbyDurationId;
	}

	/**
	 * @param standbyDurationId the standbyDurationId to set
	 */
	public void setStandbyDurationId(Long standbyDurationId) {
		this.standbyDurationId = standbyDurationId;
	}

	/**
	 * @return the validDayFrom
	 */
	public Integer getValidDayFrom() {
		return validDayFrom;
	}

	/**
	 * @param validDayFrom the validDayFrom to set
	 */
	public void setValidDayFrom(Integer validDayFrom) {
		this.validDayFrom = validDayFrom;
	}

	/**
	 * @return the validDayTo
	 */
	public Integer getValidDayTo() {
		return validDayTo;
	}

	/**
	 * @param validDayTo the validDayTo to set
	 */
	public void setValidDayTo(Integer validDayTo) {
		this.validDayTo = validDayTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the standbyGroupId
	 */
	public Long getStandbyGroupId() {
		return standbyGroupId;
	}

	/**
	 * @param standbyGroupId the standbyGroupId to set
	 */
	public void setStandbyGroupId(Long standbyGroupId) {
		this.standbyGroupId = standbyGroupId;
	}

	/**
	 * @return the validFrom
	 */
	public TimeDto getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(TimeDto validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public TimeDto getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(TimeDto validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the nextUserInNextDuration
	 */
	public Boolean getNextUserInNextDuration() {
		return nextUserInNextDuration;
	}

	/**
	 * @param nextUserInNextDuration the nextUserInNextDuration to set
	 */
	public void setNextUserInNextDuration(Boolean nextUserInNextDuration) {
		this.nextUserInNextDuration = nextUserInNextDuration;
	}
}
