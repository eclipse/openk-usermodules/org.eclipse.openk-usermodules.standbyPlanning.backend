/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.AddressDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.OrganisationDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "DistanceService" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "DistanceServiceDto")
@JsonInclude(Include.NON_NULL)
public class DistanceServiceDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private LocationDto location;
	private Long userId;
	private Long bodyId;
	private OrganisationDto organisation;
	private AddressDto privateAddress;
	private String organisationDistance;
	private String privateDisance;

	public DistanceServiceDto() {
		/** default constructor. */
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the organisation
	 */
	public OrganisationDto getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(OrganisationDto organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the privateAddress
	 */
	public AddressDto getPrivateAddress() {
		return privateAddress;
	}

	/**
	 * @param privateAddress the privateAddress to set
	 */
	public void setPrivateAddress(AddressDto privateAddress) {
		this.privateAddress = privateAddress;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public String getOrganisationDistance() {
		return organisationDistance;
	}

	public void setOrganisationDistance(String organisationDistance) {
		this.organisationDistance = organisationDistance;
	}

	public String getPrivateDisance() {
		return privateDisance;
	}

	public void setPrivateDisance(String privateDisance) {
		this.privateDisance = privateDisance;
	}

	public Long getBodyId() {
		return bodyId;
	}

	public void setBodyId(Long bodyId) {
		this.bodyId = bodyId;
	}

}
