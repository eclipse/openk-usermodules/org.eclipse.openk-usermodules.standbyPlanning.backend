/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "REPORTGENERATIONCONFIG" database table.
 */
@Entity
@Table(name = "REPORTGENERATIONCONFIG")
public class ReportGenerationConfig extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REPORTGENERATIONCONFIG_ID_SEQ")
	@SequenceGenerator(name = "REPORTGENERATIONCONFIG_ID_SEQ", sequenceName = "REPORTGENERATIONCONFIG_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "name", length = 256, nullable = false)
	private String name;

	@Column(name = "file_name_pattern", length = 128, nullable = false)
	private String fileNamePattern;
	
	@Column(name = "subject", length = 128, nullable = false)
	private String subject;
	
	@Column(name = "v_to", nullable = true)
	private String to;

	@Column(name = "print_format", length = 128, nullable = false)
	private String printFormat;
	
	@Column(name = "report_name", length = 256, nullable = false)
	private String reportName;
	
	@Column(name = "stand_by_list_id", nullable = false)
	private Long standByListId;
	
	@Column(name = "status_id", nullable = false)
	private Long statusId;
	
	@Column(name = "trigger_week_day", nullable = false)
	private Integer triggerWeekDay;

	@Column(name = "trigger_hour", nullable = false)
	private Integer triggerHour;
	
	@Column(name = "trigger_minute", nullable = false)
	private Integer triggerMinute;
	
	@Column(name = "valid_from_day_offset", nullable = false)
	private Integer validFromDayOffset;
	
	@Column(name = "valid_from_hour", nullable = false)
	private Integer validFromHour;
	
	@Column(name = "valid_from_minute", nullable = false)
	private Integer validFromMinute;

	@Column(name = "valid_to_day_offset", nullable = false)
	private Integer validToDayOffset;
	
	@Column(name = "valid_to_hour", nullable = false)
	private Integer validToHour;
	
	@Column(name = "valid_to_minute", nullable = false)
	private Integer validToMinute;
	
	@Lob
	@Column(name = "email_text", nullable = false)
	private String emailText;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileNamePattern() {
		return fileNamePattern;
	}

	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<String> getTo() {
		if (to == null) {
			return new ArrayList<>();
		}
		return Arrays.asList(to.split(","));
	}

	public void setTo(List<String> toList) {
		this.to = String.join(",", toList);
	}

	public String getPrintFormat() {
		return printFormat;
	}

	public void setPrintFormat(String printFormat) {
		this.printFormat = printFormat;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Long getStandByListId() {
		return standByListId;
	}

	public void setStandByListId(Long standByListId) {
		this.standByListId = standByListId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Integer getTriggerWeekDay() {
		return triggerWeekDay;
	}

	public void setTriggerWeekDay(Integer triggerWeekDay) {
		this.triggerWeekDay = triggerWeekDay;
	}

	public Integer getTriggerHour() {
		return triggerHour;
	}

	public void setTriggerHour(Integer triggerHour) {
		this.triggerHour = triggerHour;
	}

	public Integer getTriggerMinute() {
		return triggerMinute;
	}

	public void setTriggerMinute(Integer triggerMinute) {
		this.triggerMinute = triggerMinute;
	}

	public Integer getValidFromDayOffset() {
		return validFromDayOffset;
	}

	public void setValidFromDayOffset(Integer validFromDayOffset) {
		this.validFromDayOffset = validFromDayOffset;
	}

	public Integer getValidFromHour() {
		return validFromHour;
	}

	public void setValidFromHour(Integer validFromHour) {
		this.validFromHour = validFromHour;
	}

	public Integer getValidFromMinute() {
		return validFromMinute;
	}

	public void setValidFromMinute(Integer validFromMinute) {
		this.validFromMinute = validFromMinute;
	}

	public Integer getValidToDayOffset() {
		return validToDayOffset;
	}

	public void setValidToDayOffset(Integer validToDayOffset) {
		this.validToDayOffset = validToDayOffset;
	}

	public Integer getValidToHour() {
		return validToHour;
	}

	public void setValidToHour(Integer validToHour) {
		this.validToHour = validToHour;
	}

	public Integer getValidToMinute() {
		return validToMinute;
	}

	public void setValidToMinute(Integer validToMinute) {
		this.validToMinute = validToMinute;
	}
	
	public String getEmailText() {
		return emailText;
	}
	
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	@Override
	public int compareTo(AbstractEntity arg0) {
		return (getId().equals(arg0.getId())) ? 1 : 0;
	}

}
