/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.util.DateHelper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * filter criteria
	 */
	private StandbyScheduleFilterDto filter;

	/**
	 * first row
	 */
	private PlanHeaderDto planHeader;

	/**
	 * all other rows
	 */
	private List<PlanRowsDto> listPlanRows = new ArrayList<>();
	private HashMap<Date, PlanRowsDto> dayRowMap = new HashMap<>();
	private HashMap<Long, Integer> groupCountMap = new HashMap<>();

	/**
	 * content
	 */

	public StandbyScheduleDto() {
		/** default constructor. */
	}

	public StandbyScheduleDto(Date validFrom, Date validTo, List<StandbyGroupSelectionDto> listStandbyGroupDto) {

		Date dateIndex = DateHelper.getStartOfDay(validFrom);
		Date endDate = DateHelper.getEndOfDay(validTo);

		while (DateHelper.isDateBefore(dateIndex, endDate)) {
			PlanRowsDto dto = new PlanRowsDto(listStandbyGroupDto);
			dto.setLabel(DateHelper.getLabelFormDate(dateIndex));
			listPlanRows.add(dto);
			dayRowMap.put(dateIndex, dto);
			dateIndex = DateHelper.addDaysToDate(dateIndex, 1);
		}

	}

	public PlanRowsDto getRow(Date dateIndex) {
		if (dateIndex == null) {
			return null;
		}
		return dayRowMap.get(dateIndex);
	}

	/**
	 * @return the filter
	 */
	public StandbyScheduleFilterDto getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(StandbyScheduleFilterDto filter) {
		this.filter = filter;
	}

	/**
	 * @return the planHeader
	 */
	public PlanHeaderDto getPlanHeader() {
		return planHeader;
	}

	/**
	 * @param planHeader the planHeader to set
	 */
	public void setPlanHeader(PlanHeaderDto planHeader) {
		this.planHeader = planHeader;
	}

	/**
	 * @return the listPlanRows
	 */
	public List<PlanRowsDto> getListPlanRows() {
		return listPlanRows;
	}

	/**
	 * @param listPlanRows the listPlanRows to set
	 */
	public void setListPlanRows(List<PlanRowsDto> listPlanRows) {
		this.listPlanRows = listPlanRows;
	}

	/**
	 * 
	 * @param body
	 */
	public void count(StandbyScheduleBodySelectionDto body) {
		Integer counter = Integer.valueOf(0);
		Long groupId = body.getStandbyGroup().getId();
		if (groupCountMap.containsKey(groupId)) {
			counter = groupCountMap.get(groupId);
		}
		counter++;
		groupCountMap.put(groupId, counter);

	}

	public Integer groupSize(Long groupId) {
		Integer size = Integer.valueOf(0);
		if (groupCountMap.containsKey(groupId)) {
			size = groupCountMap.get(groupId);
		}

		return size;
	}

}
