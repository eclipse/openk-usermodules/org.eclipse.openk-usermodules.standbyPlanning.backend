/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.RegionDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle REGION operations. */
public class RegionController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(RegionController.class);

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private EntityConverter entityConverter;

	protected List<Long> lsTempRemovalIds = new ArrayList<>();

	/**
	 * Method to query all regions.
	 * 
	 * @return
	 */
	public List<RegionDto> getRegions() {
		ArrayList<RegionDto> listRegion = new ArrayList<>();
		Iterable<Region> itRegionList = regionRepository.findAllByOrderByRegionNameAsc();
		for (Region region : itRegionList) {
			listRegion.add((RegionDto) entityConverter.convertEntityToDto(region, new RegionDto()));
		}
		return listRegion;
	}

	/**
	 * Method to query all regions.
	 * 
	 * @return
	 */
	public List<RegionSelectionDto> getRegionsSelection() {
		ArrayList<RegionSelectionDto> listRegion = new ArrayList<>();
		Iterable<Region> itRegionList = regionRepository.findAllByOrderByRegionNameAsc();
		for (Region region : itRegionList) {
			listRegion.add((RegionSelectionDto) entityConverter.convertEntityToDto(region, new RegionSelectionDto()));
		}
		return listRegion;
	}

	/**
	 * Method to select a region by its id.
	 * 
	 * @param userid
	 * @return
	 */
	public RegionDto getRegion(Long regionid) {
		Region region = regionRepository.findOne(regionid);
		return (RegionDto) entityConverter.convertEntityToDto(region, new RegionDto());
	}

	/**
	 * Method to persist a region.
	 * 
	 * @param regionDto {@link RegionDto}
	 * @return
	 * @throws SpException
	 */
	public RegionDto saveRegion(RegionDto regionDto) throws SpException {
		Region region = new Region();
		List<Location> listLocations = new ArrayList<>();

		try {
			if (regionDto.getId() != null && regionRepository.exists(regionDto.getId())) {
				region = regionRepository.findOne(regionDto.getId());
				listLocations = region.getLsLocations();

			}

			region = (Region) entityConverter.convertDtoToEntity(regionDto, region);
			region.setLsLocations(listLocations);

			region = regionRepository.save(region);

			return (RegionDto) entityConverter.convertEntityToDto(region, new RegionDto());

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, new Gson().toJson(regionDto));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	@SuppressWarnings("unchecked")
	public List<LocationSelectionDto> saveLocationsForRegion(Long regionId, List<LocationSelectionDto> lsLocationDtos)
			throws SpException {
		Long locationId = null;
		try {

			Region region = regionRepository.findOne(regionId);

			for (LocationSelectionDto dto : lsLocationDtos) {
				locationId = dto.getId();
				if (regionRepository.exists(regionId) && locationRepository.exists(locationId)) {
					Location location = locationRepository.findOne(locationId);
					region.getLsLocations().add(location);
					regionRepository.save(region);

					location.getLsRegions().add(region);
					locationRepository.save(location);
				} else {
					throw new IllegalArgumentException();
				}
			}

			lsLocationDtos = new ArrayList<>();
			lsLocationDtos = entityConverter.convertEntityToDtoList(region.getLsLocations(), lsLocationDtos,
					LocationSelectionDto.class);

			return lsLocationDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, regionId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	@SuppressWarnings("unchecked")
	public List<LocationSelectionDto> deleteLocationsForRegion(Long regionId, List<LocationSelectionDto> lsLocationDtos)
			throws SpException {

		Long locationId = null;

		try {

			Region region = regionRepository.findOne(regionId);

			for (LocationSelectionDto dto : lsLocationDtos) {
				locationId = dto.getId();

				if (regionRepository.exists(regionId) && locationRepository.exists(locationId)) {

					Location location = locationRepository.findOne(locationId);

					if (region.getLsLocations().contains(location)) {
						region.getLsLocations().remove(location);
						regionRepository.save(region);
					}

					if (location.getLsRegions().contains(region)) {
						location.getLsRegions().remove(region);
						locationRepository.save(location);
					}
				} else {
					throw new IllegalArgumentException();
				}
			}

			lsLocationDtos = new ArrayList<>();
			lsLocationDtos = entityConverter.convertEntityToDtoList(region.getLsLocations(), lsLocationDtos,
					LocationSelectionDto.class);

			return lsLocationDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, regionId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}
}
