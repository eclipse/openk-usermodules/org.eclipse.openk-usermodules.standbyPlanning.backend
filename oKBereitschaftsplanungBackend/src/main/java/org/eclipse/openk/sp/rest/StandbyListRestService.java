/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.controller.StandbyListController;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyListHasStandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyListSelectionDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("standbylist")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StandbyListRestService extends BaseResource {

	@Autowired
	private StandbyListController sLCo;

	public StandbyListRestService() {
		super(Logger.getLogger(StandbyListRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getStandbyLists(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<StandbyListDto>> invokable = modusr -> sLCo.getStandbyLists();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getStandbyListsSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<StandbyListSelectionDto>> invokable = modusr -> sLCo.getStandbyListsSelection();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/dropdownlist")
	public Response getStandbyListsDropdown(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<StandbyListSelectionDto>> invokable = modusr -> sLCo.getStandbyListsDropdown();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/{id}")
	public Response getStandbyList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<StandbyListDto> invokable = modusr -> sLCo.getStandbyList(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/save")
	public Response saveStandbyList(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid StandbyListDto standbyListDto) {

		ModifyingInvokable<AbstractDto> invokable = modusr -> sLCo.saveStandbyList(standbyListDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/standbygroup/save/list")
	public Response addStandbyGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto) {

		ModifyingInvokable<List<StandbyListHasStandbyGroupDto>> invokable = modusr -> sLCo
				.saveStandbyGroupsForStandbyList(id, lsStandbyListHasStandbyGroupDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/standbygroup/delete/list")
	public Response deleteStandbyGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<StandbyListHasStandbyGroupDto> lsStandbyGroupDto) {

		ModifyingInvokable<List<StandbyListHasStandbyGroupDto>> invokable = modusr -> sLCo
				.deleteStandbyGroupsForStandbyList(id, lsStandbyGroupDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
