/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StandbyScheduleBodyRepository extends JpaRepository<StandbyScheduleBody, Long> {

	@Query(value = "SELECT sbsb.id FROM StandbyScheduleBody sbsb " + "WHERE (sbsb.standbyGroup.id = :groupId "
			+ "AND sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo AND sbsb.status.id = :status) "
			+ "ORDER BY sbsb.validFrom ASC")
	List<Long> findIdsByGroupAndDateAndStatus(@Param("groupId") Long groupId, @Param("validFrom") Date validFrom,
			@Param("validTo") Date validTo, @Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE (sbsb.standbyGroup.id = :groupId "
			+ "AND sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo AND sbsb.status.id = :status) "
			+ "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByGroupAndDateAndStatus(@Param("groupId") Long groupId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo, @Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb "
			+ "WHERE (sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo AND sbsb.status.id = :status) ")
	List<StandbyScheduleBody> findRowByDateAndStatus(@Param("validFrom") Date validFrom, @Param("validTo") Date validTo,
			@Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb "
			+ "WHERE (sbsb.user.id = :userId AND sbsb.standbyGroup.id = :groupId "
			+ "AND sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo AND sbsb.status.id = :status) "
			+ "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByUserAndGroupAndDateAndStatus(@Param("userId") Long userId,
			@Param("groupId") Long groupId, @Param("validFrom") Date validFrom, @Param("validTo") Date validTo,
			@Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE (("
			+ "(sbsb.validTo >= :nowTime AND sbsb.validFrom <= :nowTime ) "
			+ "OR (sbsb.validFrom >= :nowTime AND sbsb.validFrom < :dayEnd )) "
			+ "AND sbsb.status.id = :status AND sbsb.user.id = :userId ) " + "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByUserAndDateAndStatus(@Param("userId") Long userId, @Param("nowTime") Date nowTime,
			@Param("dayEnd") Date dayEnd, @Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE (sbsb.standbyGroup.id in :groupIds "
			+ "AND sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo AND sbsb.status.id = :status) "
			+ "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByGroupsAndDateAndStatus(@Param("groupIds") Long[] groupIds,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo, @Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE (("
			+ "(sbsb.validTo >= :nowTime AND sbsb.validFrom <= :nowTime ) "
			+ "OR (sbsb.validFrom >= :nowTime AND sbsb.validFrom < :dayEnd )) "
			+ "AND sbsb.status.id = :status AND sbsb.standbyGroup.id in :groupIds) " + "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findData(@Param("groupIds") Long[] groupIds, @Param("nowTime") Date nowTime,
			@Param("dayEnd") Date dayEnd, @Param("status") Long status);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE (("
			+ "(sbsb.validTo >= :nowTime AND sbsb.validFrom <= :nowTime ) "
			+ "OR (sbsb.validFrom >= :nowTime AND sbsb.validFrom < :dayEnd )) " + "AND sbsb.status.id = :status) "
			+ "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByDateAndStatus(@Param("nowTime") Date nowTime, @Param("dayEnd") Date dayEnd,
			@Param("status") Long status);

	/**
	 * 1= sbsb.validFrom 2= sbsb.validTo </br>
	 * </br>
	 * #######1____2######## </br>
	 * ####[----------]##### sbsb.validFrom >= :validFrom AND sbsb.validTo <=
	 * :validTo </br>
	 * ####[------]######### sbsb.validFrom >= :validFrom AND sbsb.validFrom <=
	 * :validTo </br>
	 * ########[--------]### sbsb.validTo >= :validFrom AND sbsb.validTo <= :validTo
	 * </br>
	 * ########[-]########## sbsb.validFrom <= :validFrom AND sbsb.validTo >=
	 * :validTo
	 */
	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE ((sbsb.user.id = :userId) "
			+ "AND ((sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo ) "
			+ "OR (sbsb.validFrom >= :validFrom AND sbsb.validFrom <= :validTo ) "
			+ "OR (sbsb.validTo >= :validFrom AND sbsb.validTo <= :validTo ) "
			+ "OR (sbsb.validFrom <= :validFrom AND sbsb.validTo >= :validTo )))" + "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByUserHittingDateInterval(@Param("userId") Long userId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	@Query(value = "SELECT sbsb FROM StandbyScheduleBody sbsb " + "WHERE ((sbsb.standbyGroup.id = :groupId) "
			+ "AND sbsb.status.id = :statusId " + "AND ((sbsb.validFrom >= :validFrom AND sbsb.validTo <= :validTo ) "
			+ "OR (sbsb.validFrom >= :validFrom AND sbsb.validFrom <= :validTo ) "
			+ "OR (sbsb.validTo >= :validFrom AND sbsb.validTo <= :validTo ) "
			+ "OR (sbsb.validFrom <= :validFrom AND sbsb.validTo >= :validTo )))" + "ORDER BY sbsb.validFrom ASC")
	List<StandbyScheduleBody> findByGroupAndStatusHittingDateInterval(@Param("groupId") Long groupId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo, @Param("statusId") Long statusId);
}
