/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;

public class ValidationHelper {
	protected static final Logger LOGGER = Logger.getLogger(ValidationHelper.class);

	/** default private constructor. */
	private ValidationHelper() {

	}

	/**
	 * Method to remove all null values from list and check if it still has the same
	 * size.
	 * 
	 * @param lsValues
	 * @return
	 * @throws SpException
	 */
	public static Boolean isNoNullValueInList(List<Object> lsValues) throws SpException {
		int startSize = lsValues.size();
		lsValues.removeAll(Collections.singleton(null));
		int endSize = lsValues.size();
		if (startSize != endSize) {
			SpErrorEntry ee = SpExceptionEnum.MISSING_PARAMETERS_EXCEPTION.getEntry();
			SpException spEE = new SpException(ee);
			LOGGER.error(spEE, spEE);
			throw spEE;
		}
		return true;
	}
}
