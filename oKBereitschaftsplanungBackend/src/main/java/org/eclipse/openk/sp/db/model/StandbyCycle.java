/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The persistent class for the "STANDBY_CYCLE" database table.
 */
@Entity
@Table(name = "STANDBY_CYCLE")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class StandbyCycle extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_CYCLE_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_CYCLE_ID_SEQ", sequenceName = "STANDBY_CYCLE_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "cycle_name", length = 256, nullable = false)
	private String cycleName;

	@Column(name = "hand_over_day", length = 256, nullable = false)
	private String handOverDay;

	@Column(name = "start_time")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private Date startTime;

	@Column(name = "end_time")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private Date endTime;

	@Column(name = "hand_over_on_holiday")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private Date handOverOnHoliday;

	@Column(name = "is_special_standby", nullable = false)
	@Convert("booleanConverter")
	private Boolean isSpecialStandby;

	@Column(name = "weekday_start", length = 256, nullable = false)
	private String weekdayStart;

	@Column(name = "weekend_start_time")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private Date weekendStartTime;

	@Column(name = "weekday_end", length = 256, nullable = false)
	private String weekdayEnd;

	@Column(name = "weekend_end_time")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private Date weekendEndTime;

	public StandbyCycle() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cycleName
	 */
	public String getCycleName() {
		return cycleName;
	}

	/**
	 * @param cycleName
	 *            the cycleName to set
	 */
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	/**
	 * @return the handOverDay
	 */
	public String getHandOverDay() {
		return handOverDay;
	}

	/**
	 * @param handOverDay
	 *            the handOverDay to set
	 */
	public void setHandOverDay(String handOverDay) {
		this.handOverDay = handOverDay;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the handOverOnHoliday
	 */
	public Date getHandOverOnHoliday() {
		return handOverOnHoliday;
	}

	/**
	 * @param handOverOnHoliday
	 *            the handOverOnHoliday to set
	 */
	public void setHandOverOnHoliday(Date handOverOnHoliday) {
		this.handOverOnHoliday = handOverOnHoliday;
	}

	/**
	 * @return the isSpecialStandby
	 */
	public Boolean getIsSpecialStandby() {
		return isSpecialStandby;
	}

	/**
	 * @param isSpecialStandby
	 *            the isSpecialStandby to set
	 */
	public void setIsSpecialStandby(Boolean isSpecialStandby) {
		this.isSpecialStandby = isSpecialStandby;
	}

	/**
	 * @return the weekdayStart
	 */
	public String getWeekdayStart() {
		return weekdayStart;
	}

	/**
	 * @param weekdayStart
	 *            the weekdayStart to set
	 */
	public void setWeekdayStart(String weekdayStart) {
		this.weekdayStart = weekdayStart;
	}

	/**
	 * @return the weekendStartTime
	 */
	public Date getWeekendStartTime() {
		return weekendStartTime;
	}

	/**
	 * @param weekendStartTime
	 *            the weekendStartTime to set
	 */
	public void setWeekendStartTime(Date weekendStartTime) {
		this.weekendStartTime = weekendStartTime;
	}

	/**
	 * @return the weekdayEnd
	 */
	public String getWeekdayEnd() {
		return weekdayEnd;
	}

	/**
	 * @param weekdayEnd
	 *            the weekdayEnd to set
	 */
	public void setWeekdayEnd(String weekdayEnd) {
		this.weekdayEnd = weekdayEnd;
	}

	/**
	 * @return the weekendEndTime
	 */
	public Date getWeekendEndTime() {
		return weekendEndTime;
	}

	/**
	 * @param weekendEndTime
	 *            the weekendEndTime to set
	 */
	public void setWeekendEndTime(Date weekendEndTime) {
		this.weekendEndTime = weekendEndTime;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}
}
