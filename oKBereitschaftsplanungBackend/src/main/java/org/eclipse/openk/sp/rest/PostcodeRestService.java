/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.PostcodeController;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("postcode")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostcodeRestService extends BaseResource {

	@Autowired
	private PostcodeController postcodeController;

	public PostcodeRestService() {
		super(Logger.getLogger(PostcodeRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getPostcodes(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<PostcodeDto>> invokable = modusr -> postcodeController.getPostcodes();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getPostcodesSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		return getPostcodes(jwt);
	}

	@GET
	@Path("/{id}")
	public Response getPostcode(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<PostcodeDto> invokable = modusr -> postcodeController.getPostcode(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/save")
	public Response savePostcode(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid PostcodeDto postcodeDto) {

		ModifyingInvokable<PostcodeDto> invokable = modusr -> postcodeController.savePostcode(postcodeDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
