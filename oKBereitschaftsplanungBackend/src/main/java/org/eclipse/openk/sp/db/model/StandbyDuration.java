/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBY_DURATION" database table.
 */
@Entity
@Table(name = "STANDBY_DURATION")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class StandbyDuration extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_DURATION_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_DURATION_ID_SEQ", sequenceName = "STANDBY_DURATION_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIME)
	private Date validFrom;

	@Column(name = "valid_day_from", nullable = false)
	private Integer validDayFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIME)
	private Date validTo;

	@Column(name = "valid_day_to", nullable = false)
	private Integer validDayTo;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "next_user_in_next_duration", nullable = false)
	@Convert("booleanConverter")
	private Boolean nextUserInNextDuration;

	@ManyToOne()
	@JoinColumn(name = "standby_group_id", nullable = false)
	private StandbyGroup standbyGroup;

	public StandbyDuration() {
		/** default constructor. */
	}

	public StandbyDuration(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validDayFrom
	 */
	public Integer getValidDayFrom() {
		return validDayFrom;
	}

	/**
	 * @param validDayFrom
	 *            the validDayFrom to set
	 */
	public void setValidDayFrom(Integer validDayFrom) {
		this.validDayFrom = validDayFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the validDayTo
	 */
	public Integer getValidDayTo() {
		return validDayTo;
	}

	/**
	 * @param validDayTo
	 *            the validDayTo to set
	 */
	public void setValidDayTo(Integer validDayTo) {
		this.validDayTo = validDayTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the standbyGroup
	 */
	public StandbyGroup getStandbyGroup() {
		return standbyGroup;
	}

	/**
	 * @param standbyGroup
	 *            the standbyGroup to set
	 */
	public void setStandbyGroup(StandbyGroup standbyGroup) {
		this.standbyGroup = standbyGroup;
	}

	/**
	 * @return the nextUserInNextDuration
	 */
	public Boolean getNextUserInNextDuration() {
		return nextUserInNextDuration;
	}

	/**
	 * @param nextUserInNextDuration
	 *            the nextUserInNextDuration to set
	 */
	public void setNextUserInNextDuration(Boolean nextUserInNextDuration) {
		this.nextUserInNextDuration = nextUserInNextDuration;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	@Override
	public StandbyDuration copy() {
		StandbyDuration newSBD = new StandbyDuration();
		newSBD.setId(null);
		newSBD.setModificationDate(new Date());
		newSBD.setNextUserInNextDuration(this.getNextUserInNextDuration());
		newSBD.setValidDayFrom(this.getValidDayFrom());
		newSBD.setValidDayTo(this.getValidDayTo());
		newSBD.setValidFrom(this.getValidFrom());
		newSBD.setValidTo(this.getValidTo());

		return newSBD;
	}
}
