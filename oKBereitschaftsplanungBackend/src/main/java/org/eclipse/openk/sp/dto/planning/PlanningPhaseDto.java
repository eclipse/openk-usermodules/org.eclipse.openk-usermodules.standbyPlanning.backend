/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "PlanningPhaseDto")
@JsonInclude(Include.NON_NULL)
public class PlanningPhaseDto extends AbstractDto {
	/** */
	private static final long serialVersionUID = 1L;

	private Date startDate;
	private Date endDate;
	private List<UserInStandbyGroup> lsUsers;

	public PlanningPhaseDto() {
	}

	public PlanningPhaseDto(Date startDate, Date endDate, List<UserInStandbyGroup> lsUsers) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.lsUsers = lsUsers;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the lsUsers
	 */
	public List<UserInStandbyGroup> getLsUsers() {
		return lsUsers;
	}

	/**
	 * @param lsUsers the lsUsers to set
	 */
	public void setLsUsers(List<UserInStandbyGroup> lsUsers) {
		this.lsUsers = lsUsers;
	}
}
