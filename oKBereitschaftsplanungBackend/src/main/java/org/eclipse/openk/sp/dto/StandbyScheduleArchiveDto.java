/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsArchiveDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleArchiveDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * filter criteria
	 */
	private StandbyScheduleFilterDto filter;

	/**
	 * first row
	 */
	private PlanHeaderDto planHeader;

	/**
	 * all other rows
	 */
	private List<PlanRowsArchiveDto> listPlanRows = new ArrayList<>();

	/**
	 * content
	 */

	public StandbyScheduleArchiveDto() {
		/** default constructor. */
	}

	/**
	 * @return the filter
	 */
	public StandbyScheduleFilterDto getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(StandbyScheduleFilterDto filter) {
		this.filter = filter;
	}

	/**
	 * @return the planHeader
	 */
	public PlanHeaderDto getPlanHeader() {
		return planHeader;
	}

	/**
	 * @param planHeader the planHeader to set
	 */
	public void setPlanHeader(PlanHeaderDto planHeader) {
		this.planHeader = planHeader;
	}

	/**
	 * @return the listPlanRows
	 */
	public List<PlanRowsArchiveDto> getListPlanRows() {
		return listPlanRows;
	}

	/**
	 * @param listPlanRows the listPlanRows to set
	 */
	public void setListPlanRows(List<PlanRowsArchiveDto> listPlanRows) {
		this.listPlanRows = listPlanRows;
	}

}
