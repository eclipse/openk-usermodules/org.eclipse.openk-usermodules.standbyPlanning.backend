/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.dto.UserFunctionDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle USERFUNCTION operations. */
public class UserFunctionController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(UserFunctionController.class);

	@Autowired
	private UserFunctionRepository userFunctionRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * Method to query all userFunctions.
	 * 
	 * @return
	 */
	public List<UserFunctionDto> getUserFunctions() {
		ArrayList<UserFunctionDto> listUserFunction = new ArrayList<>();
		Iterable<UserFunction> itUserFunctionList = userFunctionRepository.findAllByOrderByFunctionNameAsc();
		for (UserFunction userFunction : itUserFunctionList) {
			listUserFunction
					.add((UserFunctionDto) entityConverter.convertEntityToDto(userFunction, new UserFunctionDto()));
		}
		return listUserFunction;
	}

	/**
	 * Method to query all userFunctions.
	 * 
	 * @return
	 */
	public List<UserFunctionSelectionDto> getUserFunctionsSelection() {
		ArrayList<UserFunctionSelectionDto> listUserFunction = new ArrayList<>();
		Iterable<UserFunction> itUserFunctionList = userFunctionRepository.findAllByOrderByFunctionNameAsc();
		for (UserFunction userFunction : itUserFunctionList) {
			listUserFunction.add((UserFunctionSelectionDto) entityConverter.convertEntityToDto(userFunction,
					new UserFunctionSelectionDto()));
		}
		return listUserFunction;
	}

	/**
	 * Method to select a userFunction by its id.
	 * 
	 * @param userid
	 * @return
	 */
	public UserFunctionDto getUserFunction(Long userFunctionid) {
		UserFunction userFunction = userFunctionRepository.findOne(userFunctionid);
		return (UserFunctionDto) entityConverter.convertEntityToDto(userFunction, new UserFunctionDto());
	}

	/**
	 * Method to persist a userFunction.
	 * 
	 * @param userid
	 * @return
	 * @throws HttpStatusException
	 */
	public UserFunctionDto saveUserFunction(UserFunctionDto userFunctionDto) {
		UserFunction userFunction = new UserFunction();
		UserFunction tempUserfunction = null;

		if (userFunctionDto.getFunctionId() != null) {
			userFunction = userFunctionRepository.findOne(userFunctionDto.getFunctionId());
			tempUserfunction = userFunction;

		}

		userFunction = (UserFunction) entityConverter.convertDtoToEntity(userFunctionDto, userFunction);

		if (tempUserfunction != null) {
			userFunction.setLsUserHasUserFunction(tempUserfunction.getLsUserHasUserFunction());
		}

		userFunction = userFunctionRepository.save(userFunction);

		return (UserFunctionDto) entityConverter.convertEntityToDto(userFunction, new UserFunctionDto());
	}
}
