/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;

public class PlanRowsArchiveDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String label;
	private String style;

	private List<List<StandbyScheduleBodySelectionDto>> listGroupBodies = new ArrayList<>();

	public PlanRowsArchiveDto() {
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param lable the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the listGroupBodys
	 */
	public List<List<StandbyScheduleBodySelectionDto>> getListGroupBodys() {
		return listGroupBodies;
	}

	/**
	 * @param listGroupBodys the listGroupBodys to set
	 */
	public void setListGroupBodys(List<List<StandbyScheduleBodySelectionDto>> listGroupBodys) {
		this.listGroupBodies = listGroupBodys;
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

}
