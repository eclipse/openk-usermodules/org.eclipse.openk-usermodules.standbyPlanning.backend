/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListHasStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyListHasStandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyListSelectionDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle LOCATION operations. */
public class StandbyListController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(StandbyListController.class);

	@Autowired
	private StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	private StandbyListHasStandbyGroupRepository standbyListHasStandbyGroupRepository;

	/**
	 * Method to query all standbyLists.
	 * 
	 * @return
	 */
	public List<StandbyListDto> getStandbyLists() {

		List<StandbyList> lsStandbyListList = standbyListRepository.findAllByOrderByTitleAsc();
		List<StandbyListDto> dtoList = new ArrayList<>();

		dtoList = entityConverter.convertEntityToDtoList(lsStandbyListList, dtoList, StandbyListDto.class);

		return dtoList;
	}

	/**
	 * 
	 * @return
	 */
	public List<StandbyListSelectionDto> getStandbyListsSelection() {

		List<StandbyList> lsStandbyListList = standbyListRepository.findAllByOrderByTitleAsc();
		List<StandbyListSelectionDto> dtoList = new ArrayList<>();

		dtoList = entityConverter.convertEntityToDtoList(lsStandbyListList, dtoList, StandbyListSelectionDto.class);

		return dtoList;
	}

	/**
	 * 
	 * @return
	 */
	public List<StandbyListSelectionDto> getStandbyListsDropdown() {
		List<StandbyListSelectionDto> listDto = new ArrayList<>();

		List<Object[]> lsStandbyListList = standbyListRepository.findAllLists();

		for (Object[] objects : lsStandbyListList) {
			StandbyListSelectionDto dto = new StandbyListSelectionDto();
			dto.setTitle((String) objects[0]);
			dto.setId(((Number) objects[1]).longValue());

			listDto.add(dto);
		}

		Collections.sort(listDto, (o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));

		return listDto;
	}

	/**
	 * Method to select a standbyList by its id.
	 * 
	 * @param standbyListid
	 * @return
	 */
	public StandbyListDto getStandbyList(Long standbyListid) {
		StandbyList standbyList = standbyListRepository.findOne(standbyListid);
		StandbyListDto dto = (StandbyListDto) entityConverter.convertEntityToDto(standbyList, new StandbyListDto());

		Collections.sort(dto.getLsStandbyListHasStandbyGroup(),
				(o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));

		return dto;
	}

	/**
	 * Method to persist a standbyList.
	 * 
	 * @param standbyListDto
	 * @return
	 * @throws HttpStatusException
	 */
	public StandbyListDto saveStandbyList(StandbyListDto standbyListDto) throws SpException {
		StandbyList standbyList = new StandbyList();
		List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = new ArrayList<>();
		try {
			if (standbyListDto.getId() != null && standbyListRepository.exists(standbyListDto.getId())) {
				standbyList = standbyListRepository.findOne(standbyListDto.getId());
				lsStandbyListHasStandbyGroup = standbyList.getLsStandbyListHasStandbyGroup();
			}

			// StandbyList
			standbyList = (StandbyList) entityConverter.convertDtoToEntity(standbyListDto, standbyList);
			standbyList.setModificationDate(new Date());
			standbyList.setLsStandbyListHasStandbyGroup(lsStandbyListHasStandbyGroup);

			standbyList = standbyListRepository.save(standbyList);
			return (StandbyListDto) entityConverter.convertEntityToDto(standbyList, new StandbyListDto());

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_LIST_WITH_ID, new Gson().toJson(standbyListDto));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	protected StandbyList getSavedStandbyList(Long id) {
		return getSavedStandbyList(new StandbyList(id));
	}

	private StandbyList getSavedStandbyList(StandbyList standbyList) {
		StandbyList savedStandbyList = null;
		if (standbyList.getId() != null) {
			savedStandbyList = standbyListRepository.findOne(standbyList.getId());
		}
		return savedStandbyList;
	}

	/**
	 * 
	 * @param standbyListId
	 * @param lsStandbyGroupDto
	 * @return
	 * @throws SpException
	 */
	public List<StandbyListHasStandbyGroupDto> saveStandbyGroupsForStandbyList(Long standbyListId,
			List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto) throws SpException {

		StandbyList standbyList = standbyListRepository.findOne(standbyListId);

		for (StandbyListHasStandbyGroupDto dto : lsStandbyListHasStandbyGroupDto) {
			StandbyListHasStandbyGroup standbyListHasStandbyGroup = (StandbyListHasStandbyGroup) entityConverter
					.convertDtoToEntity(dto, new StandbyListHasStandbyGroup());

			if (dto.getId() != null && standbyListHasStandbyGroupRepository.exists(dto.getId())) {
				// update existing
				StandbyListHasStandbyGroup savedUisg = standbyListHasStandbyGroupRepository.findOne(dto.getId());

				if (savedUisg.getPosition().intValue() != dto.getPosition().intValue()) {
					this.pushPositionHigher(savedUisg.getStandbyList().getId(), dto.getPosition());
				}
				savedUisg.setPosition(dto.getPosition());
				standbyListHasStandbyGroupRepository.save(savedUisg);

			} else if (standbyListRepository.exists(standbyListId)
					&& standbyGroupRepository.exists(dto.getStandbyGroupId())) {
				// new
				StandbyGroup standbyGroup = standbyGroupRepository.findOne(dto.getStandbyGroupId());
				standbyListHasStandbyGroup.setId(null);
				standbyListHasStandbyGroup.setStandbyGroup(standbyGroup);
				standbyListHasStandbyGroup.setStandbyList(standbyList);

				Integer nextPosition = getNextPosition(standbyList);
				standbyListHasStandbyGroup.setPosition(nextPosition);
				standbyList.getLsStandbyListHasStandbyGroup().add(standbyListHasStandbyGroup);

				standbyListHasStandbyGroupRepository.save(standbyListHasStandbyGroup);
				standbyListRepository.save(standbyList);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBL_OR_SBG, standbyListId, dto.getStandbyGroupId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}

		}

		ArrayList<StandbyListHasStandbyGroupDto> lsDtos = new ArrayList<>();
		lsDtos = (ArrayList<StandbyListHasStandbyGroupDto>) entityConverter.convertEntityToDtoList(
				standbyList.getLsStandbyListHasStandbyGroup(), lsDtos, StandbyListHasStandbyGroupDto.class);

		Collections.sort(lsDtos, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));

		return lsDtos;
	}

	/**
	 * 
	 * @param standbyListId
	 * @param lsStandbyGroupDto
	 * @return
	 * @throws SpException
	 */
	public List<StandbyListHasStandbyGroupDto> deleteStandbyGroupsForStandbyList(Long standbyListId,
			List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto) throws SpException {

		StandbyList standbyList = standbyListRepository.findOne(standbyListId);

		for (StandbyListHasStandbyGroupDto dto : lsStandbyListHasStandbyGroupDto) {
			if (standbyListRepository.exists(standbyListId)
					&& standbyListHasStandbyGroupRepository.exists(dto.getId())) {

				StandbyListHasStandbyGroup standbyListHasStandbyGroup = standbyListHasStandbyGroupRepository
						.findOne(dto.getId());
				standbyList.getLsStandbyListHasStandbyGroup().remove(standbyListHasStandbyGroup);

				standbyListRepository.save(standbyList);
				standbyListHasStandbyGroupRepository.delete(standbyListHasStandbyGroup);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBL_OR_SBG, standbyListId, dto.getId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		ArrayList<StandbyListHasStandbyGroupDto> lsDtos = new ArrayList<>();
		lsDtos = (ArrayList<StandbyListHasStandbyGroupDto>) entityConverter.convertEntityToDtoList(
				standbyList.getLsStandbyListHasStandbyGroup(), lsDtos, StandbyListHasStandbyGroupDto.class);

		Collections.sort(lsDtos, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));

		return lsDtos;
	}

	/**
	 * Method to select all entities which position is equal or higher then the
	 * given to push all of them to the next position.
	 * 
	 * @param groupId
	 * @param pos
	 */
	public void pushPositionHigher(Long listId, Integer pos) {
		List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = standbyListHasStandbyGroupRepository
				.findListMemberWithMinPosition(listId, pos);
		for (StandbyListHasStandbyGroup uisg : lsStandbyListHasStandbyGroup) {
			uisg.setPosition(uisg.getPosition() + 1);
			standbyListHasStandbyGroupRepository.save(uisg);
		}
	}

	private Integer getNextPosition(StandbyList standbyList) {
		List<StandbyListHasStandbyGroup> list = standbyList.getLsStandbyListHasStandbyGroup();
		int maxPos = 0;

		for (StandbyListHasStandbyGroup standbyListHasStandbyGroup : list) {
			if (standbyListHasStandbyGroup.getPosition() > maxPos) {
				maxPos = standbyListHasStandbyGroup.getPosition();
			}
		}

		return ++maxPos;
	}

}
