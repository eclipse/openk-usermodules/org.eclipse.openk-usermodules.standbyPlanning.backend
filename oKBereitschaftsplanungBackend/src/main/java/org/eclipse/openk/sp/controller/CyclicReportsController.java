/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.CyclicReportGenerationConfigRepository;
import org.eclipse.openk.sp.db.model.ReportGenerationConfig;
import org.eclipse.openk.sp.dto.ReportGenerationConfigDto;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.ArchiveHelper;
import org.eclipse.openk.sp.util.MailHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@EnableScheduling
@Transactional(rollbackFor = Exception.class)
/** Class to handle automatic report generation and configuration. */
public class CyclicReportsController extends AbstractController {

	protected static final Logger LOGGER = Logger.getLogger(CyclicReportsController.class);

	@Autowired
	private CyclicReportGenerationConfigRepository automaticReportGenerationConfigRepository;

	@Autowired
	private MailHelper mailHelper;

	@Autowired
	private ArchiveHelper archiveHelper;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	private ReportController reportController;

	@Value("${report.cyclic.datePattern:yyyy-MM-dd}")
	private String datePattern;

	@Value("${report.cyclic.timePattern:HH-mm}")
	private String timePattern;

	@Value("${report.cyclic.weekPattern:'KW'ww}")
	private String weekPattern;

	@Value("${report.cyclic.archivePath:/opt/openk/reports/}")
	private String archivePath;

	@Value("${report.cyclic.slotDeltaMin:5}")
	private int slotDeltaMin;

	@Value("${report.cyclic.zoneName:Europe/Berlin}")
	private String zoneName;

	@Value("${report.cyclic.locale:de}")
	private String locale;

	@Value("${report.cyclic.sendReportMail:true}")
	private boolean sendReportMail;

	@Value("${report.cyclic.archiveReport:true}")
	private boolean archiveReport;

	private DateTimeFormatter datePatternFormatter;

	private DateTimeFormatter timePatternFormatter;

	private DateTimeFormatter weekPatternFormatter;

	private ZoneId zoneId;
	
	private ZoneId zoneIdUTC;


	@PostConstruct
	private void init() {
		Locale l = new Locale(locale);
		datePatternFormatter = DateTimeFormatter.ofPattern(datePattern).withLocale(l);
		timePatternFormatter = DateTimeFormatter.ofPattern(timePattern).withLocale(l);
		weekPatternFormatter = DateTimeFormatter.ofPattern(weekPattern).withLocale(l);
		zoneId = ZoneId.of(zoneName);
		zoneIdUTC = ZoneId.of("UTC");

	}

	public List<ReportGenerationConfigDto> getAllReportGenerationConfig() throws SpException {
		List<ReportGenerationConfigDto> configs = new ArrayList<>();
		for (ReportGenerationConfig cfg : automaticReportGenerationConfigRepository.findAll()) {
			configs.add((ReportGenerationConfigDto) entityConverter.convertEntityToDto(cfg,
					new ReportGenerationConfigDto()));
		}
		return configs;
	}

	public Response deleteReportGenerationConfig(Long configId) throws SpException {
		Optional<ReportGenerationConfig> oConfig = automaticReportGenerationConfigRepository.findById(configId);
		if (oConfig.isPresent()) {
			automaticReportGenerationConfigRepository.delete(configId);
			return Response.noContent().build();
		}
		SpErrorEntry ee = SpExceptionEnum.HTTP_NOT_FOUND_EXCEPTION.getEntry();
		String msgParam = "Not Found. Report generation config with id: " + configId;
		SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
		LOGGER.error(spE, spE);
		throw spE;
	}

	public ReportGenerationConfigDto editReportGenerationConfig(Long configId,
			ReportGenerationConfigDto reportGenerationConfig) throws SpException {
		Optional<ReportGenerationConfig> oConfig = automaticReportGenerationConfigRepository.findById(configId);
		if (oConfig.isPresent()) {
			ReportGenerationConfig cfg = (ReportGenerationConfig) entityConverter
					.convertDtoToEntity(reportGenerationConfig, new ReportGenerationConfig());
			cfg.setId(configId);
			automaticReportGenerationConfigRepository.save(cfg);
			reportGenerationConfig.setId(configId);
			return reportGenerationConfig;
		}
		SpErrorEntry ee = SpExceptionEnum.HTTP_NOT_FOUND_EXCEPTION.getEntry();
		String msgParam = "Not Found. Report generation config with id: " + configId;
		SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
		LOGGER.error(spE, spE);
		throw spE;
	}

	public ReportGenerationConfigDto addReportGenerationConfig(ReportGenerationConfigDto reportGenerationConfig)
			throws SpException {
		ReportGenerationConfig newCfg = (ReportGenerationConfig) entityConverter
				.convertDtoToEntity(reportGenerationConfig, new ReportGenerationConfig());
		newCfg.setId(null);
		ReportGenerationConfig savedCfg = automaticReportGenerationConfigRepository.save(newCfg);
		return (ReportGenerationConfigDto) entityConverter.convertEntityToDto(savedCfg,
				new ReportGenerationConfigDto());
	}

	protected File generateReport(ReportGenerationConfig reportCfg, LocalDateTime triggerDate) throws SpException {
		ReportDto reportDto = new ReportDto();
		reportDto.setReportName(reportCfg.getReportName());
		reportDto.setPrintFormat(reportCfg.getPrintFormat().toLowerCase());
		reportDto.setStatusId(reportCfg.getStatusId());

		// calculate from and to date
		LocalDateTime fromLocalDateTime = triggerDate.plusDays(reportCfg.getValidFromDayOffset())
				.withHour(reportCfg.getValidFromHour()).withMinute(reportCfg.getValidFromMinute());
		LocalDateTime toLocalDateTime = triggerDate.plusDays(reportCfg.getValidToDayOffset())
				.withHour(reportCfg.getValidToHour()).withMinute(reportCfg.getValidToMinute());
		Date fromDate = Date.from(fromLocalDateTime.atZone(zoneIdUTC).toInstant());
		Date toDate = Date.from(toLocalDateTime.atZone(zoneIdUTC).toInstant());

		reportDto.setValidFromDate(fromDate);
		reportDto.setValidToDate(toDate);
		reportDto.setStandByListId(reportCfg.getStandByListId());
		return reportController.generateReport(reportDto);
	}


	protected void generateAndDistributeReports(LocalDateTime triggerTimeGreaterEqual,
			LocalDateTime triggerTimeLowerEqual) {
		List<ReportGenerationConfig> currentConfigs = getReportGenerationConfigs(triggerTimeGreaterEqual,
				triggerTimeLowerEqual);
		for (ReportGenerationConfig cfg : currentConfigs) {
			LOGGER.info("Matching report generationg config: " + cfg.getName());
			try {
				LocalDateTime triggerDateTime = triggerTimeGreaterEqual.withHour(cfg.getTriggerHour())
						.withMinute(cfg.getTriggerMinute());
				File reportFile = generateReport(cfg, triggerDateTime);
				String fileName = fileNameOfCfg(triggerDateTime, cfg);
				storeReport(reportFile, fileName);
				sendReport(reportFile, fileName, cfg, triggerDateTime);
			} catch (Exception e) {
				LOGGER.warn("Could not autogenerate and send report configuration with name: " + cfg.getName(), e);
			}
		}
	}

	protected void sendReport(File reportFile, String fileName, ReportGenerationConfig cfg, LocalDateTime triggerDateTime) {
		if (!sendReportMail || cfg.getTo() == null || cfg.getTo().isEmpty()) {
			return;
		}
		try {
			MimeMessage mail = mailHelper.newMultipartMail();
			String subject = placeholderReplacement(triggerDateTime, cfg.getSubject());
			MailHelper.setSubject(mail, subject);
			MailHelper.setToRecipients(mail, cfg.getTo());
			String text = placeholderReplacement(triggerDateTime, cfg.getEmailText());
			MailHelper.addText(mail, text);
			MailHelper.addAttachment(mail, reportFile, fileName);
			mailHelper.send(mail);
		} catch (IOException | MessagingException e) {
			LOGGER.warn("Could not send report mail with name: " + cfg.getName() + " - " + e.getMessage(), e);
		}
	}

	private void storeReport(File reportFile, String fileName) {
		if (!archiveReport) {
			return;
		}
		String absPath = archivePath + File.separator + fileName;
		try {
			archiveHelper.copyFile(reportFile.getAbsolutePath(), absPath);
		} catch (IOException e) {
			LOGGER.warn("Could not store report file with name: " + fileName + " - " + e.getMessage(), e);
		}
	}

	protected String fileNameOfCfg(LocalDateTime triggerDateTime, ReportGenerationConfig cfg) {
		String fileName = cfg.getFileNamePattern() + "." + cfg.getPrintFormat().toLowerCase();
		fileName = placeholderReplacement(triggerDateTime, fileName);
		return fileName;
	}

	private String placeholderReplacement(LocalDateTime triggerDateTime, String orig) {
		String triggerDateString = datePatternFormatter.format(triggerDateTime);
		String triggerTimeString = timePatternFormatter.format(triggerDateTime);
		String triggerWeekString = weekPatternFormatter.format(triggerDateTime);
		String newString = orig.replace("{Date}", triggerDateString);
		newString = newString.replace("{Time}", triggerTimeString);
		newString = newString.replace("{Week}", triggerWeekString);
		return newString;
	}
	

	protected List<ReportGenerationConfig> getReportGenerationConfigs(LocalDateTime triggerTimeGreaterEqual,
			LocalDateTime triggerTimeLower) {
		int weekDay = triggerTimeGreaterEqual.getDayOfWeek().getValue();
		int hour = triggerTimeGreaterEqual.getHour();
		int minuteGE = triggerTimeGreaterEqual.getMinute();
		int minuteLE = triggerTimeLower.getMinute();
		LOGGER.info("Search for report generation configs: " + weekDay + ", " + hour + ", " + minuteGE + ", " + minuteLE);
		return automaticReportGenerationConfigRepository.findConfigs(weekDay, hour, minuteGE, minuteLE);
	}

	/**
	 * Checks cyclic report generation configuration for matching trigger time.
	 */
	@Scheduled(cron = "${report.cyclic.checkCron:0 0/5 * * * ?}")
	public void checkCyclicGenerationSlot() {
		LOGGER.info("Check for automatic report generation");
		LocalDateTime now = LocalDateTime.now(zoneId);
		int slotNumber = now.getMinute() / slotDeltaMin;
		LocalDateTime triggerTimeGreaterEqual = now.withMinute(slotNumber * slotDeltaMin);
		LocalDateTime triggerTimeLowerEqual = triggerTimeGreaterEqual.plusMinutes(slotDeltaMin - 1L);
		LOGGER.info("Check for automatic report generation - " + triggerTimeGreaterEqual);
		generateAndDistributeReports(triggerTimeGreaterEqual, triggerTimeLowerEqual);
		
		
		
	}

}
