/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest.validation;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.dto.ValidationDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("validation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ValidationRestService extends BaseResource {

	@Autowired
	private ValidationController validationController;

	public ValidationRestService() {
		super(Logger.getLogger(ValidationRestService.class.getName()), new FileHelper());
	}

	@PUT
	@Path("/standbyschedule/status/{id}")
	public Response validate(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			ValidationDto validatonDto) {

		validatonDto.setStatusId(id);
		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> validationController
				.startValidation(validatonDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN,
				Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}
}
