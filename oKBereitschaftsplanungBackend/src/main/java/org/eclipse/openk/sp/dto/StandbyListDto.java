/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBYLIST" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "StandbyListDto")
@JsonInclude(Include.NON_NULL)
public class StandbyListDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Name fehlt")
	@Size(max = 256, message = "Name mit max. 256 Zeichen\")")
	private String title;
	private Date modificationDate;
	private List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroup = new ArrayList<>();

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the lsStandbyGroups
	 */
	public List<StandbyListHasStandbyGroupDto> getLsStandbyListHasStandbyGroup() {
		return lsStandbyListHasStandbyGroup;
	}

	/**
	 * @param lsStandbyGroups the lsStandbyGroups to set
	 */
	public void setLsStandbyListHasStandbyGroup(List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroup) {
		this.lsStandbyListHasStandbyGroup = lsStandbyListHasStandbyGroup;
	}

}
