/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

import java.io.Serializable;
import java.text.MessageFormat;

import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.util.SpMsg;

public class SpException extends HttpStatusException implements Serializable {

	public SpException() {
		super(500);
	}

	public SpException(HttpStatusException e) {
		super(e.getHttpStatus(), "Error in Standbyplanning Application", e);
	}

	public SpException(int code, String message, Exception e) {
		super(code, message, e);
	}

	public SpException(int code, Exception e, String message, Object... params) {
		super(code, MessageFormat.format(message, params), e);
	}

	public SpException(SpErrorEntry ee) {
		super(ee.getCode(), ee.getMessage(), ee.getE());
	}

	public SpException(SpErrorEntry ee, Object... params) {
		super(ee.getCode(), MessageFormat.format(ee.getMessage(), params), ee.getE());
	}

	public SpException(SpExceptionEnum spExceptionType, String txtBranches) {
		super(spExceptionType.getEntry().code,
				MessageFormat.format(spExceptionType.getEntry().getMessage(), SpMsg.getLbl(txtBranches)));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2789207067059442424L;

}
