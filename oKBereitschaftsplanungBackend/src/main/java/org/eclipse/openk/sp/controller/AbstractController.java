/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public class AbstractController {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List validateEntitys(List inputList, JpaRepository repository, Logger logger) {

		List validEntityList = new ArrayList<>();

		// only existing can be used
		for (Object o : inputList) {

			AbstractEntity savedEntity = validateEntity((AbstractEntity) o, repository, logger);

			if (savedEntity != null) {
				validEntityList.add(savedEntity);
			}
		}

		return validEntityList;
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AbstractEntity validateEntity(AbstractEntity entity, JpaRepository repository, Logger logger) {

		AbstractEntity savedEntity = null;

		if (entity.getId() != null) {
			savedEntity = (AbstractEntity) repository.findOne(entity.getId());
			if (savedEntity == null) {
				logger.info("Entity does not exist! '" + entity.getId() + "'");
			}
		} else {
			logger.info("new Entity can't be saved in this context!");
		}
		return savedEntity;

	}
}