-- DELETE FORMER VALUES FROM TABLES
DELETE FROM {0}.archive_body 
DELETE FROM {0}.archive_header
DELETE FROM {0}.standby_schedule_body
DELETE FROM {0}.standby_list_has_standby_group
DELETE FROM {0}.int_ignored_calendar_for_group
DELETE FROM {0}.int_branches_for_groups
DELETE FROM {0}.int_regions_in_location
DELETE FROM {0}.int_regions_for_standby_group
DELETE FROM {0}.int_location_has_postcode
DELETE FROM {0}.int_standbygroup_has_function
DELETE FROM {0}.standby_list
DELETE FROM {0}.user_has_user_function
DELETE FROM {0}.user_in_region
DELETE FROM {0}.user_in_standby_group
DELETE FROM {0}.location_for_branch
DELETE FROM {0}.user_function
DELETE FROM {0}.standby_duration
DELETE FROM {0}.standby_group
DELETE FROM {0}.location
DELETE FROM {0}.region
DELETE FROM {0}.standby_user
DELETE FROM {0}.organisation
DELETE FROM {0}.contact_data
DELETE FROM {0}.address
DELETE FROM {0}.postcode
DELETE FROM {0}.branch
DELETE FROM {0}.standby_status 
DELETE FROM {0}.calendar 
DELETE FROM {0}.reportgenerationconfig;


-- RESET SEQUENCES 
DROP SEQUENCE {0}.USER_HAS_USER_FUNCTION_ID_SEQ
CREATE SEQUENCE {0}.USER_HAS_USER_FUNCTION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.USER_IN_REGION_ID_SEQ
CREATE SEQUENCE {0}.USER_IN_REGION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.USER_IN_STANDBY_GROUP_ID_SEQ
CREATE SEQUENCE {0}.USER_IN_STANDBY_GROUP_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_LIST_HAS_GROUP_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_LIST_HAS_GROUP_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.USER_FUNCTION_ID_SEQ
CREATE SEQUENCE {0}.USER_FUNCTION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_DURATION_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_DURATION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_GROUP_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_GROUP_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.LOCATION_FOR_BRANCH_ID_SEQ
CREATE SEQUENCE {0}.LOCATION_FOR_BRANCH_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.LOCATION_ID_SEQ
CREATE SEQUENCE {0}.LOCATION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.REGION_ID_SEQ
CREATE SEQUENCE {0}.REGION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_USER_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_USER_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.ORGANISATION_ID_SEQ
CREATE SEQUENCE {0}.ORGANISATION_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.CONTACT_DATA_ID_SEQ
CREATE SEQUENCE {0}.CONTACT_DATA_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.ADDRESS_ID_SEQ
CREATE SEQUENCE {0}.ADDRESS_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.POSTCODE_ID_SEQ
CREATE SEQUENCE {0}.POSTCODE_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.BRANCH_ID_SEQ
CREATE SEQUENCE {0}.BRANCH_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_LIST_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_LIST_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_SCHEDULE_BODY_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_SCHEDULE_BODY_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.STANDBY_STATUS_ID_SEQ
CREATE SEQUENCE {0}.STANDBY_STATUS_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.ARCHIVE_BODY_ID_SEQ
CREATE SEQUENCE {0}.ARCHIVE_BODY_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.ARCHIVE_HEADER_ID_SEQ
CREATE SEQUENCE {0}.ARCHIVE_HEADER_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.CALENDAR_ID_SEQ
CREATE SEQUENCE {0}.CALENDAR_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE
DROP SEQUENCE {0}.REPORTGENERATIONCONFIG_ID_SEQ
CREATE SEQUENCE {0}.REPORTGENERATIONCONFIG_ID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE