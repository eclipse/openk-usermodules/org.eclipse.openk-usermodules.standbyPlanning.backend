-- {0}.branch;
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Strom');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Gas');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Wasser');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Fernwärme');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Telekommunikation');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Fernwirktechnik');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Verkehrssignalanlagen');
INSERT INTO {0}.branch(id, title) VALUES ({0}.BRANCH_ID_SEQ.nextval, 'Beleuchtungsanlagen');