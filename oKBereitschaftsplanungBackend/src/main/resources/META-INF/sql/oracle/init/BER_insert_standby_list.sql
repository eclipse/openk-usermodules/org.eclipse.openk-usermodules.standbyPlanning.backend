/* {0}.standby_list */
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (1, TO_TIMESTAMP('2018-08-06 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Einsatzleiter Strom');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (2, TO_TIMESTAMP('2018-08-06 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Einsatzleiter Gas');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (3, TO_TIMESTAMP('2018-08-06 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Alle Gruppen');
/* Erweiterung der N-ERGIE */
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (4, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Sulzbach_Rosenberg');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (5, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Nürnberg_NN');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (6, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Rothenburg_NR');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (7, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Nürnberg_NZ');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (8, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Weissenburg_NW');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (9, TO_TIMESTAMP('2018-11-11 02:00:00','yyyy-MM-dd hh:mi:ss'), 'Ing Bereitschaft MDN/NSG');
ALTER SEQUENCE {0}.STANDBY_LIST_ID_SEQ INCREMENT BY 10