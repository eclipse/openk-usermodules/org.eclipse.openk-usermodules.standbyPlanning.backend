-- DELETE FORMER VALUES FROM TABLES
DELETE FROM {0}.archive_body;
DELETE FROM {0}.archive_header;
DELETE FROM {0}.standby_schedule_body;
DELETE FROM {0}.standby_list_has_standby_group;
DELETE FROM {0}.int_ignored_calendar_for_group;
DELETE FROM {0}.int_branches_for_groups;
DELETE FROM {0}.int_regions_in_location;
DELETE FROM {0}.int_regions_for_standby_group;
DELETE FROM {0}.int_location_has_postcode;
DELETE FROM {0}.int_standbygroup_has_function;
DELETE FROM {0}.standby_list;
DELETE FROM {0}.user_has_user_function;
DELETE FROM {0}.user_in_region;
DELETE FROM {0}.user_in_standby_group;
DELETE FROM {0}.location_for_branch;
DELETE FROM {0}.user_function;
DELETE FROM {0}.standby_duration;
DELETE FROM {0}.standby_group;
DELETE FROM {0}.location;	
DELETE FROM {0}.region; 
DELETE FROM {0}.standby_user;
DELETE FROM {0}.organisation;
DELETE FROM {0}.contact_data;
DELETE FROM {0}.address;
DELETE FROM {0}.postcode;
DELETE FROM {0}.branch;
DELETE FROM {0}.standby_status;
DELETE FROM {0}.calendar;
DELETE FROM {0}.reportgenerationconfig;


-- RESET SEQUENCES 
ALTER Sequence IF EXISTS {0}.user_has_user_function_id_seq restart;
ALTER Sequence IF EXISTS {0}.user_in_region_id_seq restart;
ALTER Sequence IF EXISTS {0}.user_in_standby_group_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_list_has_group_id_seq restart;
ALTER Sequence IF EXISTS {0}.user_function_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_duration_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_group_id_seq restart;
ALTER Sequence IF EXISTS {0}.location_for_branch_id_seq restart;
ALTER Sequence IF EXISTS {0}.location_id_seq restart;
ALTER Sequence IF EXISTS {0}.region_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_user_id_seq restart;
ALTER Sequence IF EXISTS {0}.organisation_id_seq restart;
ALTER Sequence IF EXISTS {0}.contact_data_id_seq restart;
ALTER Sequence IF EXISTS {0}.address_id_seq restart;
ALTER Sequence IF EXISTS {0}.postcode_id_seq restart;
ALTER Sequence IF EXISTS {0}.branch_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_list_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_schedule_body_id_seq restart;
ALTER Sequence IF EXISTS {0}.standby_status_id_seq restart;
ALTER Sequence IF EXISTS {0}.archive_body_id_seq restart;
ALTER Sequence IF EXISTS {0}.archive_header_id_seq restart;
ALTER Sequence IF EXISTS {0}.calendar_id_seq restart;
ALTER Sequence IF EXISTS {0}.reportgenerationconfig_id_seq restart;

