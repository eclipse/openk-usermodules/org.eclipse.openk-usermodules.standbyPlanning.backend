/* {0}.branch */
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Strom');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Gas');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Wasser');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Fernwärme');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Telekommunikation');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Fernwirktechnik');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Verkehrssignalanlagen');
INSERT INTO {0}.branch(id, title) VALUES (nextval('branch_id_seq'), 'Beleuchtungsanlagen');