/* public.organisation */
INSERT INTO {0}.organisation(id, orga_name, address_id) VALUES (nextval('organisation_id_seq'), 'TestOrga', 1);
INSERT INTO {0}.organisation(id, orga_name, address_id) VALUES (nextval('organisation_id_seq'), 'Mustermann GmbH', 2);