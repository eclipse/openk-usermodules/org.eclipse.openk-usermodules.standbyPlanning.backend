/*Branch History*/
CREATE TABLE IF NOT EXISTS LOCATION_FOR_BRANCH_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE LOCATION_FOR_BRANCH EXCLUDING ALL);
--
-- Create a row in {0}.location_for_branch_hist_audit to reflect the operation performed on location_for_branch,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_location_for_branch_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.location_for_branch_hist SELECT nextval('location_for_branch_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.location_for_branch_hist SELECT nextval('location_for_branch_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.location_for_branch_hist SELECT nextval('location_for_branch_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS location_for_branch_audit on {0}.location_for_branch;
CREATE TRIGGER location_for_branch_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.location_for_branch FOR EACH ROW EXECUTE PROCEDURE process_location_for_branch_audit();