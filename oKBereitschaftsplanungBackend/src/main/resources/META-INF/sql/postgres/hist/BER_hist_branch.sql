/*Branch History*/
CREATE TABLE IF NOT EXISTS BRANCH_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE BRANCH EXCLUDING ALL);
--
-- Create a row in {0}.branch_hist_audit to reflect the operation performed on branch,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_branch_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.branch_hist SELECT nextval('branch_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.branch_hist SELECT nextval('branch_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.branch_hist SELECT nextval('branch_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS branch_audit on {0}.branch;
CREATE TRIGGER branch_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.branch FOR EACH ROW EXECUTE PROCEDURE process_branch_audit();