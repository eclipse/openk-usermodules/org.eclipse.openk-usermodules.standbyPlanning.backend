/*ContactData History*/
CREATE TABLE IF NOT EXISTS CONTACT_DATA_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE CONTACT_DATA EXCLUDING ALL);
--
-- Create a row in {0}.contact_data_hist_audit to reflect the operation performed on contact_data,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_contact_data_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.contact_data_hist SELECT nextval('contact_data_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.contact_data_hist SELECT nextval('contact_data_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.contact_data_hist SELECT nextval('contact_data_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS contact_data_audit on {0}.contact_data;
CREATE TRIGGER contact_data_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.contact_data FOR EACH ROW EXECUTE PROCEDURE process_contact_data_audit();