/*Int_branchs_for_groups History*/
CREATE TABLE IF NOT EXISTS INT_BRANCHES_FOR_GROUPS_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE INT_BRANCHES_FOR_GROUPS EXCLUDING ALL);
--
-- Create a row in {0}.int_branches_for_groups_hist_audit to reflect the operation performed on int_branches_for_groups,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_int_branches_for_groups_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.int_branches_for_groups_hist SELECT nextval('int_branches_for_groups_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.int_branches_for_groups_hist SELECT nextval('int_branches_for_groups_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.int_branches_for_groups_hist SELECT nextval('int_branches_for_groups_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS int_branches_for_groups_audit on {0}.int_branches_for_groups;
CREATE TRIGGER int_branches_for_groups_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.int_branches_for_groups FOR EACH ROW EXECUTE PROCEDURE process_int_branches_for_groups_audit();