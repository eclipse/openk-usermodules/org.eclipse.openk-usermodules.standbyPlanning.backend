/*Int_int_regions_in_location History*/
/*Address History*/
CREATE TABLE IF NOT EXISTS INT_REGIONS_IN_LOCATION_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE INT_REGIONS_IN_LOCATION EXCLUDING ALL);
--
-- Create a row in {0}.int_int_regions_in_location_hist_audit to reflect the operation performed on int_regions_in_location,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_int_regions_in_location_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.int_regions_in_location_hist SELECT nextval('int_regions_in_location_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.int_regions_in_location_hist SELECT nextval('int_regions_in_location_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.int_regions_in_location_hist SELECT nextval('int_regions_in_location_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS int_regions_in_location_audit on {0}.int_regions_in_location;
CREATE TRIGGER int_regions_in_location_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.int_regions_in_location FOR EACH ROW EXECUTE PROCEDURE process_int_regions_in_location_audit();