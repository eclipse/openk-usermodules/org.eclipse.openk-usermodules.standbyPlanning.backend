[[section-design-decisions]]

== Design Decisions

All architecture decisions based on the Architecture Committee Handbook. There are no other deviations then the following.

=== BDEW Whitepaper "Anforderungen an sichere Steuerungs- und Telekommunikationssysteme "

During the development of this module the whitepaper of the BDWE will be used as basis concept, but only for the given four aspects with the here given exclusions.

.aspects from BDWE whitepaper
[cols="4", options="header"]
|========================================================
|aspect|german|priority|description
|integrity|Integrität|high|- Integrity testing of module specific configuration files will be checked on DEV and TEST system manualy before they will be deployed to PROD.

- A crypted checksum for system-, config- and application files will not be implemented.

- user information will be handled with the 'Auth & Auth' core module. Therefore there are no real user data sended in the module. Just the crypted bearer value.  
|availability|Verfügbarkeit|normal|In the document the aspect availability is mostly described as a hardware based topic and has not really much to do with development of this module. The only point that fits is the patching situation. (p.12) But because of the normal priority a short downtime should be acceptable. A server start is recommended but not necessary. But the application contexts need to be undployed and deployed. Therefore a short downtime is needed.
|intimacy|Vertraulichkeit|normal|At the moment there is no intimacy classifaction defined. The Standby Planning module uses user data for generation standby plans. All the data are stored in a local database with restricted access. The Module uses HTTP-Rest requests for getting the data to UI. Therefore restrictions can be set that only users with special roles can be granted access.
|maintainability |Wartbarkeit|normal|- There is no defined maintainability contract at the moment.

- The Standby Planning module is currently splittet into three components. Backend, Frontend and database. It uses moduls from openK like 'Auth & Auth' but has not other interaction to the module. So far there are no interfaces that should be called from other modules. 
|========================================================

.table of decisons
[options="header"]
|========================================================
|Date|Short|Description

|2018-05-29|Fontend Framework|We chose to use the current version of Angular (6.1.0) in order to have a stable environment. An older version like Angular 4 was no option since the newer version has several bugfixes and optimizations. Angular JS was no alternative for us.

|2018-05-29|Fontend Buildmechanism|We decided to stay as close to the native and recommended build mechanism as possible. Therefore we are using Angular CLI as our build- and testtool. We opposed to use Maven for Frontendbuild as this brings additional complexity into the build process and is far away from the recommended and accepted way in the community.

|2018-05-29|Fontend - Backend - DB|At this point the module is splitted in 3 different projects. 
- Frontend: Angular with cli build
- Backend: Java with Maven build
- Database
Frontend and Backend have different components but at this point it would not make much sense to modulize them. (Diffenrent .jars, for example, would be more effort than benefit.) 

|2018-05-29|Camunda BPM|Camunda BPM is used as BPMN engine to route processes.

|2018-05-29|Tools and APIs|The technical constraints are documented in the 02_architecture_constraints.adoc and defined in the backend (spbe) pom.xml

|2018-05-29|ng2-daterangepicker|The source code of the date picker openK prefered is not maintained since 10 month. The last change was an ugrade to angular 4. Because the Standby Planning module uses angular 6 we decide not to use this date picker. Instead we chose to use the datepicker from ng-bootstrap (https://ng-bootstrap.github.io) which is well maintained.

|2018-06-14|AG-Grid for tables|The Product Owner preferred the usage of AG-Grid in this Project. As we did not see any disadvantages we agreed and used AG-Grid from now on. The alternative standard HTML-Tables were therefore obsolete.

|2018-06-29|no use of bom.xml|A bom.xml is not needed as long no further modularization is needed for the backend modul. The pom.xml file is enough to describe the external used .jars.

|2018-10-23|Standard HTML Tables for View "Abfrage aktuelle Bereitschaft"|For the View "Abfrage aktuelle Bereitschaft" it was required that it is responsive in order to use it on mobile devices and on Computers. We couldn´t get the AG-Grid responsive and therefore asked the Product Owner to use Standard HTML Tables. He agreed - (https://openkonsequenz.atlassian.net/browse/BP-187?focusedCommentId=10277&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-10277).

|2019-01-17|Using three git repositories|We decided to use two repositories for frontend, backend and technical documentation. The third project contains some selenium integration and regession tests.

|2019-01-17|Standby bodys work with real date|We decided to use real date information in standby bodys. If a standby duration goes from Mo. 16:00 to Di. 8:00, then two standby bodys would be generated. The dateline splits the duration. 

|2019-01-17|Documentation of used libraries|They way we realized the user requirements with this application, gives us a inherent structure for documentation and configuration of used libraries. In the backend project the pom.xml file and in the frontend project the package.json file contains the information of used libraries. 

|2019-01-17|Frontend building with npm| The backend java code has been be build by maven. The frontend Angular code will be build by npm. https://docs.npmjs.com/about-npm/index.html
We decided to use npm without maven to have a straight forward realisation of the building structure. https://medium.com/spektrakel-blog/angular-in-the-enterprise-building-angular-apps-through-maven-3ca535152f85

|2019-01-17|Frontend packaging without maven|To reduce the complexity of building an packaging Angular code, we decided to pass maven in this context. 

|2019-01-17|Frontend testing with Karma|The Angular frontend code contains unit and integration tests by using Karma. The test results will be shown in the SonarQube application. 

|2019-01-17|Connection Pooling| We decided to ues connection pooling in the StandbyPlanning application. That allows use to reduce the time to establish connections to the database. 

|2019-01-17|Integrity of planning data |The product owner and the team of developers decided to use database shadow tables. Theses tables saves copies of manipulated data rows. This mechanism works outside of the application directly in the database. There are no interfaces implemented between the shadow tables and the application. That design decision make sure that no-one from outside of the database, from the StandbyPlanning backend  to the frontend, can change existing data in the history tables.  

|2019-01-29|BIRT as reporting framework	| We decided to use BIRT as reporting framework, while the IP-Check for that library works on Eclipse. The alternative JASPER REPORTS has no valid IP-Check on Eclipse. There were requirements that were covered by a report engine. BIRT offered these functionalities. With this it is possible to make adaptations on the customer side.

|2019-01-29|Camunda BPM not working| Unfortunately we can not use the Camunda engine. While the IP-Check on Eclipse dose not work for that engine, we decided to dismount the Camunda engine on this project. 

|========================================================